# [Confluence Wiki Page](https://magairports.atlassian.net/wiki/spaces/MRes/pages/728695068/MAG+RES+ADMIN+End-to-End+tests?atlOrigin=eyJpIjoiYTMxNzUxODNjMzY4NDhlYzhmYTZhYmRlZDkwMDg3MWUiLCJwIjoiYyJ9)

# Setup

1. Create a json file in fixtures folder named "users.json" and fill it with these fields

``` 
{
  "email": "YOUR_EMAIL",
  "password": "YOUR_PASSWORD"
}
```

2. Ensure you have all the necessary READ, WRITE, UPDATE and CREATE permissions
3. Install Dependencies
```
npm install
```

4. To run cypress, open a command line from mag.res.web.admin.cypress folder and run

``` 
npx cypress open 
```