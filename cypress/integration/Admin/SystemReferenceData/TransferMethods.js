import * as customFunctions from '../../../support/functions.js'
import * as Data from '../../../fixtures/AdminSetup.json'

const transferMethodRoute = '/admin/api/transferMethod'

describe('Transfer Methods Test', function () {
    const TransferMethod = Data.Admin.SystemReferenceData.TransferMethod

    beforeEach(() => {
        cy.logIn("/admin")

        cy.route('GET', transferMethodRoute).as('GetData')

        cy.get('[data-cy=sidebar-item-system-reference-data]').click()
        cy.get('[data-cy=sidebar-item-system-reference-data-transfer-methods]').click()

        cy.wait('@GetData')
    })


    it('Modify Image', function () {
        Modify(TransferMethod.Modified, TransferMethod.Original.Name)
    })

    it('Checks Table for Modified Image', function () {
        CheckTable(TransferMethod.Modified)
    })

    it('Reverts Image', function () {
        Modify(TransferMethod.Original, TransferMethod.Modified.Name)
    })

    it('Checks Table for Reverted Image', function () {
        CheckTable(TransferMethod.Original)
    })
})

function Modify(testData, search) {
    cy.route('POST', transferMethodRoute + '/image/' + testData.FileName).as('PostImage')
    cy.route('GET', transferMethodRoute + '/*').as('GetImage')
    cy.route('PUT', transferMethodRoute + '/*').as('PutData')

    var modified = false

    cy.get('.table-component__table__body')
        .find('tr').each((tr) => {
            if (tr[0].cells[0].innerText == search) {

                cy.wrap(tr[0].cells[2]).contains('Change').click()

                cy.wait('@GetImage')

                cy.get('[data-cy=transfer-methods-form-name-input] > .row > .field > :nth-child(1) > .input > .form-control').clear().type(testData.Name)

                customFunctions.FileUpload('[data-cy=transfer-methods-form-icon-input]', testData.Path, testData.FileName)
                cy.wait('@PostImage')
                cy.get('[data-cy=transfer-method-action-button]').click()

                cy.wait('@PutData').then((response) => {
                    cy.wrap(response.status).should('eq', 200)
                    modified = true
                })
            }
        }).then(() => {
            cy.wrap(modified).should('eq', true)
        })
}

function CheckTable(testData) {
    var found = false
    cy.get('.table-component__table__body')
        .find('tr').each((tr) => {
            if (tr[0].cells[0].innerText == testData.Name) {
                var imageSrcSplit = tr[0].cells[1].childNodes[0].childNodes[0].src.split("/")
                var imageSrc = imageSrcSplit[imageSrcSplit.length - 1]
                cy.wrap(imageSrc).should('eq', testData.FileName)
                found = true
            }
        }).then(() => {
            cy.wrap(found).should('eq', true)
        })
}