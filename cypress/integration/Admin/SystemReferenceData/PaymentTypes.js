import * as customFunctions from '../../../support/functions.js'
import * as Data from '../../../fixtures/AdminSetup.json'

describe("Payment Type Tests", function () {

    const PaymentType = Data.Admin.SystemReferenceData.PaymentType

    const paymentTypeRoute = '/admin/api/paymentType'

    beforeEach(function () {
        cy.logIn("/admin")

        cy.route('GET', paymentTypeRoute).as('GetData')

        cy.get('[data-cy=sidebar-item-system-reference-data]').click()
        cy.get('[data-cy=sidebar-item-system-reference-data-payment-types]').click()

        cy.wait('@GetData')
    })

    it('Create Payment Types', function () {
        cy.route('POST', paymentTypeRoute).as('PostData')

        cy.get('[data-cy=payment-types-list-create-button]').click()

        FillForm(PaymentType.Original)

        cy.wait('@PostData').then((response) => {
            expect(response.status).to.eq(200)
        })
    })

    it('Checks Table for Created Payment Type', function () {
        CheckTable(PaymentType.Original)
    })

    it('Modify Payment Type', function () {
        cy.route('PUT', paymentTypeRoute + '/*').as('PutData')
        var modified = false
        cy.get('.table-component__table__body')
            .find('tr').each((tr, i) => {
                if (tr[0].cells[0].innerText == PaymentType.Original.Name) {
                    cy.wrap(tr[0].cells[7]).contains('Change').click()

                    FillForm(PaymentType.Modified)

                    cy.wait('@PutData').then((response) => {
                        cy.wrap(response.status).should('eq', 200)
                        modified = true
                    })
                }
            }).then(() => {
                cy.wrap(modified).should('eq', true)
            })
    })

    it('Checks Table for Modified Payment Type', function () {
        CheckTable(PaymentType.Modified)
    })


    it("Delete Payment Type", function () {
        cy.route('DELETE', paymentTypeRoute + '/*').as('DeleteData')
        var deleted = false

        cy.get('.table-component__table__body')
            .find('tr').each((tr, i) => {
                if (tr[0].cells[0].innerText == PaymentType.Modified.Name) {
                    cy.wrap(tr[0].cells[7]).contains('Delete').click()
                    cy.get('.modal-footer > .btn').click()

                    cy.wait('@DeleteData').then((response) => {
                        cy.wrap(response.status).should('eq', 200)
                        deleted = true
                    })
                }
            }).then(() => {
                cy.wrap(deleted).should('eq', true)
            })

        cy.wait('@GetData')

        cy.get('.table-component__table__body')
            .find('tr').each((tr, i) => {
                expect(tr[0].cells[0].innerText).to.not.equal(PaymentType.Original.Name)
                expect(tr[0].cells[0].innerText).to.not.equal(PaymentType.Modified.Name)
            })
    })
})

function FillForm(testData) {
    cy.get('[data-cy=payment-type-form-white-label-select] > .field > :nth-child(1) > .input > .form-control').select(testData.WhiteLabel)
    cy.get('[data-cy=payment-type-form-name-input] > .row > .field > :nth-child(1) > .input > .form-control').clear().type(testData.Name)
    cy.get('[data-cy=payment-type-form-gateway-code-input] > .row > .field > :nth-child(1) > .input > .form-control').clear().type(testData.PaymentGatewayCode)
    cy.get('[data-cy=payment-type-form-display-type-select] > .field > :nth-child(1) > .input > .form-control').select(testData.DisplayType.Type)
    cy.get('[data-cy=payment-type-form-language-code-input] > .row > .field > :nth-child(1) > .input > .form-control').clear().type(testData.DisplayType.Name)
    cy.get('[data-cy=payment-type-form-help-text-input] > .row > .field > :nth-child(1) > .input > .form-control').clear().type(testData.DisplayType.HelpText)

    customFunctions.SetCheckbox('[data-cy=payment-type-form-confirm-no-payment-checkbox] > .row > .field > .form-check > .input__checkbox-label > .input__checkbox-input', testData.ConfirmNoPayment)
    customFunctions.SetCheckbox('[data-cy=payment-type-form-default-checkbox] > .row > .field > .form-check > .input__checkbox-label > .input__checkbox-input', testData.Default)
    customFunctions.SetCheckbox('[data-cy=payment-type-form-active-checkbox] > .row > .field > .form-check > .input__checkbox-label > .input__checkbox-input', testData.Active)

    cy.get('[data-cy=payment-type-form-sort-order-input] > .row > .field > :nth-child(1) > .input > .form-control').clear().type(testData.SortOrder)

    cy.get('[data-cy=payment-type-action-button]').click()
}

function CheckTable(testData) {
    var found = false
    cy.get('.table-component__table__body')
        .find('tr').each((tr, i) => {
            if (tr[0].cells[0].innerText == testData.Name) {
                expect(tr[0].cells[0].innerText).to.equal(testData.Name)
                expect(tr[0].cells[1].innerText).to.equal(testData.WhiteLabel)
                expect(tr[0].cells[2].innerText).to.equal(testData.PaymentGatewayCode)
                expect(tr[0].cells[3].innerText).to.equal(testData.DisplayType.Type)
                expect(tr[0].cells[4].innerText.toLowerCase()).to.equal(testData.Default.toString())
                expect(parseInt(tr[0].cells[5].innerText)).to.equal(testData.SortOrder)
                expect(tr[0].cells[6].innerText.toLowerCase()).to.equal(testData.Active.toString())
                found = true
            }
        }).then(() => {
            cy.wrap(found).should('eq', true)
        })
}