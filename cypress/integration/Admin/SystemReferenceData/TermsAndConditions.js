import * as Data from '../../../fixtures/AdminSetup.json'

const termsAndConditionsRoute = 'admin/api/termsAndConditions'

describe('Terms and Conditions Tests', function () {

    const TAC = Data.Admin.SystemReferenceData.TermsAndConditions

    beforeEach(function () {
        cy.logIn("/admin")

        cy.route('GET', termsAndConditionsRoute).as('GetData')

        cy.get('[data-cy=sidebar-item-system-reference-data]').click()
        cy.get('[data-cy=sidebar-item-system-reference-data-terms-and-conditions]').click()

        cy.wait('@GetData')
    })

    it('Modify Terms and Conditions', function () {
        Modify(TAC.Modified, TAC.Original.Name)
    })

    it('Checks Table for Modified Terms and Conditions', function () {
        CheckTable(TAC.Modified)
    })


    it('Revert Terms and Conditions', function () {
        Modify(TAC.Original, TAC.Modified.Name)
    })

    it('Checks Table for Reverted Terms and Conditions', function () {
        CheckTable(TAC.Original)
    })
})

function Modify(testData, search) {
    cy.route('GET', termsAndConditionsRoute + '/*').as('GetTermsAndConditions')
    cy.route('PUT', termsAndConditionsRoute + '/*').as('PutData')
    var modified = false
    cy.get('.table-component__table__body')
        .find('tr').each((tr) => {
            if (tr[0].cells[1].innerText == search) {

                cy.wrap(tr[0].cells[2]).contains('Change').click()

                cy.wait('@GetTermsAndConditions')

                cy.get('[data-cy=terms-and-conditions-form-airport-select] > .field > :nth-child(1) > .input > .form-control').select(testData.Airport)
                cy.get('[data-cy=terms-and-conditions-form-name-input] > .row > .field > :nth-child(1) > .input > .form-control').clear().type(testData.Name)

                cy.get('.editr--content').clear().type(testData.TermsAndConditions)

                cy.get('[data-cy=terms-and-conditions-form-save-btn]').click()

                cy.wait('@PutData').then((response) => {
                    cy.wrap(response.status).should('eq', 200)
                    modified = true
                })
            }
        }).then(() => {
            cy.wrap(modified).should('eq', true)
        })
}

function CheckTable(testData) {
    var found = false
    cy.get('.table-component__table__body')
        .find('tr').each((tr) => {
            if (tr[0].cells[1].innerText == testData.Name) {
                cy.wrap(tr[0].cells[0].innerText).should('eq', testData.Airport)
                cy.wrap(tr[0].cells[1].innerText).should('eq', testData.Name)
                found = true
            }
        }).then(() => {
            cy.wrap(found).should('eq', true)
        })
}