import * as customFunctions from '../../../support/functions.js'
import * as Data from '../../../fixtures/AdminSetup.json'

const taxCodeRoute = 'admin/api/taxCode'

describe('Tax Code Tests', function () {

    const TaxCode = Data.Admin.SystemReferenceData.TaxCode

    beforeEach(function () {
        cy.logIn("/admin")

        cy.route('GET', taxCodeRoute).as('GetData')

        cy.get('[data-cy=sidebar-item-system-reference-data]').click()
        cy.get('[data-cy=sidebar-item-system-reference-data-tax-codes]').click()

        cy.wait('@GetData')
    })

    it('Modify Tax Code', function () {
        Modify(TaxCode.Modified, TaxCode.Original.Name)
    })

    it('Checks Table for Modified Tax Code', function () {
        CheckTable(TaxCode.Modified)
    })


    it('Revert Tax Code', function () {
        Modify(TaxCode.Original, TaxCode.Modified.Name)
    })

    it('Checks Table for Reverted Tax Code', function () {
        CheckTable(TaxCode.Original)
    })
})

function Modify(testData, search) {
    cy.route('GET', taxCodeRoute + '/*').as('GetTaxCode')
    cy.route('PUT', taxCodeRoute + '/*').as('PutData')
    var modified = false
    cy.get('.table-component__table__body')
        .find('tr').each((tr) => {
            if (tr[0].cells[0].innerText == search) {

                cy.wrap(tr[0].cells[2]).contains('Change').click()

                cy.wait('@GetTaxCode')

                customFunctions.SetCheckbox('[data-cy=tax-code-form-default-checkbox] > .row > .field > .form-check > .input__checkbox-label > .input__checkbox-input', testData.Default)
                cy.get('[data-cy=tax-code-form-code-input] > .row > .field > div > .input > .form-control').clear().type(testData.Name)

                cy.get('[data-cy=tax-code-save-button]').click()

                cy.wait('@PutData').then((response) => {
                    cy.wrap(response.status).should('eq', 200)
                    modified = true
                })
            }
        }).then(() => {
            cy.wrap(modified).should('eq', true)
        })
}

function CheckTable(testData) {
    var found = false
    cy.get('.table-component__table__body')
        .find('tr').each((tr) => {
            if (tr[0].cells[0].innerText == testData.Name) {
                cy.wrap(tr[0].cells[0].innerText).should('eq', testData.Name)
                cy.wrap(tr[0].cells[1].innerText).should('eq', testData.Default.toString())
                found = true
            }
        }).then(() => {
            cy.wrap(found).should('eq', true)
        })
}