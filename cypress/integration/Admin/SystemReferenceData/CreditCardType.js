import * as customFunctions from '../../../support/functions.js'
import * as Data from '../../../fixtures/AdminSetup.json'

const CreditCardType = Data.Admin.SystemReferenceData.CreditCardType

const creditCardTypeRoute = '/admin/api/creditCardType'

describe("Credit Card Types Tests", function () {

    beforeEach(function () {
        cy.logIn("/admin")

        cy.route('GET', creditCardTypeRoute).as('GetData')

        cy.get('[data-cy=sidebar-item-system-reference-data]').click()
        cy.get('[data-cy=sidebar-item-system-reference-data-credit-card-types]').click()

        cy.wait('@GetData')
    })

    it('Create Credit Card Types', function () {

        cy.route('POST', creditCardTypeRoute).as('PostData')

        cy.get('[data-cy=credit-card-types-list-create-button]').click()

        FillForm(CreditCardType.Original)

        cy.wait('@PostData').then((response) => {
            expect(response.status).to.eq(200)
        })
    })

    it('Checks Table for Created Credit Card Types', function () {
        CheckTable(CreditCardType.Original)
    })

    it('Modify Credit Card Type', function () {
        cy.route('PUT', creditCardTypeRoute + '/*').as('PutData')

        var modified = false

        cy.get('.table-component__table__body > tr').each((tr, i) => {
                if (tr[0].cells[1].innerText == CreditCardType.Original.Name) {
                    cy.wait(1000)
                    cy.wrap(tr[0].cells[5]).contains('Change').click()

                    FillForm(CreditCardType.Modified)

                    cy.wait('@PutData').then((response) => {
                        cy.wrap(response.status).should('eq', 200)
                        modified = true
                    })

                }
            }).then(() => {
                cy.wrap(modified).should('eq', true)
            })
    })

    it('Check Table for Modified Credit Card Type', function () {
        CheckTable(CreditCardType.Modified)
    })

    it("Delete Credit Card Types", function () {
        var deleted = false
        cy.route('DELETE', creditCardTypeRoute + '/*').as('DeleteData')

        cy.get('.table-component__table__body > tr').each((tr, i) => {
                if (tr[0].cells[1].innerText == CreditCardType.Modified.Name) {
                    cy.wait(1000) //wait for toast to disappear
                    cy.wrap(tr[0].cells[5]).contains('Delete').click()
                    cy.get('.modal-footer > .btn').click()

                    cy.wait('@DeleteData').then((response) => {
                        cy.wrap(response.status).should('eq', 204)
                        deleted = true
                    })
                }
            }).then(() => {
                cy.wrap(deleted).should('eq', true)
            })

        cy.wait('@GetData')

        cy.get('.table-component__table__body > tr').each((tr, i) => {
                expect(tr[0].cells[1].innerText).to.not.equal(CreditCardType.Original.Name)
                expect(tr[0].cells[1].innerText).to.not.equal(CreditCardType.Modified.Name)
            })
    })

    //TableSort
    /*const Loop = Array.from({ length: 4 }, (v, k) => k)
    const test = ['White Label', 'Name', 'Fee', 'Amount']

    it('Check Ascending', function () {
        customFunctions.TableSortAscending(Loop, test, '.table-component__table__head', '.table-component__table__body')
    })

    it('Check Descending', function () {
        customFunctions.TableSortDescending(Loop, test, '.table-component__table__head', '.table-component__table__body')
    })*/
})

function FillForm(testData) {
    cy.get('[data-cy=credit-card-types-form-whitelabel-name-select] > .field > :nth-child(1) > .input > .form-control').select(testData.Label)
    cy.get('[data-cy=credit-card-types-form-card-name-input] > .row > .field > :nth-child(1) > .input > .form-control').clear().type(testData.Name)
    cy.get('[data-cy=credit-card-types-form-fee-select] > .field > :nth-child(1) > .input > .form-control').select(testData.Fee)
    cy.get('[data-cy=credit-card-types-form-amount-input] > .row > .field > :nth-child(1) > .input > .form-control').clear().type(testData.Amount.toString())

    customFunctions.SetCheckbox('[data-cy=credit-card-types-active-checkbox] > .row > .field > .form-check > .input__checkbox-label > .input__checkbox-input', testData.Active)

    cy.get('[data-cy=credit-card-types-action-button]').click()
}

function CheckTable(testData) {
    var found = false
    cy.get('.table-component__table__body > tr').each((tr, i) => {
            if (tr[0].cells[1].innerText == testData.Name) {
                expect(tr[0].cells[0].innerText).to.equal(testData.Label)
                expect(tr[0].cells[1].innerText).to.equal(testData.Name)
                expect(tr[0].cells[2].innerText).to.equal(testData.Fee)
                expect(tr[0].cells[3].innerText).to.equal(testData.Amount.toString())
                expect(tr[0].cells[4].innerText).to.equal(customFunctions.Capitalize(testData.Active.toString()))
                found = true
            }
        }).then(() => {
            cy.wrap(found).should('eq', true)
        })
}