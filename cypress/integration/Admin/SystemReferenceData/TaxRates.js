import moment from 'moment'
import * as customFunctions from '../../../support/functions.js'
import * as Data from '../../../fixtures/AdminSetup.json'

const taxCodeRoute = 'admin/api/taxCode'
const taxRatesRoute = '/admin/api/taxRate'

describe('Tax Rates Tests', function () {

  const TaxRate = Data.Admin.SystemReferenceData.TaxRate

  var fromDate = {
    Original: customFunctions.ParseDate(null, TaxRate.Original.FromDate.Amount, TaxRate.Original.FromDate.Period),
    Modified: customFunctions.ParseDate(null, TaxRate.Modified.FromDate.Amount, TaxRate.Modified.FromDate.Period),
  }

  var toDate = {
    Original: customFunctions.ParseDate(fromDate.Original, TaxRate.Original.ToDate.Amount, TaxRate.Original.ToDate.Period),
    Modified: customFunctions.ParseDate(fromDate.Modified, TaxRate.Modified.ToDate.Amount, TaxRate.Modified.ToDate.Period),
  }

  beforeEach(function () {
    cy.logIn("/admin")

    cy.route('GET', taxRatesRoute).as('GetData')

    cy.get('[data-cy=sidebar-item-system-reference-data]').click()
    cy.get('[data-cy=sidebar-item-system-reference-data-tax-rates]').click()

    cy.wait('@GetData')
  })

  it('Modify Tax Rates', function () {
    Modify(TaxRate.Modified, fromDate.Modified, toDate.Modified, TaxRate.Original.Name)
  })

  it('Checks Table for Modified Tax Rates', function () {
    CheckTable(TaxRate.Modified, fromDate.Modified, toDate.Modified)
  })


  it('Revert Tax Rates', function () {
    Modify(TaxRate.Original, fromDate.Original, toDate.Original, TaxRate.Modified.Name)
  })

  it('Checks Table for Reverted Tax Rates', function () {
    CheckTable(TaxRate.Original, fromDate.Original, toDate.Original)
  })
})

function Modify(testData, DateFrom, DateTo, search) {
  cy.route('GET', taxRatesRoute + '/*').as('GetTaxRates')
  cy.route('GET', taxCodeRoute).as('GetTaxCode')
  cy.route('PUT', taxRatesRoute + '/*').as('PutData')

  var modified = false
  cy.get('.table-component__table__body')
    .find('tr').each((tr) => {
      if (tr[0].cells[1].innerText == search) {

        cy.wrap(tr[0].cells[5]).contains('Change').click()

        cy.wait('@GetTaxRates')

        cy.wait('@GetTaxCode')

        cy.get('[data-cy=tax-rates-form-id-select] >.field > :nth-child(1) > .input > .form-control').select(testData.TaxCode)
        cy.get('[data-cy=tax-rates-form-name-input]').clear().type(testData.Name)

        customFunctions.SetDate('#validFrom', DateFrom)
        customFunctions.SetDate('#validTo', DateTo)

        cy.get('[data-cy=tax-rates-form-overall-rate-input]').clear().type(testData.OverallTaxRate)

        cy.get('[data-cy=tax-rates-form-tax-1-label-input]').clear().type(testData.Tax1.Label)
        cy.get('[data-cy=tax-rates-form-tax-1-amount-input]').clear().type(testData.Tax1.Amount)

        cy.get('[data-cy=tax-rates-form-tax-2-label-input]').clear().type(testData.Tax2.Label)
        cy.get('[data-cy=tax-rates-form-tax-2-amount-input]').clear().type(testData.Tax2.Amount)

        cy.get('[data-cy=tax-rate-save-button]').click()

        cy.wait('@PutData').then((response) => {
          cy.wrap(response.status).should('eq', 200)
          modified = true
        })
      }
    }).then(() => {
      cy.wrap(modified).should('eq', true)
    })
}

function CheckTable(testData, DateFrom, DateTo) {
  var found = false
  cy.get('.table-component__table__body')
    .find('tr').each((tr) => {
      if (tr[0].cells[1].innerText == testData.Name) {
        cy.wrap(tr[0].cells[0].innerText).should('eq', testData.TaxCode)
        cy.wrap(tr[0].cells[1].innerText).should('eq', testData.Name)
        cy.wrap(tr[0].cells[2].innerText).should('eq', moment(DateFrom).format('D/M/YYYY'))
        cy.wrap(tr[0].cells[3].innerText).should('eq', moment(DateTo).format('D/M/YYYY'))
        cy.wrap(tr[0].cells[4].innerText).should('eq', testData.OverallTaxRate.toString())

        found = true
      }
    }).then(() => {
      cy.wrap(found).should('eq', true)
    })
}