import * as Data from '../../../fixtures/AdminSetup.json'

const whiteLabelRoute = 'admin/api/whiteLabel'
const channelRoute = 'admin/api/channel'

describe('White Label Tests', function () {

    const WhiteLabel = Data.Admin.SystemReferenceData.WhiteLabel

    beforeEach(function () {
        cy.logIn("/admin")

        cy.route('GET', whiteLabelRoute).as('GetData')

        cy.get('[data-cy=sidebar-item-system-reference-data]').click()
        cy.get('[data-cy=sidebar-item-system-reference-data-white-labels]').click()

        cy.wait('@GetData')
    })

    it('Modify White Label', function () {
        Modify(WhiteLabel.Modified, WhiteLabel.Original.Name)
    })

    it('Checks Table for Modified White Label', function () {
        CheckTable(WhiteLabel.Modified)
    })


    it('Revert White Label', function () {
        Modify(WhiteLabel.Original, WhiteLabel.Modified.Name)
    })

    it('Checks Table for Reverted White Label', function () {
        CheckTable(WhiteLabel.Original)
    })
})

function Modify(testData, search) {
    cy.route('GET', whiteLabelRoute + '/*').as('GetWhiteLabel')
    cy.route('PUT', whiteLabelRoute + '/*').as('PutData')
    cy.route('GET', channelRoute).as('GetChannel')
    var modified = false
    cy.get('.table-component__table__body')
        .find('tr').each((tr) => {
            if (tr[0].cells[0].innerText == search) {

                cy.wrap(tr[0].cells[2]).contains('Change').click()

                cy.wait('@GetChannel')
                cy.wait('@GetWhiteLabel')

                cy.get('[data-cy=white-label-form-name-input]').clear().type(testData.Name)
                cy.get('[data-cy=white-label-form-app-name-input]').clear().type(testData.ApplicationName)
                cy.get('[data-cy=white-label-form-channel-select] > .field > :nth-child(1) > .input > .form-control').select(testData.Channel)

                cy.get('[data-cy=white-label-save-button]').click()

                cy.wait('@PutData').then((response) => {
                    cy.wrap(response.status).should('eq', 200)
                    modified = true
                })
            }
        }).then(() => {
            cy.wrap(modified).should('eq', true)
        })
}

function CheckTable(testData) {
    var found = false
    cy.get('.table-component__table__body')
        .find('tr').each((tr) => {
            if (tr[0].cells[0].innerText == testData.Name) {
                cy.wrap(tr[0].cells[0].innerText).should('eq', testData.Name)
                cy.wrap(tr[0].cells[1].innerText).should('eq', testData.Channel)
                found = true
            }
        }).then(() => {
            cy.wrap(found).should('eq', true)
        })
}