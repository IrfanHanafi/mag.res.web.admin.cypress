import * as Data from '../../../fixtures/AdminSetup.json'

const reportingProductRoute = 'admin/api/reportingProduct'
const airportGroupRoute = 'admin/api/airportGroup'

describe('Reporting Product Tests', function () {

    const ReportingProduct = Data.Admin.SystemReferenceData.ReportingProduct

    beforeEach(function () {
        cy.logIn("/admin")

        cy.route('GET', reportingProductRoute).as('GetData')

        cy.get('[data-cy=sidebar-item-system-reference-data]').click()
        cy.get('[data-cy=sidebar-item-system-reference-data-reporting-products]').click()

        cy.wait('@GetData')
    })

    it('Modify Reporting Product', function () {
        Modify(ReportingProduct.Modified, ReportingProduct.Original.Name)
    })

    it('Checks Table for Modified Reporting Product', function () {
        CheckTable(ReportingProduct.Modified)
    })


    it('Revert Reporting Product', function () {
        Modify(ReportingProduct.Original, ReportingProduct.Modified.Name)
    })

    it('Checks Table for Reverted Reporting Product', function () {
        CheckTable(ReportingProduct.Original)
    })
})

function Modify(testData, search) {
    cy.route('GET', airportGroupRoute).as('GetAirportGroup')

    var modified = false
    cy.get('tbody')
        .find('tr').each((tr) => {
            debugger
            if (tr[0].cells[0].innerText == search) {

                cy.route('GET', reportingProductRoute + '/*').as('GetReportingProduct')
                cy.route('PUT', reportingProductRoute + '/*').as('PutData')

                cy.wrap(tr[0].cells[3]).contains('Change').click()

                cy.wait('@GetAirportGroup')
                cy.wait('@GetReportingProduct')

                cy.get('[data-cy=reporting-product-form-name-input] > .row > .field > :nth-child(1) > .input > .form-control').clear().type(testData.Name)
                cy.get('[data-cy=reporting-product-form-code-input] > .row > .field > :nth-child(1) > .input > .form-control').clear().type(testData.Code)
                cy.get('[data-cy=reporting-product-form-status-select] > .field > :nth-child(1) > .input > .form-control').select(testData.ReportingProductStatus)
                cy.get('[data-cy=reporting-product-form-airport-group-select] > .field > :nth-child(1) > .input > .form-control').select(testData.AirportGroup)

                cy.get('[data-cy=save-reporting-product-btn]').click()

                cy.wait('@PutData').then((response) => {
                    cy.wrap(response.status).should('eq', 200)
                    modified = true
                })
            }
        }).then(() => {
            cy.wrap(modified).should('eq', true)
        })
}

function CheckTable(testData) {
    var found = false
    cy.get('tbody')
        .find('tr').each((tr) => {
            if (tr[0].cells[0].innerText == testData.Name) {
                cy.wrap(tr[0].cells[0].innerText).should('eq', testData.Name)
                cy.wrap(tr[0].cells[1].innerText).should('eq', testData.Code)
                cy.wrap(tr[0].cells[2].innerText).should('eq', testData.ReportingProductStatus)
                found = true
            }
        }).then(() => {
            cy.wrap(found).should('eq', true)
        })
}