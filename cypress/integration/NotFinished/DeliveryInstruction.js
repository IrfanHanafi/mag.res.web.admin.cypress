import * as customFunctions from '../../support/functions.js'
import * as Data from '../../../fixtures/AdminSetup.json'

const deliveryInstructionRoute = 'admin/api/deliveryInstruction'
const instructionTypeRoute = 'admin/api/instructionType'
const barrierVerificationTypeRoute = 'admin/api/barrierVerificationType'

const DeliveryInstruction = Data.Admin.SystemReferenceData.DeliveryInstruction

describe('Delivery Instruction Tests', function () {

    beforeEach(function () {
        cy.logIn("/admin")

        cy.route('GET', deliveryInstructionRoute).as('GetData')

        cy.get('[data-cy=sidebar-item-system-reference-data]').click()
        cy.get('[data-cy=sidebar-item-system-reference-data-delivery-instructions]').click()

        cy.wait('@GetData')
        cy.wait(1000)
    })

    it('Modify Delivery Instruction', function () {
        Modify(DeliveryInstruction.Modified, DeliveryInstruction.Original)
    })

    it('Checks Table for Modified Delivery Instruction', function () {
        var found = false
        cy.get('.table-component__table__body > tr').each((tr) => {
                if (tr[0].cells[1].innerText == DeliveryInstruction.Modified.Name) {
                    cy.wrap(tr[0].cells[0].innerText).should('eq', DeliveryInstruction.Modified.Type)
                    cy.wrap(tr[0].cells[1].innerText).should('eq', DeliveryInstruction.Modified.Name)
                    found = true
                }
            }).then(() => {
                cy.wrap(found).should('eq', true)
            })
    })


    it('Revert Delivery Instruction', function () {
        Modify(DeliveryInstruction.Original, DeliveryInstruction.Modified)
    })

    it('Checks Table for Reverted Delivery Instruction', function () {
        var found = false
        cy.get('.table-component__table__body > tr').each((tr) => {
                if (tr[0].cells[1].innerText == DeliveryInstruction.Original.Name) {
                    cy.wrap(tr[0].cells[0].innerText).should('eq', DeliveryInstruction.Original.Type)
                    cy.wrap(tr[0].cells[1].innerText).should('eq', DeliveryInstruction.Original.Name)
                    found = true
                }
            }).then(() => {
                cy.wrap(found).should('eq', true)
            })
    })
})

function Modify(testData, search) {
    cy.route('GET', instructionTypeRoute).as('GetDeliveryInstructionType')
    cy.route('GET', barrierVerificationTypeRoute).as('GetBarrierVerificationType')
    var modified = false
    cy.get('.table-component__table__body')
        .find('tr').each((tr) => {
            if (tr[0].cells[1].innerText == search.Name) {

                cy.route('GET', deliveryInstructionRoute + '/*').as('GetDeliveryInstruction')
                cy.route('PUT', deliveryInstructionRoute + '/*').as('PutData')

                cy.wrap(tr[0].cells[2]).contains('Change').click()

                cy.wait('@GetDeliveryInstruction')
                cy.wait('@GetDeliveryInstructionType')
                cy.wait('@GetBarrierVerificationType')

                cy.get('[data-cy=delivery-instruction-form-type-select] > .field > :nth-child(1) > .input > .form-control').select(testData.Type)
                cy.get('[data-cy=delivery-instruction-form-verification-type-select] > .field > :nth-child(1) > .input > .form-control').select(testData.VerificationType)
                cy.get('[data-cy=delivery-instruction-form-name-input] > .row > .field > :nth-child(1) > .input > .form-control').clear().type(testData.Name)

                cy.get('.editr--content').clear().type(testData.Instructions)

                if (testData.Image) {
                    customFunctions.FileUpload('[data-cy=delivery-instruction-form-img-url-input]', testData.Image.Path, testData.Image.FileName)
                }

                cy.wait(1000) //waiting for toast to disapepar
                cy.get('[data-cy=save-delivery-instruction-btn]').click()

                cy.wait('@PutData').then((response) => {
                    cy.wrap(response.status).should('eq', 200)
                    modified = true
                })
            }
        }).then(() => {
            cy.wrap(modified).should('eq', true)
        })
}