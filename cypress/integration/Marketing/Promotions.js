import * as promoTest from '../../support/PromotionFunctions'
import * as Data from '../../fixtures/AdminSetup.json'

const PromotionTab = {
    DETAIL: 1,
    CHANNELS: 2,
    AFFILIATES: 3,
    DATES: 4,
    PRODUCTS: 5,
    ADDONS: 6,
    VARYINGDISCOUNTS: 7,
    PROMOTIONAVAILABILITY: 8,
    SUMMARY: 9
}

const State = {
    CREATE: 'CREATE',
    MODIFY: 'MODIFY',
    DELETE: 'DELETE'
}

const Promotion = Data.Marketing.Promotion

describe('Promotions Test', function () {

    beforeEach(() => {
        promoTest.before()
    })

    describe('Feature Test', function () {

        it('Modifies Promotion Detail', function () {
            promoTest.ModifyDetail(Promotion.Modified, Promotion.Original.Detail.Name)
        })

        it('Check Table for Modified Promotion', function () {
            promoTest.CheckTable(Promotion.Modified)
        })

        it('Revert Promotion Detail', function () {
            promoTest.ModifyDetail(Promotion.Original, Promotion.Modified.Detail.Name)
        })

        it('Check Table for Reverted Promotion', function () {
            promoTest.CheckTable(Promotion.Original)
        })
        describe('Modify Tabs', function () {

            it('Modify Channels', function () {
                promoTest.ModifyTab(Promotion.Modified, PromotionTab.CHANNELS, Promotion.Original)
            })

            it('Revert Channels', function () {
                promoTest.ModifyTab(Promotion.Original, PromotionTab.CHANNELS, Promotion.Original)
            })

            it('Modify Affiliates', function () {
                promoTest.ModifyTab(Promotion.Modified, PromotionTab.AFFILIATES, Promotion.Original)
            })

            it('Revert Affiliates', function () {
                promoTest.ModifyTab(Promotion.Original, PromotionTab.AFFILIATES, Promotion.Original)
            })

            /*
            it('Modify Dates', function () {
                promoTest.ModifyTab(Promotion.Modified, PromotionTab.DATES, Promotion.Original)
            })

            it('Revert Dates', function () {
                promoTest.ModifyTab(Promotion.Modified, PromotionTab.DATES, Promotion.Original)
            })
            */

            it('Modify Products', function () {
                promoTest.ModifyTab(Promotion.Modified, PromotionTab.PRODUCTS, Promotion.Original)
            })

            it('Revert Products', function () {
                promoTest.ModifyTab(Promotion.Original, PromotionTab.PRODUCTS, Promotion.Original)
            })

            it('Modify Add-Ons', function () {
                promoTest.ModifyTab(Promotion.Modified, PromotionTab.ADDONS, Promotion.Original)
            })

            it('Revert Add-Ons', function () {
                promoTest.ModifyTab(Promotion.Original, PromotionTab.ADDONS, Promotion.Original)
            })

            it('Create Varying Discounts', function () {
                promoTest.ModifyTab(Promotion.Modified, PromotionTab.VARYINGDISCOUNTS, Promotion.Original, State.CREATE)
            })

            it('Modify Varying Discounts', function () {
                promoTest.ModifyTab(Promotion.Modified, PromotionTab.VARYINGDISCOUNTS, Promotion.Original, State.MODIFY)
            })

            it('Delete Varying Discounts', function () {
                promoTest.ModifyTab(Promotion.Modified, PromotionTab.VARYINGDISCOUNTS, Promotion.Original, State.DELETE)
            })

            it('Modify Promotion Availability', function () {
                promoTest.ModifyTab(Promotion.Modified, PromotionTab.PROMOTIONAVAILABILITY, Promotion.Original)
            })

            it('Revert Promotion Availability', function () {
                promoTest.ModifyTab(Promotion.Original, PromotionTab.PROMOTIONAVAILABILITY, Promotion.Original)
            })
        })
    })

})