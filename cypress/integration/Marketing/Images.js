import * as customFunctions from '../../support/functions.js'
import * as Data from '../../fixtures/AdminSetup.json'

const imageRoute = '/marketing/api/image'

describe('Images Testing', function () {

    const Images = Data.Marketing.Images

    beforeEach(() => {

        cy.logInNoFeature('/marketing')

        cy.route('GET', imageRoute).as('GetData')
        cy.get('[data-cy=sidebar-item-marketing]').click()
        cy.get('[data-cy=sidebar-item-marketing-images]').click()

        cy.wait('@GetData')
    })

    it('Modify Image', function () {
        Modify(Images.Modified, Images.Original.Name)
    })

    it('Checks Table for Modified Image', function () {
        CheckTable(Images.Modified)
    })

    it('Reverts Image', function () {
        Modify(Images.Original, Images.Modified.Name)
    })

    it('Checks Table for Reverted Image', function () {
        CheckTable(Images.Original)
    })
})

function Modify(testData, search) {

    var modified = false

    cy.get('.table-component__table__body > tr').each((tr) => {
            if (tr[0].cells[0].innerText == search) {

                cy.route('GET', imageRoute + '/*').as('GetImage')
                cy.route('PUT', imageRoute + '/*').as('PutData')

                cy.wrap(tr[0].cells[2]).contains('Change').click()

                cy.wait('@GetImage')

                cy.get(':nth-child(1) > .row > .field > :nth-child(1) > .input > .form-control').clear().type(testData.Name)

                customFunctions.FileUpload('.input-file', testData.Path, testData.FileName)

                cy.get('[data-cy=image-create-button-save]').click()

                cy.wait('@PutData').then((response) => {
                    cy.wrap(response.status).should('eq', 200)
                    modified = true
                })
            }
        }).then(() => {
            cy.wrap(modified).should('eq', true)
        })
}

function CheckTable(testData) {
    var found = false
    cy.get('.table-component__table__body')
        .find('tr').each((tr) => {
            if (tr[0].cells[0].innerText == testData.Name) {
                var imageSrcSplit = tr[0].cells[1].childNodes[0].src.split("/")
                var imageSrc = imageSrcSplit[imageSrcSplit.length - 1]
                cy.wrap(imageSrc).should('eq', testData.FileName)
                found = true
            }
        }).then(() => {
            cy.wrap(found).should('eq', true)
        })
}