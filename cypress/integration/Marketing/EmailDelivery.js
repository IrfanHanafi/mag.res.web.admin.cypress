import * as Data from '../../fixtures/AdminSetup.json'

const emailDeliveryRoute = '/marketing/api/EmailDelivery'
const emailDeliveryTypesRoute = '/marketing/api/EmailDelivery/types'
const airportReferenceRoute = '/marketing/api/reference/airport'

describe('Email Delivery', function () {

    const Airport = Data.Enum.Airport

    const EmailType = Data.Enum.EmailTypes

    const EmailDelivery = Data.Marketing.EmailDelivery

    beforeEach(() => {
        cy.logInNoFeature('/marketing')

        cy.route('GET', emailDeliveryRoute).as('GetData')
        cy.route('GET', emailDeliveryTypesRoute).as('GetTypes')
        cy.route('GET', airportReferenceRoute).as('GetAirport')

        cy.get('[data-cy=sidebar-item-marketing]').click()
        cy.get('[data-cy=sidebar-item-marketing-email-delivery]').click()

        cy.wait('@GetData')
        cy.wait('@GetTypes')
        cy.wait('@GetAirport')
    })

    describe('Filter', function () {
        it('Search Term Filter', function () {
            cy.get('[data-cy=email-delivery-list-filter] > .row > .field > :nth-child(1) > .input > .form-control').type(EmailDelivery.Original.Name)

            cy.get('.table-component__table__body')
                .find('tr').each((tr) => {
                    if (tr[0].cells[0].innerText == EmailDelivery.Original.Name) {
                        cy.wrap(tr[0].cells[0].innerText).should('eq', EmailDelivery.Original.Name)
                    }
                    else
                        cy.wrap(tr[0].cells[0].innerText).should('contain', EmailDelivery.Original.Name)
                })
        })

        it('Airport Filter', function () {

            Object.keys(Airport).forEach(function (key) {
                cy.get(':nth-child(2) > .row > .field > :nth-child(1) > .input > .form-control').select(Airport[key])
                cy.wait(1000) //wait for page to load, No API calls made
                cy.get('.table-component__table__body > tr').each((tr, i) => {
                    cy.wrap(tr[0].cells[1].innerText).should('eq', Airport[key])
                })
            })
        })

        /*it('Type Filter', function () {

            Object.keys(EmailType).forEach(function (key) {
                cy.get(':nth-child(3) > .row > .field > :nth-child(1) > .input > .form-control').select(EmailType[key])

                //need to account for empty tables
                cy.get('.table-component__table__body')
                    .find('tr').each((tr) => {
                        cy.wrap(tr[0].cells[2].innerText).should('eq', EmailType[key])
                    })
            })
        })*/
    })

    describe('Email Delivery Features', function () {
        it('Modifies Email Delivery', function () {
            Modify(EmailDelivery.Modified, EmailDelivery.Original.Name)
        })

        it('Checks Table for Modified Email Delivery', function () {
            CheckTable(EmailDelivery.Modified)
        })

        it('Reverts Email Delivery', function () {
            Modify(EmailDelivery.Original, EmailDelivery.Modified.Name)
        })

        it('Checks Table for Reverted Email Delivery', function () {
            CheckTable(EmailDelivery.Original)
        })
    })
})

function Modify(testData, search) {

    var modified = false
    cy.get('.table-component__table__body')
        .find('tr').each((tr) => {
            if (tr[0].cells[0].innerText == search) {

                cy.route('GET', emailDeliveryRoute + '/*').as('GetEmailDelivery')
                cy.route('PUT', emailDeliveryRoute + '/*').as('PutData')

                cy.wrap(tr[0].cells[3]).contains('Change').click()

                cy.wait('@GetEmailDelivery')

                cy.get('[data-cy=email-delivery-input-name] > .row > .field > :nth-child(1) > .input > .form-control').clear().type(testData.Name)
                cy.get('[data-cy=email-delivery-select-airport] > .field > :nth-child(1) > .input > .form-control').select(testData.Airport)
                cy.get('[data-cy=email-delivery-select-email-delivery-type] > .field > :nth-child(1) > .input > .form-control').select(testData.Type)
                cy.get('[data-cy=email-delivery-text-area-email-template] > .field > .input > .form-control').clear().type(testData.EmailTemplate)

                cy.get('[data-cy=email-delivery-create-button-save]').click()

                cy.wait('@PutData').then((response) => {
                    cy.wrap(response.status).should('eq', 200)
                    modified = true
                })
            }
        }).then(() => {
            cy.wrap(modified).should('eq', true)
        })
}

function CheckTable(testData) {
    var found = false
    cy.get('.table-component__table__body > tr').each((tr) => {
            if (tr[0].cells[0].innerText == testData.Name) {
                cy.wrap(tr[0].cells[0].innerText).should('eq', testData.Name)
                cy.wrap(tr[0].cells[1].innerText).should('eq', testData.Airport)
                cy.wrap(tr[0].cells[2].innerText).should('eq', testData.Type)
                found = true
            }
        }).then(() => {
            cy.wrap(found).should('eq', true)
        })
}