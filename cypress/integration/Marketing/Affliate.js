import * as customFunctions from "../../support/functions.js"
import * as Data from '../../fixtures/AdminSetup.json'

const affliateRoute = '/marketing/api/affiliate'
const channelReferenceRoute = '/marketing/api/reference/channel'
const emailConfirmationReferenceRoute = '/marketing/api/reference/emailconfirmation'

describe('Affliate Test', function () {

    const Affliate = Data.Marketing.Affliate

    beforeEach(() => {
        cy.logInNoFeature('/marketing')

        cy.route('GET', affliateRoute).as('GetData')

        cy.get('[data-cy=sidebar-item-marketing]').click()
        cy.get('[data-cy=sidebar-item-marketing-affiliates]').click()

        cy.wait('@GetData')
    })

    describe('Filter Tests', function () {
        it('Search Term Code Filter', function () {

            cy.get('.table-component__filter__field').clear().type(Affliate.Original.Details.Code)

            var found = false
            cy.get('.table-component__table__body > tr').each((tr) => {
                    if (tr[0].cells[0].innerText == Affliate.Original.Details.Code) {
                        cy.wrap(tr[0].cells[0].innerText).should('eq', Affliate.Original.Details.Code)
                        found = true
                    }
                }).then(() => {
                    cy.wrap(found).should('eq', true)
                })

        })

        it('Search Term Name Filter', function () {

            cy.get('.table-component__filter__field').clear().type(Affliate.Original.Details.Name)

            var found = false
            cy.get('.table-component__table__body > tr').each((tr) => {
                    if (tr[0].cells[1].innerText == Affliate.Original.Details.Name) {
                        cy.wrap(tr[0].cells[1].innerText).should('eq', Affliate.Original.Details.Name)
                        found = true
                    }
                }).then(() => {
                    cy.wrap(found).should('eq', true)
                })
        })
    })

    describe('Feature Test', function () {
        it('Modifies Affliate', function () {
            Modify(Affliate.Modified, Affliate.Original.Details.Name)
        })

        it('Checks Table for Modified Affliate', function () {
            CheckTable(Affliate.Modified)
        })

        it('Reverts Affliate', function () {
            Modify(Affliate.Original, Affliate.Modified.Details.Name)
        })

        it('Checks Table for Reverted Affliate', function () {
            CheckTable(Affliate.Original)
        })
    })
})

function Modify(testData, search) {

    cy.route('GET', channelReferenceRoute).as('GetChannelReference')
    cy.route('GET', emailConfirmationReferenceRoute).as('GetEmailConfirmationReference')

    var modified = false
    cy.get('.table-component__table__body > tr').each((tr) => {
            if (tr[0].cells[1].innerText == search) {

                cy.route('GET', affliateRoute + '/*').as('GetAffliate')
                cy.route('PUT', affliateRoute + '/*').as('PutData')

                cy.wrap(tr[0].cells[4]).contains('Change').click()

                cy.wait('@GetAffliate')
                cy.wait('@GetChannelReference')
                cy.wait('@GetEmailConfirmationReference')

                cy.get('[data-cy=affiliate-details-code-input] > .row > .field > :nth-child(1) > .input > .form-control').clear().type(testData.Details.Code)
                cy.get('[data-cy=affiliate-details-name-input] > .row > .field > :nth-child(1) > .input > .form-control').clear().type(testData.Details.Name)
                cy.get('[data-cy=affiliate-details-channel-select] > .field > :nth-child(1) > .input > .form-control').select(testData.Details.Channel)
                cy.get('[data-cy=affiliate-details-email-confirmations-select] > .field > :nth-child(1) > .input > .form-control').select(testData.Details.DefaultEmailConfirmation)

                customFunctions.SetCheckbox('[data-cy=affiliate-details-search-only-checkbox] > .row > .field > .form-check > .input__checkbox-label > .input__checkbox-input', testData.Details.AffliateSearchOnly)

                cy.get('.card-header').then((tab) => {
                    cy.wrap(tab[0].childNodes[0].childNodes[2]).click()

                    customFunctions.SetCheckbox('[data-cy=affiliate-discount-apply-discount-checkbox] > .row > .field > .form-check > .input__checkbox-label > .input__checkbox-input', testData.Discount.ApplyDiscount)

                    if (testData.Discount.ApplyDiscount) {
                        customFunctions.SetCheckbox('[data-cy=affiliate-discount-apply-discount-to-affiliate-checkbox] > .row > .field > .form-check > .input__checkbox-label > .input__checkbox-input', testData.Discount.ApplyDiscountToAffliateProductOnly)

                        cy.get('[data-cy=affiliate-discount-type-select] > .field > :nth-child(1) > .input > .form-control').select(testData.Discount.DiscountType)
                        cy.get('[data-cy=affiliate-discount-value-select] > .row > .field > :nth-child(1) > .input > .form-control').clear().type(testData.Discount.DiscountValue)
                    }
                })

                cy.get('[data-cy=save-affiliate-btn]').click()

                cy.wait('@PutData').then((response) => {
                    cy.wrap(response.status).should('eq', 200)
                    modified = true
                })
            }
        }).then(() => {
            cy.wrap(modified).should('eq', true)
        })
}

function CheckTable(testData) {
    var found = false
    cy.get('.table-component__table__body > tr').each((tr) => {
            if (tr[0].cells[1].innerText == testData.Details.Name) {
                cy.wrap(tr[0].cells[0].innerText).should('eq', testData.Details.Code)
                cy.wrap(tr[0].cells[1].innerText).should('eq', testData.Details.Name)
                cy.wrap(tr[0].cells[2].innerText).should('eq', testData.Details.AffliateSearchOnly.toString())
                cy.wrap(tr[0].cells[2].innerText).should('eq', testData.Discount.ApplyDiscountToAffliateProductOnly.toString())
                found = true
            }
        }).then(() => {
            cy.wrap(found).should('eq', true)
        })
}