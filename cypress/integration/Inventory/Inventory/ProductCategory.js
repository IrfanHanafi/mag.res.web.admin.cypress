import * as customFunctions from '../../../support/functions.js'
import * as Data from '../../../fixtures/AdminSetup.json'

const productCategoryRoute = 'inventory/api/productcategory'

describe('Product Category Tests', function () {

    const Airport = Data.Enum.Airport

    const ProductCategory = Data.Inventory.Inventory.ProductCategory

    beforeEach(function () {
        cy.logInNoPre("inventory")

        cy.route('GET', productCategoryRoute).as('GetData')

        cy.get('[data-cy=sidebar-item-inventory]').click()
        cy.get('[data-cy=sidebar-item-inventory-product-categories]').click()

        cy.wait('@GetData')
    })

    describe('Filter Test', function () {
        it('Filter Name', function () {

            var found = false

            cy.get('.table-component__filter__field').clear().type(ProductCategory.Original.Name)

            cy.get('.table-component__table__body > tr').each((tr) => {
                if (tr[0].cells[1].innerText == ProductCategory.Original.Name) {
                    cy.wrap(tr[0].cells[0].innerText).should('eq', ProductCategory.Original.Airport)
                    cy.wrap(tr[0].cells[1].innerText).should('eq', ProductCategory.Original.Name)
                    found = true
                }
            }).then(() => {
                cy.wrap(found).should('eq', true)
            })
        })

        it('Filter Airport', function () {
            Object.keys(Airport).forEach(function (key) {
                cy.get('.table-component__filter__field').clear().type(Airport[key])

                cy.get('.table-component__table__body > tr').each((tr) => {
                    cy.wrap(tr[0].cells[0].innerText).should('eq', Airport[key])
                })
            })
        })

    })

    describe('Feature Test', function () {
        it('Modify Product Category', function () {
            Modify(ProductCategory.Modified, ProductCategory.Original.Name)
        })

        it('Checks Table for Modified Product Category', function () {
            CheckTable(ProductCategory.Modified)
        })


        it('Revert Product Category', function () {
            Modify(ProductCategory.Original, ProductCategory.Modified.Name)
        })

        it('Checks Table for Reverted Product Category', function () {
            CheckTable(ProductCategory.Original)
        })

        it('Checks Table for Reverted Product Category', function () {
            CheckTable(ProductCategory.Original)
        })
    })
})

function Modify(testData, search) {
    cy.route('GET', productCategoryRoute + '/*').as('GetProductCategory')
    cy.route('PUT', productCategoryRoute + '/*').as('PutData')
    var modified = false

    cy.get('.table-component__table__body > tr').each((tr) => {

        if (tr[0].cells[1].innerText == search) {

            cy.wrap(tr[0].cells[2]).contains('Change').click()

            cy.wait('@GetProductCategory')

            cy.get('[data-cy=product-category-priority-select] > .field > :nth-child(1) > .input > .form-control').select(testData.Priority)

            cy.get('[data-cy=product-category-name-input] > .row > .field > :nth-child(1) > .input > .form-control').clear().type(testData.Name)

            cy.get('[data-cy=product-category-description-source-select] > .field > :nth-child(1) > .input > .form-control').select(testData.DescriptionSource)

            cy.get('[data-cy=product-category-more-info-source-select] > .field > :nth-child(1) > .input > .form-control').select(testData.MoreInfoSource)

            cy.get('[data-cy=product-category-map-image-source-select] > .field > :nth-child(1) > .input > .form-control').select(testData.MapImageSource)

            customFunctions.SetCheckbox('.field > .form-check > .input__checkbox-label > .input__checkbox-input', testData.ProductBundleCategory)

            cy.get('[data-cy=product-category-description-input] > .editr--content').clear().type(testData.DescriptionSource)

            cy.get('[data-cy=product-category-more-info-input] > .editr--content').clear().type(testData.MoreInfoSource)

            customFunctions.SelectCheckbox('.checkbox-scroll-container', testData.AvailableProducts)

            cy.get('[data-cy=product-category-create-button-save]').click()

            cy.wait('@PutData').then((response) => {
                cy.wrap(response.status).should('eq', 200)
                modified = true
            })
        }
    }).then(() => {
        cy.wrap(modified).should('eq', true)
    })
}

function CheckTable(testData) {
    var found = false

    cy.get('.table-component__table__body > tr').each((tr) => {
        if (tr[0].cells[1].innerText == testData.Name) {
            cy.wrap(tr[0].cells[0].innerText).should('eq', testData.Airport)
            cy.wrap(tr[0].cells[1].innerText).should('eq', testData.Name)
            found = true
        }
    }).then(() => {
        cy.wrap(found).should('eq', true)
    })
}