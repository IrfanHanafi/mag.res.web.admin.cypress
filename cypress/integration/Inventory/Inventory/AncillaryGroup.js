import * as customFunctions from '../../../support/functions.js'
import * as Data from '../../../fixtures/AdminSetup.json'

const ancillaryGroupRoute = 'inventory/api/AddOnCosted/group'


describe('Ancillary Group Tests', function () {

    const AncillaryGroup = Data.Inventory.Inventory.AncillaryGroup

    beforeEach(function () {
        cy.logInNoPre("inventory")

        cy.route('GET', ancillaryGroupRoute).as('GetData')

        cy.get('[data-cy=sidebar-item-inventory]').click()
        cy.get('[data-cy=sidebar-item-inventory-add-on-groups]').click()

        cy.wait('@GetData')
    })

    describe('Filter Test', function () {
        it('Filter Name', function () {

            var found = false

            if (AncillaryGroup.Original.Active) {
                cy.get('#activeStates').select('Active')
            }
            else {
                cy.get('#activeStates').select('Inactive')
            }

            cy.wait(500)

            cy.get('#addOnGroupTableFilter').clear().type(AncillaryGroup.Original.Name)

            cy.get('tbody > tr').each((tr) => {
                if (tr[0].cells[0].innerText == AncillaryGroup.Original.Name) {
                    cy.wrap(tr[0].cells[0].innerText).should('eq', AncillaryGroup.Original.Name)
                    found = true
                }
            }).then(() => {
                cy.wrap(found).should('eq', true)
            })
        })

        it('Filter Description', function () {

            var found = false

            if (AncillaryGroup.Original.Active) {
                cy.get('#activeStates').select('Active')
            }
            else {
                cy.get('#activeStates').select('Inactive')
            }

            cy.wait(500)

            cy.get('#addOnGroupTableFilter').clear().type(AncillaryGroup.Original.Description)

            cy.get('tbody > tr').each((tr) => {
                if (tr[0].cells[1].innerText == AncillaryGroup.Original.Description) {
                    cy.wrap(tr[0].cells[1].innerText).should('eq', AncillaryGroup.Original.Description)
                    found = true
                }
            }).then(() => {
                cy.wrap(found).should('eq', true)
            })
        })

        it('Filter Active', function () {
            var activeStatus = ['Active', 'Inactive']
            var statusBool = ['true', 'false']

            cy.wrap(activeStatus).each((status, i) => {
                cy.get('#activeStates').select(status)

                cy.wait(500)

                cy.get('tbody > tr').each((tr) => {
                    cy.wrap(tr[0].cells[2].innerText).should('eq', statusBool[i])
                })
            })
        })

    })

    describe('Feature Test', function () {
        it('Modify Ancillary Group', function () {
            Modify(AncillaryGroup.Modified, AncillaryGroup.Original)
        })

        it('Checks Table for Modified Ancillary Group', function () {
            CheckTable(AncillaryGroup.Modified)
        })


        it('Revert Ancillary Group', function () {
            Modify(AncillaryGroup.Original, AncillaryGroup.Modified)
        })

        it('Checks Table for Reverted Ancillary Group', function () {
            CheckTable(AncillaryGroup.Original)
        })
    })
})

function Modify(testData, search) {
    cy.route('GET', ancillaryGroupRoute + '/*').as('GetAncillaryGroup')
    cy.route('PUT', ancillaryGroupRoute + '/*').as('PutData')

    var modified = false

    if (search.Active) {
        cy.get('#activeStates').select('Active')
    }
    else {
        cy.get('#activeStates').select('Inactive')
    }

    cy.wait(500)

    cy.get('tbody > tr').each((tr) => {

        if (tr[0].cells[0].innerText == search.Name) {

            cy.wrap(tr[0].cells[3]).contains('Change').click()

            cy.wait('@GetAncillaryGroup')

            cy.get('.input > .form-control').clear().type(testData.Name)

            cy.get('.editr--content').clear().type(testData.Description)

            customFunctions.SetCheckbox('.input__checkbox-input', testData.Active)

            cy.get('[data-cy=ancillary-group-form-button-submit]').click()

            cy.wait('@PutData').then((response) => {
                cy.wrap(response.status).should('eq', 200)
                modified = true
            })
        }
    }).then(() => {
        cy.wrap(modified).should('eq', true)
    })
}

function CheckTable(testData) {
    var found = false

    if (testData.Active) {
        cy.get('#activeStates').select('Active')
    }
    else {
        cy.get('#activeStates').select('Inactive')
    }

    cy.wait(500)

    cy.get('tbody > tr').each((tr) => {
        if (tr[0].cells[0].innerText == testData.Name) {
            cy.wrap(tr[0].cells[0].innerText).should('eq', testData.Name)
            cy.wrap(tr[0].cells[1].innerText).should('eq', testData.Description)
            cy.wrap(tr[0].cells[2].innerText).should('eq', testData.Active.toString())
            found = true
        }
    }).then(() => {
        cy.wrap(found).should('eq', true)
    })
}