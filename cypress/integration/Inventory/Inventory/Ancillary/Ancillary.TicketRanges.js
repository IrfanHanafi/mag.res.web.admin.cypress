import * as Data from '../../../../fixtures/AdminSetup.json'
import * as customFunctions from '../../../../support/functions'
import * as addFunctions from '../../../../support/AncillaryFunctions.js'

const ancillaryRoute = 'inventory/api/AddOnCosted'

const Ancillary = Data.Inventory.Inventory.Ancillary
const TicketRanges = {
    Original: Ancillary.Original.TicketRanges,
    Modified: Ancillary.Modified.TicketRanges
}
const Tab = Data.Enum.AncillaryTab

describe('Ancillary Ticket Ranges Tests', function () {

    beforeEach(function () {
        cy.logInNoPre("/inventory")

        cy.route('GET', ancillaryRoute).as('GetData')

        cy.get('[data-cy=sidebar-item-inventory]').click()
        cy.get('[data-cy=sidebar-item-inventory-add-ons]').click()

        cy.wait('@GetData')

        addFunctions.VisitTab(Ancillary.Original, Tab.TICKETRANGES)
    })

    it('Add Ticket Ranges', function () {
        AddTicketRanges(TicketRanges.Original)
    })

    it('Check Ticket Ranges', function () {
        CheckTicketRanges(TicketRanges.Original)
    })

    it('Modify Ticket Ranges', function () {
        ModifyTicketRanges(TicketRanges.Modified)
    })

    it('Check Modified Ticket Ranges', function () {
        CheckTicketRanges(TicketRanges.Modified)
    })

    it('Delete Ticket Ranges', function () {
        DeleteTicketRanges()
    })
})

function AddTicketRanges(testData) {
    cy.route('PUT', ancillaryRoute + '/*/ticketrange').as('PutTicketRange')

    cy.get('[data-cy=add-ons-ticket-ranges-input-ticket-minimum]').type(testData.TicketMinNo)
    cy.get('[data-cy=add-ons-ticket-ranges-input-ticket-maximum]').type(testData.TicketMaxNo)
    cy.get('[data-cy=add-ons-ticket-ranges-button-new-ticket-range]').click()
    cy.wait('@PutTicketRange').then((response) => {
        cy.wrap(response.status).should('eq', 200)
    })
}

function ModifyTicketRanges(testData) {
    cy.route('PUT', ancillaryRoute + '/*/ticketrange').as('PutTicketRange')

    cy.get('[aria-colindex="1"] > .form-control').clear().type(testData.TicketMinNo)
    cy.get('[aria-colindex="2"] > .form-control').clear().type(testData.TicketMaxNo)

    cy.get('[data-cy=add-ons-ticket-ranges-button-save]').click()

    cy.wait('@PutTicketRange').then((response) => {
        cy.wrap(response.status).should('eq', 200)
    })
}

function CheckTicketRanges(testData) {
    cy.get('[aria-colindex="1"] > .form-control').then((content) => {
        cy.wrap(content[0].value).should('eq', testData.TicketMinNo.toString())
    })
    cy.get('[aria-colindex="2"] > .form-control').then((content) => {
        cy.wrap(content[0].value).should('eq', testData.TicketMaxNo.toString())
    })
}

function DeleteTicketRanges() {
    cy.route('DELETE', ancillaryRoute + '/*/ticketrange/*/range/*').as('DeleteTicketRange')
    cy.get('[data-cy=add-ons-ticket-ranges-table-container] > tbody > tr').then((tr) => {
        cy.wrap(tr[0].cells[2]).contains('Delete').click()
        cy.wait(500)
        cy.wait('@DeleteTicketRange').then((response) => {
            cy.wrap(response.status).should('eq', 200)
        })
    })
}