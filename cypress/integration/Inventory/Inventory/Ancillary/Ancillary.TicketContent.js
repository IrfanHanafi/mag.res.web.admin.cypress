import * as Data from '../../../../fixtures/AdminSetup.json'
import * as customFunctions from '../../../../support/functions'
import * as addFunctions from '../../../../support/AncillaryFunctions.js'

const ancillaryRoute = 'inventory/api/AddOnCosted'

const Ancillary = Data.Inventory.Inventory.Ancillary
const TicketContent = {
    Original: Ancillary.Original.TicketContent,
    Modified: Ancillary.Modified.TicketContent
}
const Tab = Data.Enum.AncillaryTab

describe('Ancillary Ticket Content Tests', function () {

    beforeEach(function () {
        cy.logInNoPre("/inventory")

        cy.route('GET', ancillaryRoute).as('GetData')

        cy.get('[data-cy=sidebar-item-inventory]').click()
        cy.get('[data-cy=sidebar-item-inventory-add-ons]').click()

        cy.wait('@GetData')

        addFunctions.VisitTab(Ancillary.Original, Tab.TICKETCONTENT)
    })

    it('Modify Ticket Content', function () {
        ModifyTicketContent(TicketContent.Modified)
    })

    it('Check Modified Ticket Content', function () {
        CheckTicketContent(TicketContent.Modified)
    })

    it('Revert Ticket Content', function () {
        ModifyTicketContent(TicketContent.Original)
    })

    it('Check Reverted Ticket Content', function () {
        CheckTicketContent(TicketContent.Original)
    })
})

function ModifyTicketContent(testData) {
    cy.route('POST', ancillaryRoute + '/*/ticket').as('PostTicketContent')
    cy.wait(500)
    cy.get('[data-cy=add-ons-ticket-content-input-title]').clear().type(testData.Title)
    cy.get('[data-cy=add-ons-ticket-content-input-body-text] > .editr--content').clear().type(testData.BodyText)
    cy.get('[data-cy=add-ons-ticket-content-select-barcode-type]').select(testData.BarcodeType)
    cy.get('[data-cy=add-ons-ticket-content-input-barcode-text]').clear().type(testData.BarcodeText)
    cy.get('[data-cy=add-ons-ticket-content-select-pdf-version]').select(testData.PDFVersion)
    cy.get('[data-cy=add-ons-ticket-content-button-save]').click()
    cy.wait('@PostTicketContent').then((response) => {
        cy.wrap(response.status).should('eq', 200)
    })
}

function CheckTicketContent(testData) {
    cy.route('POST', ancillaryRoute + '/*/ticket').as('PostTicketContent')
    cy.wait(500)

    cy.get('[data-cy=add-ons-ticket-content-input-title]').then((content) => {
        cy.wrap(content[0].value).should('eq', testData.Title)
    })

    cy.get('[data-cy=add-ons-ticket-content-input-body-text] > .editr--content').then((content) => {
        cy.wrap(content[0].innerText).should('eq', testData.BodyText)
    })

    cy.get('[data-cy=add-ons-ticket-content-select-barcode-type]').then((content) => {
        cy.wrap(content[0].selectedOptions[0].label).should('eq', testData.BarcodeType)
    })

    cy.get('[data-cy=add-ons-ticket-content-input-barcode-text]').then((content) => {
        cy.wrap(content[0].value).should('eq', testData.BarcodeText)
    })

    cy.get('[data-cy=add-ons-ticket-content-select-pdf-version]').then((content) => {
        cy.wrap(content[0].selectedOptions[0].label).should('eq', testData.PDFVersion)
    })
}