import * as customFunctions from '../../../../support/functions'
import * as Data from '../../../../fixtures/AdminSetup.json'
import * as addFunctions from '../../../../support/AncillaryFunctions.js'

const ancillaryRoute = 'inventory/api/AddOnCosted'

const Ancillary = Data.Inventory.Inventory.Ancillary

describe('Ancillary Detail Tests', function () {

    beforeEach(function () {
        cy.logInNoPre("/inventory")

        cy.route('GET', ancillaryRoute).as('GetData')

        cy.get('[data-cy=sidebar-item-inventory]').click()
        cy.get('[data-cy=sidebar-item-inventory-add-ons]').click()

        cy.wait('@GetData')
    })

    it('Modify Ancillary', function () {
        ModifyDetail(Ancillary.Modified, Ancillary.Original)
    })

    it('Checks Table for Modified Ancillary', function () {
        CheckDetail(Ancillary.Modified.Detail)
    })

    it('Revert Ancillary', function () {
        ModifyDetail(Ancillary.Original, Ancillary.Modified)
    })

    it('Checks Table for Reverted Ancillary', function () {
        CheckDetail(Ancillary.Original.Detail)
    })

})

function ModifyDetail(testData, search) {

    cy.route('PUT', ancillaryRoute + '/*').as('PutData')

    var modified = false

    if (search.Detail.Active) {
        cy.get('[data-cy=add-on-select-active]').select('Active')
    }
    else {
        cy.get('[data-cy=add-on-select-active]').select('Inactive')
    }

    cy.get('tbody > tr').each((tr) => {
        if (tr[0].cells[0].innerText == search.Detail.Name) {

            addFunctions.RouteSetup()

            cy.wrap(tr[0].cells[7]).contains('Change').click()

            addFunctions.RouteWait()

            cy.get('[data-cy=add-ons-detail-input-name]').clear().type(testData.Detail.Name)

            cy.get('[data-cy=add-ons-detail-select-airport]').select(testData.Detail.Airport)

            cy.get('[data-cy=add-ons-detail-select-terminal]').select(testData.Detail.Terminal)

            cy.get('[data-cy=add-ons-detail-select-add-on-group]').select(testData.Detail.AncillaryGroup)

            cy.get('[data-cy=add-ons-detail-select-email-template]').select(testData.Detail.EmailTemplate)

            cy.get('[data-cy=add-ons-detail-input-max-bookable-quantity]').clear().type(testData.Detail.MaxBookableQuantity)

            cy.get('[data-cy=add-ons-detail-input-offset]').clear().type(testData.Detail.Offset)

            customFunctions.SetCheckbox('[data-cy=add-ons-detail-checkbox-booking-page] > .input__checkbox-input', testData.Detail.ShowAsBookingPage)

            cy.get('[data-cy=add-ons-detail-select-priority]').select(testData.Detail.Priority)

            cy.get('[data-cy=add-ons-detail-select-product-rule]').select(testData.Detail.ProductRule)

            customFunctions.SetCheckbox('[data-cy=add-ons-detail-checkbox-crm-intergration] > .input__checkbox-input', testData.Detail.CRMIntergration)

            cy.get('[data-cy=add-ons-detail-input-description] > .editr--content').clear().type(testData.Detail.Description)

            cy.get('[data-cy=add-ons-detail-input-long-description] > .editr--content').clear().type(testData.Detail.LongDescription)

            cy.get('[data-cy=add-ons-detail-input-email-description] > .editr--content').clear().type(testData.Detail.EmailDescription)

            customFunctions.SetCheckbox('[data-cy=add-ons-detail-checkbox-booking-search] > .input__checkbox-input', testData.Detail.IncludeBookingSearch)

            customFunctions.SetCheckbox('[data-cy=add-ons-detail-checkbox-extras-only] > .input__checkbox-input', testData.Detail.ExtrasOnlySearch)

            cy.get('[data-cy=add-ons-detail-select-tax-name]').select(testData.Detail.TaxName)

            customFunctions.SetCheckbox('[data-cy=add-ons-detail-checkbox-active] > .input__checkbox-input', testData.Detail.Active)

            customFunctions.SetCheckbox('[data-cy=add-ons-detail-checkbox-can-amend] > .input__checkbox-input', testData.Detail.CanAmend)

            cy.wait(500)

            customFunctions.SetCheckbox('[data-cy=add-ons-detail-checkbox-can-cancel] > .input__checkbox-input', testData.Detail.CanCancel)

            cy.get('[data-cy=add-ons-detail-select-duration-of-stay]').select(testData.Detail.DurationOfStay)

            cy.get('[data-cy=add-ons-detail-select-date-type]').select(testData.Detail.DateType)

            cy.get('[data-cy=add-ons-detail-select-interval]').select(testData.Detail.Interval)

            cy.get('[data-cy=add-ons-detail-select-capacity-pool]').select(testData.Detail.CapacityPool)

            cy.get('[data-cy=add-ons-detail-button-save]').click()

            cy.wait('@PutData').then((response) => {
                cy.wrap(response.status).should('eq', 200)
                modified = true
            })
        }
    }).then(() => {
        cy.wrap(modified).should('eq', true)
    })
}

function CheckDetail(testData) {
    var found = false

    if (testData.Active) {
        cy.get('[data-cy=add-on-select-active]').select('Active')
    }
    else {
        cy.get('[data-cy=add-on-select-active]').select('Inactive')
    }

    cy.get('tbody > tr').each((tr) => {
        if (tr[0].cells[0].innerText == testData.Name) {
            cy.wrap(tr[0].cells[0].innerText).should('eq', testData.Name)
            cy.wrap(tr[0].cells[1].innerText).should('eq', testData.Airport)
            cy.wrap(tr[0].cells[2].innerText).should('eq', testData.Description)
            cy.wrap(tr[0].cells[3].innerText).should('eq', testData.IncludeBookingSearch.toString())
            cy.wrap(tr[0].cells[4].innerText).should('eq', testData.ExtrasOnlySearch.toString())
            cy.wrap(tr[0].cells[5].innerText).should('eq', testData.CanCancel.toString())
            cy.wrap(tr[0].cells[6].innerText).should('eq', testData.Active.toString())
            found = true
        }
    }).then(() => {
        cy.wrap(found).should('eq', true)
    })
}