import * as Data from '../../../../fixtures/AdminSetup.json'
import * as customFunctions from '../../../../support/functions'
import * as addFunctions from '../../../../support/AncillaryFunctions.js'

const ancillaryRoute = 'inventory/api/AddOnCosted'
const ancillaryGroupRoute = 'inventory/api/AddOnCosted/group'
const durationStayTypeRoute = 'inventory/api/AddOnCosted/durationOfStayType'
const pricingBasisRoute = 'inventory/api/AddOnCosted/pricingBasis'
const availabilityTypeRoute = 'inventory/api/AddOnCosted/availabilityType'
const intervalTypeRoute = 'inventory/api/AddOnCosted/intervalType'
const addOnCostedTypeRoute = 'inventory/api/AddOnCosted/addoncostedtype'
const addOnRoute = 'inventory/api/addOn'
const emailTemplateRoute = 'inventory/api/email/template'
const vatCodeRoute = 'inventory/api/product/vatcode'
const airportRoute = 'inventory/api/airport'
const pageContentReferenceRoute = 'inventory/api/reference/PageContentFormFields'
const bardcodeReferenceRoute = 'inventory/api/reference/barcode'
const capacityPoolRoute = 'inventory/api/capacitypool'
const tariffRoute = 'inventory/api/tariff'
const addOnBookingFieldRoute = 'inventory/api/addOnBookingField'
const ancillaryFieldTypeReferenceRoute = 'inventory/api/reference/AddOnCostedFieldTypes'

const Ancillary = Data.Inventory.Inventory.Ancillary
const Offerline = {
    Original: Ancillary.Original.Offerline,
    Modified: Ancillary.Modified.Offerline
}
const Tab = Data.Enum.AncillaryTab

describe('Ancillary Offerline Tests', function () {

    beforeEach(function () {
        cy.logInNoPre("/inventory")

        cy.route('GET', ancillaryRoute).as('GetData')

        cy.get('[data-cy=sidebar-item-inventory]').click()
        cy.get('[data-cy=sidebar-item-inventory-add-ons]').click()

        cy.wait('@GetData')

        addFunctions.VisitTab(Ancillary.Original, Tab.OFFERLINES)
    })

    describe('Tabs Test', function () {

        it('Modify Offerlines', function () {
            ModifyOfferline(Offerline.Modified, Offerline.Original)
        })

        it('Check Modified Offerlines', function () {
            CheckOfferline(Offerline.Modified)
        })

        it('Revert Offerlines', function () {
            ModifyOfferline(Offerline.Original, Offerline.Modified)
        })

        it('Check Reverted Offerlines', function () {
            CheckOfferline(Offerline.Original)
        })
    })
})

function ModifyOfferline(testData, search) {
    cy.route('PUT', ancillaryRoute + '/*/OfferLine/*').as('PutOfferline')

    var modified = false
    cy.get('[data-cy=add-ons-offer-lines-table-container] > .table-component__table-wrapper > .table-component__table > .table-component__table__body > tr ').each((tr) => {
        debugger
        if (tr[0].cells[2].innerText == search.Name) {
            var FromDate = customFunctions.GetDate(testData.DateFrom)
            var ToDate = customFunctions.GetDate(testData.DateTo)

            cy.wrap(tr[0].cells[9]).contains('Edit').click()

            cy.wait('@GetAddOnOfferline')
            cy.wait('@GetAncillary')
            cy.wait('@GetTariff')

            cy.route('GET', tariffRoute + '/*/ladder').as('GetTariffLadder')

            customFunctions.SetDate('#dateFrom', FromDate)
            customFunctions.SetDate('#dateTo', ToDate)

            cy.get('[data-cy=add-ons-offer-lines-input-name]').clear().type(testData.Name)
            cy.get('.editr--content').clear().type(testData.Description)
            cy.get('[data-cy=add-ons-offer-lines-input-provider-code]').clear().type(testData.ProviderCode)
            cy.get('[data-cy=add-ons-offer-lines-select-tariff]').select(testData.Tariff)

            cy.wait('@GetTariffLadder')

            cy.get('[data-cy=add-ons-offer-lines-select-pricing-ladder]').select(testData.DefaultPricingLadder)
            cy.get('[data-cy=add-ons-offer-lines-input-allocate]').clear().type(testData.Allocate)
            cy.get('[data-cy=add-ons-offer-lines-input-max-quantity]').clear().type(testData.MaxQuantity)
            cy.get('[data-cy=add-ons-offer-lines-select-priority-input]').select(testData.Priority)

            customFunctions.SetCheckbox('[data-cy=add-ons-offer-lines-checkbox-mandatory] > .input__checkbox-input', testData.Mandatory)
            customFunctions.SetCheckbox('[data-cy=add-ons-offer-lines-checkbox-active] > .input__checkbox-input', testData.Active)

            cy.wait(500)

            cy.get('[data-cy=add-ons-offer-lines-button-save]').click()

            cy.wait('@PutOfferline').then((response) => {
                cy.wrap(response.status).should('eq', 200)
                modified = true
            })
        }
    }).then(() => {
        cy.wrap(modified).should('eq', true)
    })

    return modified
}

function CheckOfferline(testData) {

    var modified = false

    var FromDate = customFunctions.GetDate(testData.DateFrom)
    var ToDate = customFunctions.GetDate(testData.DateTo)

    FromDate = FromDate.format('DD MMM YYYY')
    ToDate = ToDate.format('DD MMM YYYY')

    cy.get('[data-cy=add-ons-offer-lines-table-container] > .table-component__table-wrapper > .table-component__table > .table-component__table__body > tr ').each((tr) => {
        if (tr[0].cells[2].innerText == testData.Name) {
            debugger
            cy.wrap(tr[0].cells[0].innerText).should('eq', FromDate)
            cy.wrap(tr[0].cells[1].innerText).should('eq', ToDate)
            cy.wrap(tr[0].cells[2].innerText).should('eq', testData.Name)
            cy.wrap(tr[0].cells[3].innerText).should('eq', testData.MaxQuantity.toString())
            cy.wrap(tr[0].cells[4].innerText).should('eq', testData.Active.toString())
            cy.wrap(tr[0].cells[5].innerText).should('eq', testData.Tariff)
            cy.wrap(tr[0].cells[6].innerText).should('eq', testData.DefaultPricingLadder)
            cy.wrap(tr[0].cells[7].innerText).should('eq', testData.Priority)
            modified = true
        }
    }).then(() => {
        cy.wrap(modified).should('eq', true)
    })
}
