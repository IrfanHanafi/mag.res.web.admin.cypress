import * as Data from '../../../../fixtures/AdminSetup.json'
import * as addFunctions from '../../../../support/AncillaryFunctions.js'

const ancillaryRoute = 'inventory/api/AddOnCosted'


const Ancillary = Data.Inventory.Inventory.Ancillary
const AmendmentRules = {
    Original: Ancillary.Original.AmendmentRules,
    Modified: Ancillary.Modified.AmendmentRules
}
const Tab = Data.Enum.AncillaryTab

describe('Ancillary Amendment Rules Tests', function () {

    beforeEach(function () {
        cy.logInNoPre("/inventory")

        cy.route('GET', ancillaryRoute).as('GetData')

        cy.get('[data-cy=sidebar-item-inventory]').click()
        cy.get('[data-cy=sidebar-item-inventory-add-ons]').click()

        cy.wait('@GetData')

        addFunctions.VisitTab(Ancillary.Original, Tab.AMENDMENTRULES)
    })

    it('Add Amendment Rules', function () {
        cy.route('POST', ancillaryRoute + '/*/amendmentrule').as('PostRule')
        addFunctions.AddRules(AmendmentRules.Original, Tab.AMENDMENTRULES)
    })

    it('Check Amendment Rules', function () {
        addFunctions.CheckRules(AmendmentRules.Original, Tab.AMENDMENTRULES)
    })

    it('Modify Amendment Rules', function () {
        cy.route('PUT', ancillaryRoute + '/*/amendmentrule/*').as('PutRule')
        addFunctions.ModifyRules(AmendmentRules.Modified, Tab.AMENDMENTRULES)
    })

    it('Check Amendment Rules', function () {
        addFunctions.CheckRules(AmendmentRules.Modified, Tab.AMENDMENTRULES)
    })

    it('Delete Amendment Rules', function () {
        cy.route('DELETE', ancillaryRoute + '/*/amendmentrule/*').as('DeleteRule')
        addFunctions.DeleteRules(Tab.AMENDMENTRULES)
    })
})