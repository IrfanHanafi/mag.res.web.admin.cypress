import * as Data from '../../../../fixtures/AdminSetup.json'
import * as customFunctions from '../../../../support/functions'
import * as addFunctions from '../../../../support/AncillaryFunctions.js'

const ancillaryRoute = 'inventory/api/AddOnCosted'
const addOnBookingFieldRoute = 'inventory/api/addOnBookingField'

const Ancillary = Data.Inventory.Inventory.Ancillary
const BookingFields = {
    Original: Ancillary.Original.AncillaryBookingFields,
    Modified: Ancillary.Modified.AncillaryBookingFields
}
const Tab = Data.Enum.AncillaryTab

describe('Ancillary Booking Field Tests', function () {

    beforeEach(function () {
        cy.logInNoPre("/inventory")

        cy.route('GET', ancillaryRoute).as('GetData')

        cy.get('[data-cy=sidebar-item-inventory]').click()
        cy.get('[data-cy=sidebar-item-inventory-add-ons]').click()

        cy.wait('@GetData')

        addFunctions.VisitTab(Ancillary.Original, Tab.ANCILLARYBOOKINGFIELDS)

        customFunctions.SetCheckbox('[data-cy=add-ons-booking-fields-checkbox-enable-specific-booking-field] > .input__checkbox-input', true)
    })

    it('Add Booking Fields', function () {
        AddBookingFields(BookingFields.Original)
    })

    it('Check Booking Fields', function () {
        CheckBookingFields(BookingFields.Original)
    })

    it('Modify Booking Fields', function () {
        ModifyBookingFields(BookingFields.Modified)
    })

    it('Check Modified Booking Fields', function () {
        CheckBookingFields(BookingFields.Modified)
    })

    it('Delete Booking Fields', function () {
        DeleteBookingFields(BookingFields.Modified)
    })
})

function AddBookingFields(testData) {

    cy.route('POST', addOnBookingFieldRoute).as('PostBookingField')

    cy.get('[data-cy=add-ons-booking-fields-select-booking-field]').select(testData.Name)
    cy.get('[data-cy=add-ons-booking-fields-button-add]').click()

    cy.wait('@PostBookingField').then((response) => {
        cy.wrap(response.status).should('eq', 201)
    })
}

function ModifyBookingFields(testData) {
    var modified = false
    cy.route('PUT', addOnBookingFieldRoute + '/*').as('PutBookingField')

    cy.get('[data-cy=add-ons-booking-fields-table-container] > .table-component__table-wrapper > .table-component__table > .table-component__table__body > tr').each((tr) => {
        if (tr[0].cells[0].children[0].value == testData.Name) {

            customFunctions.SetCheckbox('[data-cy=' + tr[0].cells[1].children[0].dataset.cy + '] > .input__checkbox-input', testData.Mandatory)

            cy.wait('@PutBookingField').then((response) => {
                cy.wrap(response.status).should('eq', 204)
                modified = true
            })
        }
    }).then(() => {
        cy.wrap(modified).should('eq', true)
    })
}

function CheckBookingFields(testData) {

    var modified = false

    cy.get('[data-cy=add-ons-booking-fields-table-container] > .table-component__table-wrapper > .table-component__table > .table-component__table__body > tr').each((tr) => {
        debugger
        if (tr[0].cells[0].children[0].value == testData.Name) {
            cy.wrap(tr[0].cells[0].children[0].value).should('eq', testData.Name)
            cy.wrap(tr[0].cells[1].children[0].children[1].control.checked).should('eq', testData.Mandatory)
            modified = true
        }
    }).then(() => {
        cy.wrap(modified).should('eq', true)
    })
}

function DeleteBookingFields(testData) {
    var modified = false

    cy.route('DELETE', addOnBookingFieldRoute + '/*').as('DeleteBookingField')

    cy.get('[data-cy=add-ons-booking-fields-table-container] > .table-component__table-wrapper > .table-component__table > .table-component__table__body > tr').each((tr) => {
        if (tr[0].cells[0].children[0].value == testData.Name) {
            cy.wrap(tr[0].cells[2]).contains('Delete').click()
            cy.get('.modal-footer > .btn').click()

            cy.wait('@DeleteBookingField').then((response) => {
                cy.wrap(response.status).should('eq', 204)
                modified = true
            })

        }
    }).then(() => {
        cy.wrap(modified).should('eq', true)
    })
}