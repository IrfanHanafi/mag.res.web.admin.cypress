import * as Data from '../../../../fixtures/AdminSetup.json'

const ancillaryRoute = 'inventory/api/AddOnCosted'

const Ancillary = Data.Inventory.Inventory.Ancillary

describe('Ancillary Filter Tests', function () {

    beforeEach(function () {
        cy.logInNoPre("/inventory")

        cy.route('GET', ancillaryRoute).as('GetData')

        cy.get('[data-cy=sidebar-item-inventory]').click()
        cy.get('[data-cy=sidebar-item-inventory-add-ons]').click()

        cy.wait('@GetData')
    })

        it('Filter Name', function () {
            var found = false

            if (Ancillary.Original.Detail.Active) {
                cy.get('[data-cy=add-on-select-active]').select('Active')
            }
            else {
                cy.get('[data-cy=add-on-select-active]').select('Inactive')
            }

            cy.get('[data-cy=add-on-input-filter]').clear().type(Ancillary.Original.Detail.Name)

            cy.get('tbody > tr').each((tr) => {
                if (tr[0].cells[0].innerText == Ancillary.Original.Detail.Name) {
                    cy.wrap(tr[0].cells[0].innerText).should('eq', Ancillary.Original.Detail.Name)
                    found = true
                }
            }).then(() => {
                cy.wrap(found).should('eq', true)
            })
        })

        it('Filter Description', function () {

            var found = false

            if (Ancillary.Original.Detail.Active) {
                cy.get('[data-cy=add-on-select-active]').select('Active')
            }
            else {
                cy.get('[data-cy=add-on-select-active]').select('Inactive')
            }

            cy.get('[data-cy=add-on-input-filter]').clear().type(Ancillary.Original.Detail.Description)
            cy.wait(500)

            cy.get('tbody > tr').each((tr) => {
                if (tr[0].cells[2].innerText == Ancillary.Original.Detail.Description) {
                    cy.wrap(tr[0].cells[2].innerText).should('eq', Ancillary.Original.Detail.Description)
                    found = true
                }
            }).then(() => {
                cy.wrap(found).should('eq', true)
            })
        })

        it('Filter Active', function () {
            var activeStatus = ['Active', 'Inactive']
            var statusBool = ['true', 'false']
            var found = [false, false]

            cy.wrap(activeStatus).each((status, i) => {
                cy.get('[data-cy=add-on-select-active]').select(status)
                cy.wait(500)

                cy.get('tbody > tr').each((tr) => {
                    if (tr[0].cells.length == 8) {
                        cy.wrap(tr[0].cells[6].innerText).should('eq', statusBool[i])
                        found[i] = true
                    }
                }).then(() => {
                    cy.log('Found Items?')
                    cy.wrap(found[i]).should('eq', true)
                })
            })
        })
})
