import * as Data from '../../../../fixtures/AdminSetup.json'
import * as addFunctions from '../../../../support/AncillaryFunctions.js'

const ancillaryRoute = 'inventory/api/AddOnCosted'


const Ancillary = Data.Inventory.Inventory.Ancillary
const CancellationRules = {
    Original: Ancillary.Original.CancellationRules,
    Modified: Ancillary.Modified.CancellationRules
}
const Tab = Data.Enum.AncillaryTab

describe('Ancillary Cancellation Rules Tests', function () {

    beforeEach(function () {
        cy.logInNoPre("/inventory")

        cy.route('GET', ancillaryRoute).as('GetData')

        cy.get('[data-cy=sidebar-item-inventory]').click()
        cy.get('[data-cy=sidebar-item-inventory-add-ons]').click()

        cy.wait('@GetData')

        addFunctions.VisitTab(Ancillary.Original, Tab.CANCELLATIONRULES)
    })

    it('Add Cancellation Rules', function () {
        cy.route('POST', ancillaryRoute + '/*/cancellationrule').as('PostRule')
        addFunctions.AddRules(CancellationRules.Original, Tab.CANCELLATIONRULES)
    })

    it('Check Cancellation Rules', function () {
        addFunctions.CheckRules(CancellationRules.Original, Tab.CANCELLATIONRULES)
    })

    it('Modify Cancellation Rules', function () {
        cy.route('PUT', ancillaryRoute + '/*/cancellationrule/*').as('PutRule')
        addFunctions.ModifyRules(CancellationRules.Modified, Tab.CANCELLATIONRULES)
    })

    it('Check Cancellation Rules', function () {
        addFunctions.CheckRules(CancellationRules.Modified, Tab.CANCELLATIONRULES)
    })

    it('Delete Cancellation Rules', function () {
        cy.route('DELETE', ancillaryRoute + '/*/cancellationrule/*').as('DeleteRule')
        addFunctions.DeleteRules(Tab.CANCELLATIONRULES)
    })
})