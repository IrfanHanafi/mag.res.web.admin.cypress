import * as Data from '../../../../fixtures/AdminSetup.json'
import * as customFunctions from '../../../../support/functions'
import * as addFunctions from '../../../../support/AncillaryFunctions.js'

const ancillaryRoute = 'inventory/api/AddOnCosted'

const Ancillary = Data.Inventory.Inventory.Ancillary
const CustomFields = {
    Original: Ancillary.Original.AncillaryCustomFields,
    Modified: Ancillary.Modified.AncillaryCustomFields
}
const Tab = Data.Enum.AncillaryTab

describe('Ancillary Custom Field Tests', function () {

    beforeEach(function () {
        cy.logInNoPre("/inventory")

        cy.route('GET', ancillaryRoute).as('GetData')

        cy.get('[data-cy=sidebar-item-inventory]').click()
        cy.get('[data-cy=sidebar-item-inventory-add-ons]').click()

        cy.wait('@GetData')

        addFunctions.VisitTab(Ancillary.Original, Tab.ANCILLARYCUSTOMFIELDS)
    })

    it('Add Custom Fields', function () {
        AddCustomFields(CustomFields.Original)
    })

    it('Check Custom Fields', function () {
        CheckCustomFields(CustomFields.Original)
    })

    it('Modify Custom Fields', function () {
        ModifyCustomFields(CustomFields.Modified)
    })

    it('Check Modified Custom Fields', function () {
        CheckCustomFields(CustomFields.Modified)
    })

    it('Delete Custom Fields', function () {
        DeleteCustomFields(CustomFields.Modified)
    })
})

function AddCustomFields(testData) {
    cy.route('POST', ancillaryRoute + '/*/customField').as('PostCustomFields')

    cy.get('[data-cy=add-ons-custom-fields-select-price-basis]').select(testData.Label + ' - ' + testData.FieldType)

    cy.get('[data-cy=add-ons-custom-fields-button-add]').click()
    cy.wait(500)

    FillCustomFields(testData)

    cy.wait('@PostCustomFields').then((response) => {
        cy.wrap(response.status).should('eq', 200)
    })
}

function ModifyCustomFields(testData) {
    var modified = false

    cy.route('PUT', ancillaryRoute + '/*/customField/*').as('PutCustomFields')

    cy.get('[data-cy=add-ons-custom-fields-table-container] > .table-component__table-wrapper > .table-component__table > .table-component__table__body > tr').each((tr) => {
        if (tr[0].cells[0].innerText == testData.Label) {
            cy.wrap(tr[0].cells[6]).contains('Edit').click()
            cy.wait(500)

            FillCustomFields(testData)

            cy.wait('@PutCustomFields').then((response) => {
                cy.wrap(response.status).should('eq', 200)
                modified = true
            })
        }
    }).then(() => {
        cy.wrap(modified).should('eq', true)
    })

}

function DeleteCustomFields(testData) {
    var modified = false

    cy.route('DELETE', ancillaryRoute + '/*/customField/*').as('DeleteCustomFields')

    cy.get('[data-cy=add-ons-custom-fields-table-container] > .table-component__table-wrapper > .table-component__table > .table-component__table__body > tr').each((tr) => {
        if (tr[0].cells[0].innerText == testData.Label) {
            cy.wrap(tr[0].cells[6]).contains('Delete').click()
            cy.get('#addOnCustomFieldConfirmDeleteModal___BV_modal_footer_ > .btn-danger').click()

            cy.wait('@DeleteCustomFields').then((response) => {
                cy.wrap(response.status).should('eq', 200)
                modified = true
            })
        }
    }).then(() => {
        cy.wrap(modified).should('eq', true)
    })
}

function CheckCustomFields(testData) {
    var modified = false

    cy.get('[data-cy=add-ons-custom-fields-table-container] > .table-component__table-wrapper > .table-component__table > .table-component__table__body > tr').each((tr) => {
        if (tr[0].cells[0].innerText == testData.Label) {
            cy.wrap(tr[0].cells[0].innerText).should('eq', testData.Label)
            cy.wrap(tr[0].cells[1].innerText).should('eq', testData.FieldType)
            cy.wrap(tr[0].cells[2].innerText).should('eq', testData.Position.toString())
            debugger
            cy.wrap(tr[0].cells[3].children[0].children[1].control.checked).should('eq', testData.Mandatory)
            cy.wrap(tr[0].cells[4].children[0].children[1].control.checked).should('eq', testData.PerCustomer)
            cy.wrap(tr[0].cells[5].children[0].children[1].control.checked).should('eq', testData.Visible)
            modified = true
        }
    }).then(() => {
        cy.wrap(modified).should('eq', true)
    })
}

function FillCustomFields(testData) {
    cy.get('[data-cy=custom-field-modal-input-position] > .row > .field > :nth-child(1) > .input > .form-control').clear().type(testData.Position)

    customFunctions.SetCheckbox('[data-cy=custom-field-modal-checkbox-per-customer] > .input__checkbox-input', testData.PerCustomer)
    customFunctions.SetCheckbox('[data-cy=custom-field-modal-checkbox-mandatory] > .input__checkbox-input', testData.Mandatory)
    customFunctions.SetCheckbox('[data-cy=custom-field-modal-checkbox-visible] > .input__checkbox-input', testData.Visible)

    cy.get('#addon-costed-custom-field-modal___BV_modal_footer_ > .btn').click()
}