import * as Data from '../../../../fixtures/AdminSetup.json'

const productRulesRoute = 'inventory/api/productRule'
const airportRoute = '/inventory/api/airport'
const leadTimeTypeRoute = '/inventory/api/productRule/leadtimetype'

const ProductRule = Data.Inventory.Inventory.ProductRule

describe('Product Rules Tests', function () {

    beforeEach(function () {
        cy.logInNoPre("inventory")

        cy.route('GET', productRulesRoute).as('GetData')
        cy.route('GET', airportRoute).as('GetAirport')
        cy.route('GET', leadTimeTypeRoute).as('GetLeadTimeType')

        cy.get('[data-cy=sidebar-item-inventory]').click()
        cy.get('[data-cy=sidebar-item-inventory-product-rules]').click()

        cy.wait('@GetData')
    })

    describe('Filter Test', function () {
        it('Filters Name', function () {
            var found = false
            cy.get('.table-component__filter__field').clear().type(ProductRule.Filter.Name)
            cy.get('.table-component__table__body > tr').each((tr, i) => {
                if (tr[0].cells[1].innerText == ProductRule.Filter.Name) {
                    expect(tr[0].cells[1].innerText).to.equal(ProductRule.Filter.Name)
                    found = true
                }
            }).then(() => {
                cy.wrap(found).should('eq', true)
            })

        })

        it('Filters Airport', function () {
            var found = false
            cy.get('.table-component__filter__field').clear().type(ProductRule.Filter.Airport)
            cy.get('.table-component__table__body > tr').each((tr, i) => {
                if (tr[0].cells[0].innerText == ProductRule.Filter.Airport) {
                    expect(tr[0].cells[0].innerText).to.equal(ProductRule.Filter.Airport)
                    found = true
                }
            }).then(() => {
                cy.wrap(found).should('eq', true)
            })
        })
    })
})

