import * as customFunctions from '../../../../support/functions.js'
import * as Data from '../../../../fixtures/AdminSetup.json'

const productRulesRoute = 'inventory/api/productRule'
const airportRoute = '/inventory/api/airport'
const leadTimeTypeRoute = '/inventory/api/productRule/leadtimetype'

const ArrivalRestriction = 'ArrivalRestriction'
const DepartureRestriction = 'DepartureRestriction'
const BookingRestriction = 'BookingRestriction'

const ProductRule = Data.Inventory.Inventory.ProductRule

describe('Product Rules Tests', function () {

    beforeEach(function () {
        cy.logInNoPre("inventory")

        cy.route('GET', productRulesRoute).as('GetData')
        cy.route('GET', airportRoute).as('GetAirport')
        cy.route('GET', leadTimeTypeRoute).as('GetLeadTimeType')

        cy.get('[data-cy=sidebar-item-inventory]').click()
        cy.get('[data-cy=sidebar-item-inventory-product-rules]').click()

        cy.wait('@GetData')
    })

    describe('Feature Test', function () {

        it('Create Product Rules', function () {

            cy.route('POST', productRulesRoute).as('PostData')

            cy.get('[data-cy=product-rule-create-btn]').click()

            FillForm(ProductRule.Original, false)

            cy.wait('@PostData').then((response) => {
                cy.wrap(response.status).should('eq',200)
            })
        })

        it('Checks Table for Created Product Rules', function () {
            CheckTable(ProductRule.Original)
        })

        describe('Modify Product Rules', function () {
            it('Modify Fields', function () {
                cy.route('GET', productRulesRoute + '/*').as('GetProductRules')
                cy.route('GET', productRulesRoute + '/*/' + ArrivalRestriction).as('GetArrivalRestriction')
                cy.route('GET', productRulesRoute + '/*/' + DepartureRestriction).as('GetDepartureRestriction')
                cy.route('GET', productRulesRoute + '/*/' + BookingRestriction).as('GetBookingRestriction')
                cy.route('PUT', productRulesRoute + '/*').as('PutData')
                cy.route('PUT', productRulesRoute + '/*/' + BookingRestriction).as('PutData')
                var modified = false

                cy.get('.table-component__table__body > tr').each((tr, i) => {
                        if (tr[0].cells[1].innerText == ProductRule.Original.Name) {

                            cy.wrap(tr[0].cells[2]).contains('Change').click()

                            FillForm(ProductRule.Modified, true)

                            cy.wait('@PutData').then((response) => {
                                cy.wrap(response.status).should('eq', 200)
                                modified = true
                            })
                        }
                    }).then(() => {
                        cy.wrap(modified).should('eq', true)
                    })
            })


            it('Time Restrictions Test', function () {
                cy.route('GET', productRulesRoute + '/*').as('GetProductRules')
                cy.route('GET', productRulesRoute + '/*/' + ArrivalRestriction).as('GetArrivalRestriction')
                cy.route('GET', productRulesRoute + '/*/' + DepartureRestriction).as('GetDepartureRestriction')
                cy.route('GET', productRulesRoute + '/*/' + BookingRestriction).as('GetBookingRestriction')

                cy.route('POST', productRulesRoute + '/*/' + ArrivalRestriction).as('PostArrivalRestriction')
                cy.route('POST', productRulesRoute + '/*/' + DepartureRestriction).as('PostDepartureRestriction')
                cy.route('POST', productRulesRoute + '/*/' + BookingRestriction).as('PostBookingRestriction')

                cy.route('PUT', productRulesRoute + '/*/' + ArrivalRestriction).as('PutArrivalRestriction')
                cy.route('PUT', productRulesRoute + '/*/' + DepartureRestriction).as('PutDepartureRestriction')
                cy.route('PUT', productRulesRoute + '/*/' + BookingRestriction).as('PutBookingRestriction')

                cy.route('DELETE', productRulesRoute + '/*/' + ArrivalRestriction + '/*').as('DeleteArrivalRestriction')
                cy.route('DELETE', productRulesRoute + '/*/' + DepartureRestriction + '/*').as('DeleteDepartureRestriction')
                cy.route('DELETE', productRulesRoute + '/*/' + BookingRestriction + '/*').as('DeleteBookingRestriction')

                cy.route('PUT', productRulesRoute + '/*').as('PutData')

                cy.get('.table-component__table__body > tr').each((tr, i) => {
                    if (tr[0].cells[1].innerText == ProductRule.Modified.Name) {

                        cy.wrap(tr[0].cells[2]).contains('Change').click()

                        cy.wait('@GetProductRules')

                        cy.wait('@GetArrivalRestriction')
                        cy.wait('@GetDepartureRestriction')
                        cy.wait('@GetBookingRestriction')
                        cy.wait('@GetAirport')
                        cy.wait('@GetFeatures')

                        cy.log('Add Restrictions')
                        AddRestriction('[data-cy=arrival-time-restriction-button-create]', '[data-cy=product-rules-arrival-time-restrictions-table-container]', ProductRule.Modified.ArrivalRestriction.Original, '@GetArrivalRestriction', '@PostArrivalRestriction')
                        AddRestriction('[data-cy=departure-time-restriction-button-create]', '[data-cy=product-rules-departure-time-restrictions-table-container]', ProductRule.Modified.DepartureRestriction.Original, '@GetDepartureRestriction', '@PostDepartureRestriction')
                        AddRestriction('[data-cy=booking-time-restriction-button-create]', '[data-cy=product-rules-booking-time-restrictions-table-container]', ProductRule.Modified.BookingRestriction.Original, '@GetBookingRestriction', '@PostBookingRestriction')

                        cy.log('Modify Restrictions')
                        EditRestriction('[data-cy=product-rules-arrival-time-restrictions-table-container]', ProductRule.Modified.ArrivalRestriction.Modified, '@GetArrivalRestriction', '@PutArrivalRestriction')
                        EditRestriction('[data-cy=product-rules-departure-time-restrictions-table-container]', ProductRule.Modified.DepartureRestriction.Modified, '@GetDepartureRestriction', '@PutDepartureRestriction')
                        EditRestriction('[data-cy=product-rules-booking-time-restrictions-table-container]', ProductRule.Modified.BookingRestriction.Modified, '@GetBookingRestriction', '@PutBookingRestriction')
                        cy.log('Delete Restrictions')
                        DeleteRestriction('[data-cy=product-rules-arrival-time-restrictions-table-container]', '@GetArrivalRestriction', '@DeleteArrivalRestriction')
                        DeleteRestriction('[data-cy=product-rules-departure-time-restrictions-table-container]', '@GetDepartureRestriction', '@DeleteDepartureRestriction')
                        DeleteRestriction('[data-cy=product-rules-booking-time-restrictions-table-container]', '@GetBookingRestriction', '@DeleteBookingRestriction')

                        cy.get('[data-cy=product-rule-save-btn]').click()

                        cy.wait('@PutData').then((response) => {
                            cy.wrap(response.status).should('eq', 200)
                        })
                    }
                })
            })

            it('Modify Product Day Rules', function () {
                cy.route('GET', productRulesRoute + '/*').as('GetProductRules')
                cy.route('GET', productRulesRoute + '/*/' + ArrivalRestriction).as('GetArrivalRestriction')
                cy.route('GET', productRulesRoute + '/*/' + DepartureRestriction).as('GetDepartureRestriction')
                cy.route('GET', productRulesRoute + '/*/' + BookingRestriction).as('GetBookingRestriction')
                cy.route('PUT', productRulesRoute + '/*').as('PutData')

                var modified = false
                cy.get('.table-component__table__body > tr').each((tr, i) => {
                        if (tr[0].cells[1].innerText == ProductRule.Modified.Name) {

                            cy.wrap(tr[0].cells[2]).contains('Change').click()

                            cy.wait('@GetProductRules')
                            cy.wait('@GetArrivalRestriction')
                            cy.wait('@GetDepartureRestriction')
                            cy.wait('@GetBookingRestriction')

                            cy.get('tbody').then((table) => {
                                cy.wrap(table[0].childNodes).each((row, i) => {
                                    var day = row[0].cells[0].textContent

                                    cy.log(day)
                                    cy.wrap(row[0].cells[1].children).then((child) => {
                                        cy.wrap(child[0].children[0]).click()

                                        for (var k = 0; k < ProductRule.Modified.LeadTimeRanges[i].From.length; k++) {
                                            cy.wrap(child[1].children[0]).type(ProductRule.Modified.LeadTimeRanges[i].From[k])
                                            cy.wrap(child[1].children[1]).type(ProductRule.Modified.LeadTimeRanges[i].To[k])
                                            cy.wrap(child[1].children[2]).click()
                                        }

                                    })

                                    cy.wrap(row[0].cells[5].children).then((child) => {
                                        cy.wrap(child[0].children[0]).click()

                                        for (var k = 0; k < ProductRule.Modified.DurationRanges[i].From.length; k++) {
                                            cy.wrap(child[1].children[0]).type(ProductRule.Modified.DurationRanges[i].From[k])
                                            cy.wrap(child[1].children[1]).type(ProductRule.Modified.DurationRanges[i].To[k])
                                            cy.wrap(child[1].children[2]).click()
                                        }
                                    })
                                })
                            })
                            cy.get('[data-cy=product-rule-save-btn]').click()

                            cy.wait('@PutData').then((response) => {
                                cy.wrap(response.status).should('eq', 200)
                                modified = true
                            })
                        }
                    }).then(() => {
                        cy.wrap(modified).should('eq', true)
                    })
            })
        })

        describe('After Modify', function () {
            it('Check Table for Modified Product Rules', function () {
                CheckTable(ProductRule.Modified)
            })

            it("Delete Product Rules", function () {
                cy.route('DELETE', productRulesRoute + '/*').as('DeleteProductRules')
                var deleted = false

                cy.get('.table-component__table__body > tr').each((tr, i) => {
                        if (tr[0].cells[1].innerText == ProductRule.Modified.Name) {

                            //waiting for toast to disappear
                            cy.wait(1000)
                            cy.wrap(tr[0].cells[2]).contains('Delete').click()
                            cy.get('.btn-danger').click()

                            cy.wait('@DeleteProductRules').then((response) => {
                                cy.wrap(response.status).should('eq', 200)
                                deleted = true
                            })
                        }
                    }).then(() => {
                        cy.wrap(deleted).should('eq', true)
                    })

                cy.wait('@GetData')

                cy.get('.table-component__table__body > tr').each((tr, i) => {
                        expect(tr[0].cells[1].innerText).to.not.equal(ProductRule.Original.Name)
                        expect(tr[0].cells[1].innerText).to.not.equal(ProductRule.Modified.Name)
                    })
            })
        })

    })
})

function FillForm(testData, modify) {

    if (!modify) {
        cy.wait('@GetAirport')
    }
    else {
        cy.wait('@GetProductRules')
        cy.wait('@GetArrivalRestriction')
        cy.wait('@GetDepartureRestriction')
        cy.wait('@GetBookingRestriction')
    }

    cy.get('[data-cy=product-rules-select-airport]').select(testData.Airport)
    cy.get('[data-cy=product-rules-input-rule-name]').clear().type(testData.Name)
    cy.get('[data-cy=product-rules-select-lead-time-type]').select(testData.LeadTimeType)
    cy.get('[data-cy=product-rules-select-min-max-type]').select(testData.MinMaxStayType)

    cy.get('[data-cy=product-rule-save-btn]').click()
}

function CheckTable(testData) {
    var found = false
    cy.get('.table-component__table__body > tr').each((tr, i) => {
            if (tr[0].cells[1].innerText == testData.Name) {
                expect(tr[0].cells[0].innerText).to.equal(testData.Airport)
                expect(tr[0].cells[1].innerText).to.equal(testData.Name)
                found = true
            }
        }).then(() => {
            cy.wrap(found).should('eq', true)
        })
}

function AddRestriction(target, tableTarget, RestrictionType, GetRoute, PostRoute) {
    cy.get(target).click()

    cy.get('#hourFromHours').select(RestrictionType.TimeFrom.Hours)
    cy.get('#hourFromMinutes').select(RestrictionType.TimeFrom.Minutes)
    cy.get('#hourToHours').select(RestrictionType.TimeTo.Hours)
    cy.get('#hourToMinutes').select(RestrictionType.TimeTo.Minutes)

    cy.get('.custom-toggle').then((days) => {
        cy.wrap(days[0].children).each((day, k) => {
            customFunctions.SetCheckbox(day[0], RestrictionType.Days[k])
        }).then(() => {
            cy.get('.modal-footer > .btn').click()
            cy.wait(PostRoute).then((response) => {
                cy.wrap(response.status).should('eq', 200)
            })
        })
    })

    cy.wait(GetRoute)

    CheckRestriction(tableTarget, RestrictionType)
}

function DeleteRestriction(target, GetRoute, DeleteRoute) {
    var modified = false
    cy.get(target).then((table) => {
        var tableBody = table[0].children[0].children[0].children[1].children
        cy.wrap(tableBody).each((row, i) => {
            cy.wrap(row[0].cells[9]).contains('Delete').click()
            cy.wait(1000)

            cy.wait(DeleteRoute).then((response) => {
                cy.wrap(response.status).should('eq', 200)
                modified = true
            })
            cy.wait(GetRoute)
        })
        cy.wrap(tableBody).should('not.be.visible')
    }).then(() => {
        debugger
        cy.wrap(modified).should('eq', true)
    })
}

function EditRestriction(target, RestrictionType, GetRoute, PutRoute) {
    var modified = false

    cy.get(target).then((table) => {
        var tableBody = table[0].children[0].children[0].children[1].children

        cy.wrap(tableBody).each((row, i) => {

            cy.wrap(row[0].cells[9]).contains('Edit').click()

            cy.get('#hourFromHours').select(RestrictionType.TimeFrom.Hours)
            cy.get('#hourFromMinutes').select(RestrictionType.TimeFrom.Minutes)
            cy.get('#hourToHours').select(RestrictionType.TimeTo.Hours)
            cy.get('#hourToMinutes').select(RestrictionType.TimeTo.Minutes)

            cy.get('.custom-toggle').then((days) => {
                cy.wrap(days[0].children).each((day, k) => {
                    customFunctions.SetCheckbox(day[0], RestrictionType.Days[k])
                }).then(() => {
                    cy.get('.modal-footer > .btn').click()
                    cy.wait(PutRoute).then((response) => {
                        cy.wrap(response.status).should('eq', 200)
                        modified = true
                    })
                })
            })

            cy.wait(1000)
            cy.wait(GetRoute)
        })

        CheckRestriction(target, RestrictionType)

    }).then(() => {
        cy.wrap(modified).should('eq', true)
    })
}

function CheckRestriction(target, RestrictionType) {
    var modified = false
    cy.get(target).then((table) => {
        var tableBody = table[0].children[0].children[0].children[1].children
        var DayValue = []

        for (var k = 0; k < RestrictionType.Days.length; k++) {
            DayValue[k] = (RestrictionType.Days[k]) ? 'Yes' : 'No'
        }
        cy.wrap(tableBody).each((row, i) => {
            cy.wrap(row[0].cells[0].innerText).should('eq', RestrictionType.TimeFrom.Hours + ':' + RestrictionType.TimeFrom.Minutes)
            cy.wrap(row[0].cells[1].innerText).should('eq', RestrictionType.TimeTo.Hours + ':' + RestrictionType.TimeTo.Minutes)
            cy.wrap(row[0].cells[2].innerText).should('eq', DayValue[0])
            cy.wrap(row[0].cells[3].innerText).should('eq', DayValue[1])
            cy.wrap(row[0].cells[4].innerText).should('eq', DayValue[2])
            cy.wrap(row[0].cells[5].innerText).should('eq', DayValue[3])
            cy.wrap(row[0].cells[6].innerText).should('eq', DayValue[4])
            cy.wrap(row[0].cells[7].innerText).should('eq', DayValue[5])
            cy.wrap(row[0].cells[8].innerText).should('eq', DayValue[6])
            modified = true
        })
    }).then(() => {
        cy.wrap(modified).should('eq', true)
    })
}