import * as customFunctions from '../../../../support/functions.js'
import * as Data from '../../../../fixtures/AdminSetup.json'

const carParkRoute = 'inventory/api/carpark'
const carParkCategoryRoute = 'inventory/api/carpark/category'
const carParkMapType = 'inventory/api/carpark/maptype'

const movementPricingRoute = 'inventory/api/movementpricing'

const countryReferenceRoute = 'inventory/api/reference/country'
const timeZoneReferenceRoute = 'inventory/api/reference/timezone'
const merchantRoute = 'inventory/api/reference/merchantaccount'

const capacityPoolRoute = 'inventory/api/capacitypool'
const airportRoute = 'inventory/api/airport'

describe('Car Parks Test', function () {

    const CarPark = Data.Inventory.Inventory.CarPark

    beforeEach(function () {
        cy.logInNoPre("inventory")

        cy.route('GET', carParkRoute).as('GetData')
        cy.route('GET', movementPricingRoute).as('GetMovementPricing')
        cy.route('GET', carParkCategoryRoute).as('GetCarParkCategory')
        cy.route('GET', countryReferenceRoute).as('GetCountryReference')
        cy.route('GET', merchantRoute).as('GetMerchantAccount')
        cy.route('GET', timeZoneReferenceRoute).as('GetTimeZoneReference')
        cy.route('GET', carParkMapType).as('GetCarParkMapType')
        cy.route('GET', capacityPoolRoute).as('GetCapacityPool')
        cy.route('GET', airportRoute).as('GetAirport')

        cy.get('[data-cy=sidebar-item-inventory]').click()
        cy.get('[data-cy=sidebar-item-inventory-car-parks]').click()

        cy.wait('@GetAirport')
        cy.wait('@GetData')
    })

    describe('Filter Test', function () {
        var FilterLoop = Array.from({ length: 3 }, (v, k) => k)

        it('Filter Car Park', function () {
            var field = [CarPark.Original.AirportName, CarPark.Original.CarParkCode, CarPark.Original.CarParkName]
            var found = [false, false, false]

            cy.wrap(FilterLoop).each((n) => {
                cy.get('.table-component__filter__field').clear().type(field[n])

                cy.get('.table-component__table__body > tr').each((tr, i) => {

                    cy.wrap(tr[0].cells[n]).contains(field[n])
                    found[n] = true
                }).then(() => {
                    cy.wrap(found[n]).should('eq', true)
                })
            })

        })
    })
})