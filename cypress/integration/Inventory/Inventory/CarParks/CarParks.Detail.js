import * as customFunctions from '../../../../support/functions.js'
import * as Data from '../../../../fixtures/AdminSetup.json'

const carParkRoute = 'inventory/api/carpark'
const carParkCategoryRoute = 'inventory/api/carpark/category'
const carParkMapType = 'inventory/api/carpark/maptype'

const movementPricingRoute = 'inventory/api/movementpricing'
const occupancyPricingRoute = '/inventory/api/occupancypricing'

const countryReferenceRoute = 'inventory/api/reference/country'
const timeZoneReferenceRoute = 'inventory/api/reference/timezone'
const merchantRoute = 'inventory/api/reference/merchantaccount'

const capacityPoolRoute = 'inventory/api/capacitypool'
const airportRoute = 'inventory/api/airport'

const movementAdjustmentUrl = '/movementadjustment'
const occupancyAdjustmentUrl = '/occupancyadjustment'

describe('Car Parks Test', function () {

    const CarPark = Data.Inventory.Inventory.CarPark

    beforeEach(function () {
        cy.logInNoPre("inventory")

        cy.route('GET', carParkRoute).as('GetData')
        cy.route('GET', movementPricingRoute).as('GetMovementPricing')
        cy.route('GET', carParkCategoryRoute).as('GetCarParkCategory')
        cy.route('GET', countryReferenceRoute).as('GetCountryReference')
        cy.route('GET', merchantRoute).as('GetMerchantAccount')
        cy.route('GET', timeZoneReferenceRoute).as('GetTimeZoneReference')
        cy.route('GET', carParkMapType).as('GetCarParkMapType')
        cy.route('GET', capacityPoolRoute).as('GetCapacityPool')
        cy.route('GET', airportRoute).as('GetAirport')

        cy.get('[data-cy=sidebar-item-inventory]').click()
        cy.get('[data-cy=sidebar-item-inventory-car-parks]').click()

        cy.wait('@GetAirport')
        cy.wait('@GetData')
    })


    describe('Modifying Car Park', function () {


        it('Modify Car Park Detail', function () {
            ModifyDetail(CarPark.Modified, CarPark.Original.CarParkName)
        })

        it('Check Modified Table', function () {
            CheckDetail(CarPark.Modified)
        })

        it('Revert Car Park Detail', function () {
            ModifyDetail(CarPark.Original, CarPark.Modified.CarParkName, carParkRoute)
        })

        it('Check Reverted Table', function () {
            CheckDetail(CarPark.Original)
        })
    })
})


function ModifyDetail(testData, search) {
    cy.route('GET', carParkRoute + '/*' + occupancyAdjustmentUrl).as('GetOccupancyAdjustment')
    cy.route('GET', carParkRoute + '/*' + movementAdjustmentUrl).as('GetMovementAdjustment')
    cy.route('GET', carParkRoute + '/*/transfer').as('GetTransfer')
    cy.route('PUT', carParkRoute + '/*').as('PutData')

    var modified = false
    cy.get('.table-component__table__body > tr').each((tr, i) => {
        if (tr[0].cells[2].innerText == search) {

            cy.wrap(tr[0].cells[4]).contains('Change').click()

            cy.wait('@GetOccupancyAdjustment')
            cy.wait('@GetMovementAdjustment')
            cy.wait('@GetTransfer')
            cy.wait('@GetMovementPricing')

            cy.wait('@GetCarParkCategory')
            cy.wait('@GetCountryReference')
            cy.wait('@GetMerchantAccount')
            cy.wait('@GetTimeZoneReference')
            cy.wait('@GetCarParkMapType')
            cy.wait('@GetCapacityPool')

            var openDate = customFunctions.GetDate(testData.CarParkDetail.OpenDate)

            cy.get('[data-cy=car-park-detail-form-category-select]').select(testData.CarParkDetail.Category)
            cy.get('[data-cy=car-park-detail-form-terminal-select]').select(testData.CarParkDetail.Terminal)

            cy.get('#carParkCode').clear().type(testData.CarParkDetail.CarParkCode)
            cy.get('#carParkName').clear().type(testData.CarParkDetail.CarParkName)
            cy.get('#carParkDisplayName').clear().type(testData.CarParkDetail.DisplayName)

            cy.get('#carParkAddressLine1').clear().type(testData.CarParkDetail.Address.Line1)
            cy.get('[data-cy=car-park-detail-form-address-line-2-input]').clear().type(testData.CarParkDetail.Address.Line2)
            cy.get('#carParkCity').clear().type(testData.CarParkDetail.Address.City)
            cy.get('#carParkCounty').clear().type(testData.CarParkDetail.Address.County)
            cy.get('#carParkPostcode').clear().type(testData.CarParkDetail.Address.Postcode)
            cy.get(':nth-child(6) > .field > .input > [data-cy=car-park-detail-form-country-input]').select(testData.CarParkDetail.Address.Country)

            cy.get('[data-cy=car-park-detail-form-description-input] > .editr > .editr--content').clear().type(testData.CarParkDetail.Description)

            cy.get('#carParkMinLeadTime').clear().type(testData.CarParkDetail.MinLeadTime)

            customFunctions.SetDate('#openDate', openDate)

            cy.get('[data-cy=car-park-detail-form-open-time-input]').select(testData.CarParkDetail.OpenDate.Time)

            cy.get('[data-cy=car-park-detail-form-merchant-account-input]').select(testData.CarParkDetail.MerchantAccount)

            cy.get('[data-cy=car-park-detail-form-time-zone-input]').select(testData.CarParkDetail.TimeZone)

            customFunctions.SelectCheckbox('.checkbox-scroll-container', testData.CarParkDetail.CapacityPool)

            customFunctions.SetCheckbox('.col-12 > .input__checkbox-label > .input__checkbox-input', testData.CarParkDetail.Alert)

            cy.get('[data-cy=car-park-detail-form-longitude-input] > .row > .field > :nth-child(1) > .input > .form-control').clear().type(testData.CarParkDetail.Maps.Longitude)

            cy.get('[data-cy=car-park-detail-form-latitude-input] > .row > .field > :nth-child(1) > .input > .form-control').clear().type(testData.CarParkDetail.Maps.Latitude)

            if (testData.CarParkDetail.MapFile)
                cy.get('.input-file').upload({ fileContent, fileName, mimeType: 'application/json' })

            cy.get('[data-cy=car-park-detail-form-map-type-select]').select(testData.CarParkDetail.Maps.MapType)

            cy.get('[data-cy=car-park-detail-form-map-description-input] > .editr > .editr--content').clear().type(testData.CarParkDetail.Maps.TextDescription)

            cy.get('[data-cy=car-park-detail-form-directions-checkbox] > .editr > .editr--content').clear().type(testData.CarParkDetail.EmailDetails.Directions)

            customFunctions.SetCheckbox(':nth-child(16) > .col > .input__checkbox-label > .input__checkbox-input', testData.CarParkDetail.EmailDetails.Reminder)

            cy.get('[data-cy=car-park-detail-form-reminder-input]').clear().type(testData.CarParkDetail.EmailDetails.ReminderDays)

            customFunctions.SetCheckbox(':nth-child(18) > .col > .input__checkbox-label > .input__checkbox-input', testData.CarParkDetail.EmailDetails.IncludePDF)

            cy.get('[data-cy=car-park-detail-form-pof-input]').clear().type(testData.CarParkDetail.CPMS.POFCode)

            cy.get('[data-cy=car-park-detail-form-connection-mapping-select]').select(testData.CarParkDetail.CPMS.ConnectionMappingSettings)

            cy.get('[data-cy=car-park-detail-form-cpms-select]').select(testData.CarParkDetail.CPMS.CPMSBarrierSettings)

            customFunctions.SetCheckbox('.mt-5 > .col > .input__checkbox-label > .input__checkbox-input', testData.CarParkDetail.Active)

            cy.get('.col > .field > [data-cy=car-park-detail-form-button-submit]').click()

            cy.wait('@PutData').then((response) => {
                cy.wrap(response.status).should('eq', 204)
            })

            modified = true
        }
    }).then(() => {
        cy.wrap(modified).should('eq', true)
    })
}

function CheckDetail(testData) {
    var found = false
    cy.get('.table-component__table__body > tr').each((tr, i) => {
        if (tr[0].cells[2].innerText == testData.CarParkName) {
            cy.wrap(tr[0].cells[0].innerText).should('eq', testData.AirportName)
            cy.wrap(tr[0].cells[1].innerText).should('eq', testData.CarParkCode)
            cy.wrap(tr[0].cells[2].innerText).should('eq', testData.CarParkName)
            cy.wrap(tr[0].cells[3].innerText).should('eq', testData.CarParkDetail.Active.toString())
            found = true
        }
    }).then(() => {
        cy.wrap(found).should('eq', true)
    })
}
