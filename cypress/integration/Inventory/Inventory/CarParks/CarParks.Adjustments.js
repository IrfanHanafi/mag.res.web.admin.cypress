import * as customFunctions from '../../../../support/functions.js'
import * as Data from '../../../../fixtures/AdminSetup.json'

const AdjustmentTab = {
    OCCUPANCY: 2,
    MOVEMENT: 3
}

const carParkRoute = 'inventory/api/carpark'
const carParkCategoryRoute = 'inventory/api/carpark/category'
const carParkMapType = 'inventory/api/carpark/maptype'

const movementPricingRoute = 'inventory/api/movementpricing'
const occupancyPricingRoute = '/inventory/api/occupancypricing'

const countryReferenceRoute = 'inventory/api/reference/country'
const timeZoneReferenceRoute = 'inventory/api/reference/timezone'
const merchantRoute = 'inventory/api/reference/merchantaccount'

const capacityPoolRoute = 'inventory/api/capacitypool'
const airportRoute = 'inventory/api/airport'

const movementAdjustmentUrl = '/movementadjustment'
const occupancyAdjustmentUrl = '/occupancyadjustment'

describe('Car Parks Test', function () {

    const CarPark = Data.Inventory.Inventory.CarPark

    beforeEach(function () {
        cy.logInNoPre("inventory")

        cy.route('GET', carParkRoute).as('GetData')
        cy.route('GET', movementPricingRoute).as('GetMovementPricing')
        cy.route('GET', carParkCategoryRoute).as('GetCarParkCategory')
        cy.route('GET', countryReferenceRoute).as('GetCountryReference')
        cy.route('GET', merchantRoute).as('GetMerchantAccount')
        cy.route('GET', timeZoneReferenceRoute).as('GetTimeZoneReference')
        cy.route('GET', carParkMapType).as('GetCarParkMapType')
        cy.route('GET', capacityPoolRoute).as('GetCapacityPool')
        cy.route('GET', airportRoute).as('GetAirport')

        cy.get('[data-cy=sidebar-item-inventory]').click()
        cy.get('[data-cy=sidebar-item-inventory-car-parks]').click()

        cy.wait('@GetAirport')
        cy.wait('@GetData')
    })
    //make it simpler
    describe('Modifying Car Park Adjustments', function () {

        it('Add Occupancy Adjustments', function () {
            AddAdjustment(CarPark.Modified.OccupancyAdjustments, CarPark.Original.CarParkName, occupancyAdjustmentUrl, AdjustmentTab.OCCUPANCY, 'data-cy=add-car-park-occupancy')
        })

        it('Check Occupancy Adjustments', function () {
            CheckAdjustment(CarPark.Modified.OccupancyAdjustments, CarPark.Original.CarParkName, occupancyPricingRoute, AdjustmentTab.OCCUPANCY)
        })

        it('Add Movement Adjustments', function () {
            AddAdjustment(CarPark.Modified.MovementAdjustments, CarPark.Original.CarParkName, movementAdjustmentUrl, AdjustmentTab.MOVEMENT, 'data-cy=add-car-park-movement')
        })

        it('Check Movement Adjustments', function () {
            CheckAdjustment(CarPark.Modified.MovementAdjustments, CarPark.Original.CarParkName, movementPricingRoute, AdjustmentTab.MOVEMENT)
        })

        it('Delete Occupancy Adjustments', function () {
            DeleteAdjustment(CarPark.Modified.OccupancyAdjustments, CarPark.Original.CarParkName, occupancyPricingRoute, AdjustmentTab.OCCUPANCY, occupancyAdjustmentUrl)
        })

        it('Delete Movement Adjustments', function () {
            DeleteAdjustment(CarPark.Modified.MovementAdjustments, CarPark.Original.CarParkName, movementPricingRoute, AdjustmentTab.MOVEMENT, movementAdjustmentUrl)
        })

    })
})

function AddAdjustment(testData, search, adjustmentUrl, adjustmentType, target) {
    cy.route('POST', carParkRoute + '/*' + adjustmentUrl).as('PostAdjustment')
    var modified = false

    cy.get('.table-component__table__body > tr').each((tr, i) => {
        if (tr[0].cells[2].innerText == search) {
            cy.wrap(tr[0].cells[4]).contains('Change').click()

            customFunctions.GetTab(adjustmentType)

            cy.get('[' + target + '-ddl]').select(testData.RuleName)

            cy.get('[' + target + '-btn]').click()

            cy.wait('@PostAdjustment').then((response) => {
                cy.wrap(response.status).should('eq', 201)
            })

            modified = true
        }
    }).then(() => {
        cy.wrap(modified).should('eq', true)
    })
}
function CheckAdjustment(testData, search, pricingRoute, adjustmentType) {
    cy.route('GET', pricingRoute).as('GetPricing')
    var modified = false

    cy.get('.table-component__table__body > tr').each((tr, i) => {
        if (tr[0].cells[2].innerText == search) {
            cy.wrap(tr[0].cells[4]).contains('Change').click()

            customFunctions.GetTab(adjustmentType)

            cy.wait('@GetPricing')

            cy.get('.table-component__table__body > tr').each((tr, i) => {
                if (tr[0].cells[0].innerText == testData.RuleName) {

                    expect(tr[0].cells[0].innerText).to.equal(testData.RuleName)
                    expect(tr[0].cells[1].innerText).to.equal(testData.ArrivalDateFrom)
                    expect(tr[0].cells[2].innerText).to.equal(testData.ArrivalDateTo)
                    modified = true
                }
            })
        }
    }).then(() => {
        cy.wrap(modified).should('eq', true)
    })
}

function DeleteAdjustment(testData, search, pricingRoute, adjustmentType, adjustmentUrl) {
    cy.route('GET', pricingRoute).as('GetPricing')
    cy.route('DELETE', carParkRoute + '/*' + adjustmentUrl + '/*').as('DeleteAdjustment')
    var modified = false

    cy.get('.table-component__table__body > tr').each((tr, i) => {
        if (tr[0].cells[2].innerText == search) {
            cy.wrap(tr[0].cells[4]).contains('Change').click()

            customFunctions.GetTab(adjustmentType)

            cy.wait('@GetPricing')

            cy.get('.table-component__table__body > tr').each((tr, i) => {
                if (tr[0].cells[0].innerText == testData.RuleName) {

                    cy.wrap(tr[0].cells[3]).contains('Delete').click()
                    cy.get('.btn-danger').click()

                    cy.wait('@DeleteAdjustment').then((response) => {
                        cy.wrap(response.status).should('eq', 204)
                        modified = true
                    })
                }
            })
        }
    }).then(() => {
        cy.wrap(modified).should('eq', true)
    })
}