import * as customFunctions from '../../../../support/functions.js'
import * as Data from '../../../../fixtures/AdminSetup.json'
import * as productFunctions from '../../../../support/ProductBundleFunctions'

const productBundleRoute = '**/api/productbundle'
const airportRoute = '**/api/airport'

const ProductBundle = Data.Inventory.Inventory.ProductBundle
const ProductAncillary = {
    Original: ProductBundle.Original.ProductAncillary.Item,
    Modified: ProductBundle.Modified.ProductAncillary.Item
}

const Tab = Data.Enum.ProductBundleTab

describe('Product Bundle Product Ancillary Test', function () {
    beforeEach(function () {
        cy.logInNoPre('inventory')

        cy.route('GET', productBundleRoute).as('GetData')
        cy.route('GET', productBundleRoute + '/*/productaddon').as('GetProductAddOn')
                    cy.route('GET', airportRoute + '/*/product/header').as('GetAirportHeader')
                    cy.route('GET', airportRoute + '/*/addon').as('GetAirportAddOn')

        cy.get('[data-cy=sidebar-item-inventory]').click()
        cy.get('[data-cy=sidebar-item-inventory-product-bundles]').click()

        cy.wait('@GetData')

        productFunctions.VisitTab(ProductBundle.Original, Tab.PRODUCTSANDANCILLARIES)
    })

    it('Add Product Ancillary', function () {
        AddProductsAncillaries(ProductAncillary.Original)
    })

    it('Check Product Ancillary', function () {
        CheckProductAncillaries(ProductAncillary.Original)
    })

    it('Modify Product Ancillary', function () {
        ModifyProductAncillaries(ProductAncillary.Modified)
    })

    it('Delete Product Ancillary', function () {
        DeleteProductAncillaries(ProductAncillary.Modified)
    })
})

//Incomplete
function AddProductsAncillaries(testData) {

    cy.get('[data-cy=add-product-bundle-addon-btn]').click()

    cy.wait('@GetProductAddOn')
    cy.wait('@GetAirportHeader')
    cy.wait('@GetAirportAddOn')

    testData.forEach(function (item, i) {
        if (item.Type == "Car Park Product") {
            cy.route('POST', productBundleRoute + '/*/product').as('PostData')
            cy.get('[data-cy=product-bundle-product-add-ons-select-products]').select(item.Name)
            cy.get('[data-cy=product-bundle-product-add-ons-button-add-product]').click()
        }
        else if (item.Type == "Add On") {
            cy.route('POST', productBundleRoute + '/*/addon').as('PostData')
            cy.get('[data-cy=product-bundle-product-add-ons-select-add-ons]').select(item.Name)
            cy.get('[data-cy=product-bundle-product-add-ons-select-offer-line]').select(item.Offerline)
            cy.get('[data-cy=product-bundle-product-add-ons-input-name]').clear().type(item.Quantity)
            cy.get('[data-cy=product-bundle-product-add-ons-button-add-add-on]').click()
        }

        cy.wait('@PostData').then((response) => {
            cy.wrap(response.status).should('eq, 200')
        })
        cy.wait('@GetProductAddOn')
    })
}

//Incomplete
function ModifyProductAncillaries(testData) {

    cy.route('PUT', productBundleRoute + '/*/addon/*').as('PutData')
    cy.get('[data-cy=add-product-bundle-addon-btn]')

    var modified = []

    for (var j; j < testData.length; j++) {
        modified[j] = false
    }

    testData.forEach(function (item, i) {
        cy.get('.table-component__table__body > tr').each((tr) => {
            if (tr[0].cells[0].innerText == item.Name) {
                cy.wrap(tr[0].cells[6]).contains('Change').click()

                FillProductAncillary(item)

                cy.wait('@PutData').then((response) => {
                    cy.wrap(response.status).should('eq', 200)
                    modified[i] = true
                })
            }
        }).then(() => {
            cy.wrap(modified[i]).should('eq', true)
        })
    })
}

//Incomplete
function DeleteProductAncillaries(testData) {

    cy.wait('@GetProductAddOn')
    cy.wait('@GetAirportHeader')
    cy.wait('@GetAirportAddOn')

    cy.route('DELETE', productBundleRoute + '/*/productaddon/*').as('DeleteData')

    var modified = []

    for (var j; j < testData.length; j++) {
        modified[j] = false
    }

    testData.forEach(function (item, i) {
        cy.get('.table-component__table__body > tr').each((tr) => {
            if (tr[0].cells[0].innerText == item.Name) {
                cy.wrap(tr[0].cells[6]).contains('Delete').click()
                cy.get('.btn-danger').click()

                cy.wait('@DeleteData').then((response) => {
                    cy.wrap(response.status).should('eq', 200)
                    modified[i] = true
                })
            }
        }).then(() => {
            cy.wrap(modified[i]).should('eq', true)
        })

        cy.wait('@GetProductAddOn')
    })
}

//Incomplete
function CheckProductAncillaries(testData) {
    var modified = []

    for (var j; j < testData.length; j++) {
        modified[j] = false
    }

    testData.forEach(function (item, i) {
        cy.get('.table-component__table__body > tr').each((tr) => {
            if (tr[0].cells[0].innerText == item.Name) {
                cy.wrap(tr[0].cells[0].innerText).should('eq', item.Name)
                cy.wrap(tr[0].cells[1].innerText).should('eq', item.Offerline)
                cy.wrap(tr[0].cells[2].innerText).should('eq', item.Quantity)
                cy.wrap(tr[0].cells[3].innerText).should('eq', item.Type)
                cy.wrap(tr[0].cells[4].innerText).should('eq', item.PriceTypeAdjustment)
                cy.wrap(tr[0].cells[0].innerText).should('eq', item.PriceTypeValue)
            }
        }).then(() => {
            cy.wrap(modified[i]).should('eq', true)
        })
    })
}

//Incomplete
function FillProductAncillary(testData) {
    if (testData.Type == 'Add On') {
        cy.get('product-addon-detail-input-quantity').clear().type(testData.Quantity)
    }

    cy.get('product-addon-detail-input-price-type-adjustment').select(testData.PriceTypeAdjustment)
    cy.get('product-addon-detail-input-price-type-value').clear().type(testData.PriceTypeValue)
    cy.get('product-addon-detail-button-save').click()
}