import * as customFunctions from '../../../../support/functions.js'
import * as Data from '../../../../fixtures/AdminSetup.json'
import * as productFunctions from '../../../../support/ProductBundleFunctions'

const productBundleRoute = '**/api/productbundle'
const airportRoute = '**/api/airport'
const reportingProductRoute = '**/api/reportingProduct'
const tariffRoute = '**/api/tariff'
const capacityPoolRoute = '**/api/capacitypool'

const ProductBundle = Data.Inventory.Inventory.ProductBundle
const CancellationRules = {
    Original: ProductBundle.Original.CancellationRules,
    Modified: ProductBundle.Modified.CancellationRules
}

const Tab = {
    IMAGES: 2,
    BOOKINGDATES: 3,
    PRODUCTSANDANCILLARIES: 4,
    CANCELLATIONRULES: 5,
    CANCELLATIONRULES: 6,
    CHANNELS: 7
}

describe('Product Bundle Cancellation Rules Test', function () {
    beforeEach(function () {
        cy.logInNoPre('inventory')

        cy.route('GET', productBundleRoute).as('GetData')

        cy.get('[data-cy=sidebar-item-inventory]').click()
        cy.get('[data-cy=sidebar-item-inventory-product-bundles]').click()

        cy.wait('@GetData')

        productFunctions.VisitTab(ProductBundle.Original, Tab.CANCELLATIONRULES)
    })

    it('Add Cancellation Rules', function () {
        productFunctions.AddRule(CancellationRules.Original, Tab.CANCELLATIONRULES)
    })

    it('Check Cancellation Rules', function () {
        productFunctions.CheckRule(CancellationRules.Original, Tab.CANCELLATIONRULES)
    })

    it('Modify Cancellation Rules', function () {
        productFunctions.ModifyRule(CancellationRules.Modified, Tab.CANCELLATIONRULES)
    })

    it('Check Cancellation Rules', function () {
        productFunctions.CheckRule(CancellationRules.Modified, Tab.CANCELLATIONRULES)
    })

    it('Delete Cancellation Rules', function () {
        productFunctions.DeleteRule(Tab.CANCELLATIONRULES)
    })
})
