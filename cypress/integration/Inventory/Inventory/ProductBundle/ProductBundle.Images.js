import * as customFunctions from '../../../../support/functions.js'
import * as Data from '../../../../fixtures/AdminSetup.json'
import * as productFunctions from '../../../../support/ProductBundleFunctions'

const productBundleRoute = '**/api/productbundle'

const ProductBundle = Data.Inventory.Inventory.ProductBundle
const Images = {
    Original: ProductBundle.Original.Images,
    Modified: ProductBundle.Modified.Images
}

const Tab = Data.Enum.ProductBundleTab

describe('Product Bundle Images Test', function () {
    beforeEach(function () {
        cy.logInNoPre('inventory')

        cy.route('GET', productBundleRoute).as('GetData')

        cy.get('[data-cy=sidebar-item-inventory]').click()
        cy.get('[data-cy=sidebar-item-inventory-product-bundles]').click()

        cy.wait('@GetData')

        productFunctions.VisitTab(ProductBundle.Original, Tab.IMAGES)
    })

    it('Add Images', function () {
        AddImages(Images.Original)
    })

    it('Check Images', function () {
        CheckImages(Images.Original)
    })

    it('Delete Images', function () {
        DeleteImages()
    })
})

//Incomplete
function AddImages(testData) {
    cy.route('POST', productBundleRoute + '/*/LogoImage').as('PostBundleIcon')
    cy.route('POST', productBundleRoute + '/*/ListViewLogoImage').as('PostListViewLogo')

    customFunctions.FileUpload('[data-cy=product-bundle-images-file-upload-bundle-icon-file]', testData.BundleIcon.Path, testData.BundleIcon.FileName)
    cy.wait('@PostBundleIcon').then((response) => {
        cy.wrap(response.status).should('eq', 200)
    })

    customFunctions.FileUpload('[data-cy=product-bundle-images-file-upload-list-icon-file]', testData.ListViewLogo.Path, testData.ListViewLogo.FileName)
    cy.wait('@PostListViewLogo').then((response) => {
        cy.wrap(response.status).should('eq', 200)
    })
}

//Incomplete
function CheckImages(testData) {
    var found = false
    cy.get(':nth-child(2) > :nth-child(1) > img').then((image) => {
        var imageSrcSplit = image[0].src.split("/")
        var imageSrc = imageSrcSplit[imageSrcSplit.length - 1]
        cy.wrap(imageSrc).should('eq', testData.ProductIcon.FileName)
    })

    cy.get(':nth-child(4) > :nth-child(1) > img').then((image) => {
        var imageSrcSplit = image[0].src.split("/")
        var imageSrc = imageSrcSplit[imageSrcSplit.length - 1]
        cy.wrap(imageSrc).should('eq', testData.ListViewLogo.FileName)
    })
}

//Incomplete
function DeleteImages() {
    cy.route('DELETE', productBundleRoute + '/*/LogoImage').as('DeleteBundleIcon')
    cy.route('DELETE', productBundleRoute + '/*/ListViewLogoImage').as('DeleteListViewLogo')

    cy.get('.col > :nth-child(3) > div > :nth-child(1)').click()
    cy.wait('@DeleteBundleIcon').then((response) => {
        cy.wrap(response.status).should('eq', 204)
    })
    cy.get('.col > :nth-child(3) > div > :nth-child(2)').click()

    cy.wait('@DeleteListViewLogo').then((response) => {
        cy.wrap(response.status).should('eq', 204)
    })
}
