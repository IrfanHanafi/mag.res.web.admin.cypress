import * as customFunctions from '../../../../support/functions.js'
import * as Data from '../../../../fixtures/AdminSetup.json'
import * as productFunctions from '../../../../support/ProductBundleFunctions'

const productBundleRoute = '**/api/productbundle'
const airportRoute = '**/api/airport'
const reportingProductRoute = '**/api/reportingProduct'
const tariffRoute = '**/api/tariff'
const capacityPoolRoute = '**/api/capacitypool'

const ProductBundle = Data.Inventory.Inventory.ProductBundle

const Tab = {
    IMAGES: 2,
    BOOKINGDATES: 3,
    PRODUCTSANDANCILLARIES: 4,
    AMENDMENTRULES: 5,
    CANCELLATIONRULES: 6,
    CHANNELS: 7
}

describe('Product Bundle Test', function () {
    beforeEach(function () {
        cy.logInNoPre('inventory')

        cy.route('GET', productBundleRoute).as('GetData')

        cy.get('[data-cy=sidebar-item-inventory]').click()
        cy.get('[data-cy=sidebar-item-inventory-product-bundles]').click()

        cy.wait('@GetData')
    })

    describe('Modify', function () {

        it('Modify Detail', function () {
            ModifyDetail(ProductBundle.Modified.Detail, ProductBundle.Original)
        })

        it('Check Modified Table', function () {
            CheckDetail(ProductBundle.Modified)
        })

        it('Revert Detail', function () {
            ModifyDetail(ProductBundle.Original.Detail, ProductBundle.Modified)
        })

        it('Check Reverted Table', function () {
            CheckDetail(ProductBundle.Original)
        })
    })
})

function ModifyDetail(testData, search) {

    cy.route('PUT', productBundleRoute + '/*').as('PutData')

    var modified = false

    productFunctions.RouteSetup()

    cy.get('.table-component__table__body > tr').each((tr) => {
        if (tr[0].cells[1].innerText == search.Detail.Name) {
            cy.wrap(tr[0].cells[3]).contains('Change').click()

            productFunctions.RouteWait()

            cy.get('[data-cy=product-bundle-detail-input-bundle-name]').clear().type(testData.Name)
            cy.get('[data-cy=product-bundle-detail-input-bundle-display-name]').clear().type(testData.DisplayName)
            cy.get('[data-cy=product-bundle-detail-select-reporting-product]').select(testData.ReportingProductBundle)

            customFunctions.SetCheckbox('[data-cy=product-bundles-detail-checkbox-upgrade-only] > .input__checkbox-input', testData.UpgradeOnly)

            cy.get('.editr--content').clear().type(testData.Description)
            cy.get('[data-cy=product-bundle-detail-select-priority]').select(testData.Priority)
            cy.get('[data-cy=product-bundle-detail-input-booking-fee-label]').clear().type(testData.BookingFeeLabel)

            customFunctions.SetCheckbox('[data-cy=product-bundles-detail-checkbox-can-amend] > .input__checkbox-input', testData.CanAmend)
            cy.get('[data-cy=product-bundle-detail-input-number-allowed-amendments]').clear().type(testData.NumAmend)

            customFunctions.SetCheckbox('[data-cy=product-bundles-detail-checkbox-allow-concurrent-booking] > .input__checkbox-input', testData.AllowConcurrentBooking)

            customFunctions.SetCheckbox('[data-cy=product-bundles-detail-checkbox-can-cancel] > .input__checkbox-input', testData.CanCancel)

            customFunctions.SetCheckbox('[data-cy=product-bundles-detail-checkbox-featured-product-bundle] > .input__checkbox-input', testData.FeaturedProductBundle)

            customFunctions.SetCheckbox('[data-cy=product-bundles-detail-checkbox-requires-promotion] > .input__checkbox-input', testData.RequiresPromotion)

            cy.get('[data-cy=product-bundle-detail-select-pricing-basis]').select(testData.PricingBasis)
            cy.get('[data-cy=product-bundle-detail-select-bundle-rule]').select(testData.ProductBundleRule)
            cy.get('[data-cy=product-bundle-detail-select-bundle-FAQ]').select(testData.ProductBundleFAQ)
            cy.get('[data-cy=product-bundle-detail-select-tac]').select(testData.TAC)

            customFunctions.SetCheckbox('[data-cy=product-bundles-detail-checkbox-display-sold-out] > .input__checkbox-input', testData.DisplaySoldOut)

            if (testData.PricingBasis == "Adjustment") {
                cy.get('[data-cy=product-bundle-detail-select-price-type-adjustment]').select(testData.PriceTypeAdjustment)
                cy.get('[data-cy=product-bundle-detail-input-price-type-value]').clear().type(testData.PriceTypeValue)
            }

            cy.get('[data-cy=save-product-bundle-btn]').click()

            cy.wait('@PutData').then((response) => {
                cy.wrap(response.status).should('eq', 200)
                modified = true
            })
        }
    }).then(() => {
        cy.wrap(modified).should('eq', true)
    })
}

function CheckDetail(search) {
    var modified = false
    cy.get('.table-component__table__body > tr').each((tr) => {
        if (tr[0].cells[1].innerText == search.Detail.Name) {
            cy.wrap(tr[0].cells[0].innerText).should('eq', search.Detail.Airport)
            cy.wrap(tr[0].cells[1].innerText).should('eq', search.Detail.Name)
            cy.wrap(tr[0].cells[2].innerText).should('eq', search.Detail.ReportingProductBundle)
            modified = true
        }
    }).then(() => {
        cy.wrap(modified).should('eq', true)
    })
}

