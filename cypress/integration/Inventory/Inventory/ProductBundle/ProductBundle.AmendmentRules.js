import * as customFunctions from '../../../../support/functions.js'
import * as Data from '../../../../fixtures/AdminSetup.json'
import * as productFunctions from '../../../../support/ProductBundleFunctions'

const productBundleRoute = '**/api/productbundle'
const airportRoute = '**/api/airport'
const reportingProductRoute = '**/api/reportingProduct'
const tariffRoute = '**/api/tariff'
const capacityPoolRoute = '**/api/capacitypool'

const ProductBundle = Data.Inventory.Inventory.ProductBundle
const AmendmentRules = {
    Original: ProductBundle.Original.AmendmentRules,
    Modified: ProductBundle.Modified.AmendmentRules
}

const Tab = {
    IMAGES: 2,
    BOOKINGDATES: 3,
    PRODUCTSANDANCILLARIES: 4,
    AMENDMENTRULES: 5,
    CANCELLATIONRULES: 6,
    CHANNELS: 7
}

describe('Product Bundle Amendment Rules Test', function () {
    beforeEach(function () {
        cy.logInNoPre('inventory')

        cy.route('GET', productBundleRoute).as('GetData')

        cy.get('[data-cy=sidebar-item-inventory]').click()
        cy.get('[data-cy=sidebar-item-inventory-product-bundles]').click()

        cy.wait('@GetData')

        productFunctions.VisitTab(ProductBundle.Original, Tab.AMENDMENTRULES)
    })

    it('Add Amendment Rules', function () {
        productFunctions.AddRule(AmendmentRules.Original, Tab.AMENDMENTRULES)
    })

    it('Check Amendment Rules', function () {
        productFunctions.CheckRule(AmendmentRules.Original, Tab.AMENDMENTRULES)
    })

    it('Modify Amendment Rules', function () {
        productFunctions.ModifyRule(AmendmentRules.Modified, Tab.AMENDMENTRULES)
    })

    it('Check Amendment Rules', function () {
        productFunctions.CheckRule(AmendmentRules.Modified, Tab.AMENDMENTRULES)
    })

    it('Delete Amendment Rules', function () {
        productFunctions.DeleteRule(Tab.AMENDMENTRULES)
    })
})
