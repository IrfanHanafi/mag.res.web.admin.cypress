import * as customFunctions from '../../../../support/functions.js'
import * as Data from '../../../../fixtures/AdminSetup.json'
import * as productFunctions from '../../../../support/ProductBundleFunctions'

const productBundleRoute = '**/api/productbundle'

const ProductBundle = Data.Inventory.Inventory.ProductBundle
const BookingDates = {
    Original: ProductBundle.Original.BookingDates,
    Modified: ProductBundle.Modified.BookingDates
}

const Tab = Data.Enum.ProductBundleTab

describe('Product Bundle Booking Dates Test', function () {
    beforeEach(function () {
        cy.logInNoPre('inventory')

        cy.route('GET', productBundleRoute).as('GetData')

        cy.get('[data-cy=sidebar-item-inventory]').click()
        cy.get('[data-cy=sidebar-item-inventory-product-bundles]').click()

        cy.wait('@GetData')

        productFunctions.VisitTab(ProductBundle.Original, Tab.BOOKINGDATES)
    })

    it('Modify Booking Dates', function () {
        ModifyBookingDates(BookingDates.Modified)
    })

    it('Check Modified Booking Dates', function () {
        CheckBookingDates(BookingDates.Modified)
    })

    it('Revert Booking Dates', function () {
        ModifyBookingDates(BookingDates.Original)
    })

    it('Check Reverted Booking Dates', function () {
        CheckBookingDates(BookingDates.Original)
    })
})

function ModifyBookingDates(testData) {

    var modified = false
    cy.route('PUT', productBundleRoute + '/*/bookingdate/*').as('PutBookingDate')
    var fromDate = customFunctions.GetDate(testData.StartDate)
    var toDate = customFunctions.GetDate(testData.EndDate)

    cy.get('[data-cy=product-bundle-booking-dates-table-container] > .table-component__table-wrapper > .table-component__table > .table-component__table__body > tr').then((tr) => {
        cy.wrap(tr[0].cells[2]).contains('Change').click()

        cy.wait('@GetBookingDate')

        customFunctions.SetDate('#bookingDateFrom', fromDate)
        customFunctions.SetDate('#bookingDateTo', toDate)

        cy.get('[data-cy=product-bundle-booking-dates-button-save]').click()

        cy.wait('@PutBookingDate').then((response) => {
            cy.wrap(response.status).should('eq', 200)
            modified = true
        })
    }).then(() => {
        cy.wrap(modified).should('eq', true)
    })
}

function CheckBookingDates(testData) {
    var modified = false
    var fromDate = customFunctions.GetDate(testData.StartDate).format('DD MMM YYYY')
    var toDate = customFunctions.GetDate(testData.EndDate).format('DD MMM YYYY')
    cy.get('[data-cy=product-bundle-booking-dates-table-container] > .table-component__table-wrapper > .table-component__table > .table-component__table__body > tr').then((tr) => {
        cy.wrap(tr[0].cells[0].innerText).should('eq', fromDate)
        cy.wrap(tr[0].cells[1].innerText).should('eq', toDate)
        modified = true
    }).then(() => {
        cy.wrap(modified).should('eq', true)
    })
}