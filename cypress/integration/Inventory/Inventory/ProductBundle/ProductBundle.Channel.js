import * as customFunctions from '../../../../support/functions.js'
import * as Data from '../../../../fixtures/AdminSetup.json'
import * as productFunctions from '../../../../support/ProductBundleFunctions'

const productBundleRoute = '**/api/productbundle'
const airportRoute = '**/api/airport'
const tariffRoute = '**/api/tariff'
const capacityPoolRoute = '**/api/capacitypool'

const ProductBundle = Data.Inventory.Inventory.ProductBundle
const Channel = {
    Original: ProductBundle.Original.Channel,
    Modified: ProductBundle.Modified.Channel
}

const Tab = Data.Enum.ProductBundleTab

const LinkStatus = {
    LINK: "Link",
    UNLINK: "Unlink"
}

describe('Product Bundle Channel Test', function () {
    beforeEach(function () {
        cy.logInNoPre('inventory')

        cy.route('GET', productBundleRoute).as('GetData')

        cy.get('[data-cy=sidebar-item-inventory]').click()
        cy.get('[data-cy=sidebar-item-inventory-product-bundles]').click()

        cy.wait('@GetData')

        productFunctions.VisitTab(ProductBundle.Original, Tab.CHANNELS)
    })

    it('Modify Channel', function () {
        ModifyChannel(Channel.Modified, Channel.Original)
    })

    it('Check Modified Channel', function () {
        CheckChannel(Channel.Modified)
    })

    it('Revert Channel', function () {
        ModifyChannel(Channel.Original, Channel.Modified)
    })

    it('Check Reverted Channel', function () {
        CheckChannel(Channel.Original)
    })

    it('Unlink Channel', function () {
        LinkChannel(Channel.Original, LinkStatus.UNLINK)
    })

    it('Link Channel', function () {
        LinkChannel(Channel.Original, LinkStatus.LINK)
    })

    it('Modify Channel Booking Dates', function () {
        ModifyChannelBookingDates(Channel.Modified.BookingDates, Channel.Original)
    })

    it('Modify Channel Booking Dates', function () {
        CheckChannelBookingDates(Channel.Modified.BookingDates, Channel.Original)
    })

    it('Revert Channel Booking Dates', function () {
        ModifyChannelBookingDates(Channel.Original.BookingDates, Channel.Original)
    })

    it('Modify Channel Booking Dates', function () {
        CheckChannelBookingDates(Channel.Original.BookingDates, Channel.Original)
    })

})

function ModifyChannel(testData, search) {
    cy.route('GET', tariffRoute).as('GetTariff')
    cy.route('GET', productBundleRoute + '/*/channel/*').as('GetChannel')
    cy.route('GET', tariffRoute + '/*/ladder').as('GetLadder')
    cy.route('GET', airportRoute + '/*/channel').as('GetAirportChannel')
    cy.route('GET', capacityPoolRoute).as('GetCapacityPool')

    cy.get('[data-cy=product-bundle-channels-table-container] > .table-component__table-wrapper > .table-component__table > .table-component__table__body > tr').each((tr) => {
        if (tr[0].cells[0].innerText == search.Name) {
            cy.wrap(tr[0].cells[6]).contains('Change').click()

            cy.wait('@GetTariff')
            cy.wait('@GetChannel')
            cy.wait('@GetLadder')
            cy.wait('@GetAirportChannel')
            cy.wait('@GetCapacityPool')

            customFunctions.SetCheckbox('[data-cy=product-bundles-channels-checkbox-rule-active] > .input__checkbox-input', testData.AllAffiliates)

            cy.get('[data-cy=product-bundle-channels-select-tariff]').select(testData.Tariff)

            cy.wait('@GetLadder')

            cy.get('[data-cy=product-bundle-channels-select-default-price-ladder]').select(testData.DefaultPriceLadder)
            cy.get('[data-cy=product-bundle-channels-select-car-park-capacity-pool]').select(testData.CarParkCapacityPool)
            cy.get('[data-cy=product-bundle-channels-select-capacity-pool]').select(testData.CapacityPool)
        }
    })
}


function CheckChannel(testData) {
    var modified = false
    cy.get('[data-cy=product-bundle-channels-table-container] > .table-component__table-wrapper > .table-component__table > .table-component__table__body > tr').each((tr) => {

        if (tr[0].cells[0].innerText == testData.Name) {
            cy.wrap(tr[0].cells[0].innerText).should('eq', testData.Name)
            cy.wrap(tr[0].cells[1].children[0].children[1].control.checked).should('eq', testData.AllAffiliates)
            cy.wrap(tr[0].cells[2].innerText).should('eq', testData.Tariff)
            cy.wrap(tr[0].cells[3].innerText).should('eq', testData.DefaultPriceLadder)
            cy.wrap(tr[0].cells[4].innerText).should('eq', testData.CarParkCapacityPool)
            cy.wrap(tr[0].cells[5].innerText).should('eq', testData.CapacityPool)
            modified = true
        }
    }).then(() => {
        cy.wrap(modified).should('eq', true)
    })
}

function LinkChannel(testData, action) {
    var modified = false

    var linkState = ""
    var linkUrl = ""
    if (action == LinkStatus.LINK) {
        linkState = "Link"
        linkUrl = "link"
    }
    else if (action == LinkStatus.UNLINK) {
        linkState = "Unlink"
        linkUrl = "unlink"
    }

    cy.route('PUT', productBundleRoute + '/*/channel/*/' + linkUrl).as(linkState)

    cy.get('[data-cy=product-bundle-channels-table-container] > .table-component__table-wrapper > .table-component__table > .table-component__table__body > tr').each((tr) => {

        if (tr[0].cells[0].innerText == testData.Name) {
            cy.wrap(tr[0].cells[6]).contains(linkState).click()

            cy.wait(`@${linkState}`).then((response) => {
                cy.wrap(response.status).should('eq', 200)
                modified = true
            })
        }
    }).then(() => {
        cy.wrap(modified).should('eq', true)
    })
}

function ModifyChannelBookingDates(testData, search) {

    var modified = false

    cy.route('PUT', productBundleRoute + '/*/channel/*/BookingDate/*').as('PutBookingDates')
    cy.route('GET', productBundleRoute + '/*/channel/*/BookingDate').as('GetBookingDates')
    cy.route('GET', productBundleRoute + '/*/channel/*/BookingDate/*').as('GetDate')
    cy.get('[data-cy=product-bundle-channels-table-container] > .table-component__table-wrapper > .table-component__table > .table-component__table__body > tr').each((tr) => {

        if (tr[0].cells[0].innerText == search.Name) {
            cy.wrap(tr[0].cells[6]).contains('Booking Dates').click()

            cy.wait('@GetBookingDates')

            cy.get('.table-component__table__body > tr').then((tr) => {
                cy.wrap(tr[0].cells[2]).contains('Change').click()

                cy.wait('@GetDate')

                FillChannelBookingDates(testData)

                cy.wait('@PutBookingDates').then((response) => {
                    cy.wrap(response.status).should('eq', 200)
                    modified = true
                })
            })
        }
    }).then(() => {
        cy.wrap(modified).should('eq', true)
    })
}

function FillChannelBookingDates(testData) {
    cy.wait(500)

    var StartDate = customFunctions.GetDate(testData.StartDate)
    var EndDate = customFunctions.GetDate(testData.EndDate)

    customFunctions.SetDate('#startDate', StartDate)

    customFunctions.SetDate('#endDate', EndDate)

    cy.get('[data-cy=save-channel-booking-date-btn]').click()
}

function CheckChannelBookingDates(testData, search) {

    var modified = false
    cy.route('GET', productBundleRoute + '/*/channel/*/BookingDate').as('GetBookingDates')

    var StartDate = customFunctions.GetDate(testData.StartDate).format('DD MMM YYYY')
    var EndDate = customFunctions.GetDate(testData.EndDate).format('DD MMM YYYY')

    cy.get('[data-cy=product-bundle-channels-table-container] > .table-component__table-wrapper > .table-component__table > .table-component__table__body > tr').each((tr) => {

        if (tr[0].cells[0].innerText == search.Name) {
            cy.wrap(tr[0].cells[6]).contains('Booking Dates').click()

            cy.wait('@GetBookingDates')

            cy.get('.table-component__table__body > tr').then((tr) => {
                cy.wrap(tr[0].cells[0].innerText).should('eq', StartDate)
                cy.wrap(tr[0].cells[1].innerText).should('eq', EndDate)

                modified = true
            })
        }
    }).then(() => {
        cy.wrap(modified).should('eq', true)
    })
}
