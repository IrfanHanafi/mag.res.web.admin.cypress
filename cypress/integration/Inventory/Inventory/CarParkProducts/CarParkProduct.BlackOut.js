import * as customFunctions from '../../../../support/functions.js'
import * as Data from '../../../../fixtures/AdminSetup.json'
import * as cppFunctions from '../../../../support/CarParkProductFunctions.js'

const productRoute = '**/api/product'
const productTariffRoute = '**/api/product/tariff'
const carParkRoute = '**/api/carpark'
const airportRoute = '**/api/airport'

const CarParkProduct = Data.Inventory.Inventory.CarParkProduct
const BlackOut = {
    Original: CarParkProduct.Original.BlackOut,
    Modified: CarParkProduct.Modified.BlackOut
}

const Tab = Data.Enum.CarParkProductTab

describe('Car Park Product Black Out Test', function () {
    beforeEach(function () {
        cy.logInNoPre('inventory')

        cy.route('GET', productRoute).as('GetData')
        cy.route('GET', productTariffRoute).as('GetProductTariff')
        cy.route('GET', carParkRoute).as('GetCarPark')
        cy.route('GET', airportRoute).as('GetAirport')

        cy.get('[data-cy=sidebar-item-inventory]').click()
        cy.get('[data-cy=sidebar-item-inventory-car-park-products]').click()

        cy.wait('@GetData')
        cy.wait('@GetProductTariff')
        cy.wait('@GetCarPark')
        cy.wait('@GetAirport')

        cppFunctions.VisitTab(CarParkProduct.Original, Tab.BLACKOUTS)

        cy.wait('@GetBlackOut')
    })

    it('Add Black Out', function () {
        AddBlackOut(BlackOut.Original)
    })

    it('Check Black Out', function () {
        CheckBlackOut(BlackOut.Original)
    })

    it('Modified Black Out', function () {
        ModifyBlackOut(BlackOut.Modified, BlackOut.Original)
    })

    it('Check Black Out', function () {
        CheckBlackOut(BlackOut.Modified)
    })

    it('Delete Black Out', function () {
        DeleteBlackOut(BlackOut.Modified)
    })
})

function AddBlackOut(testData) {

    cy.get('[data-cy=car-park-p-back-out-create-btn]').click()

    cy.route('POST', productRoute + '/*/blackout').as('PostBlackOut')

    FillBlackOut(testData)

    cy.wait('@PostBlackOut').then((response) => {
        cy.wrap(response.status).should('eq', 200)
    })
}

function ModifyBlackOut(testData, search) {

    cy.route('GET', productRoute + '/blackout/*').as('GetProductBlackOut')
    cy.route('PUT', productRoute + '/blackout/*').as('PutBlackOut')
    var modified = false
    cy.get('[data-cy=car-park-product-black-out-table-container] > tbody > tr').each((tr) => {
        if (tr[0].cells[0].innerText == search.Description) {
            cy.wrap(tr[0].cells[5]).contains('Change').click()
            cy.wait('@GetProductBlackOut')

            FillBlackOut(testData)

            cy.wait('@PutBlackOut').then((response) => {
                cy.wrap(response.status).should('eq', 200)
                modified = true
            })
        }
    }).then(() => {
        cy.wrap(modified).should('eq', true)
    })

    cy.get('.container > div > .btn').click()
}

function CheckBlackOut(testData) {
    var fromDate = customFunctions.GetDate(testData.StartDate)
    var startTime = fromDate.format("HH:mm")

    var toDate = customFunctions.GetDate(testData.EndDate)
    var endTime = toDate.format("HH:mm")

    var modified = false
    cy.get('[data-cy=car-park-product-black-out-table-container] > tbody > tr').each((tr) => {
        if (tr[0].cells[0].innerText == testData.Description) {
            cy.wrap(tr[0].cells[0].innerText).should('eq', testData.Description)
            cy.wrap(tr[0].cells[1].innerText).should('eq', fromDate.format('DD/MM/YYYY'))
            cy.wrap(tr[0].cells[2].innerText).should('eq', startTime)
            cy.wrap(tr[0].cells[3].innerText).should('eq', toDate.format('DD/MM/YYYY'))
            cy.wrap(tr[0].cells[4].innerText).should('eq', endTime)
            modified = true
        }
    }).then(() => {
        cy.wrap(modified).should('eq', true)
    })
}

function FillBlackOut(testData) {
    var fromDate = customFunctions.GetDate(testData.StartDate)
    var startTime = fromDate.format("HH:mm")

    var toDate = customFunctions.GetDate(testData.EndDate)
    var endTime = toDate.format("HH:mm")

    cy.get('[data-cy=car-park-product-black-out-input-description]').clear().type(testData.Description)

    customFunctions.SetDate('[data-cy=car-park-product-black-out-calendar-start-date] > .field > .input > #startDate', fromDate)

    cy.get('[data-cy=car-park-product-black-out-input-start-time]').clear().type(startTime)

    customFunctions.SetDate('[data-cy=car-park-product-black-out-calendar-end-date] > .field > .input > #startDate', toDate)

    cy.get('[data-cy=car-park-product-black-out-input-end-time]').clear().type(endTime)

    cy.get('[data-cy=car-park-p-back-out-save-btn]').click()
}

function DeleteBlackOut(testData) {
    cy.route('DELETE', productRoute + '/blackout/*').as('DeleteBlackOut')
    var modified = false
    cy.get('[data-cy=car-park-product-black-out-table-container] > tbody > tr').each((tr) => {
        if (tr[0].cells[0].innerText == testData.Description) {
            cy.wrap(tr[0].cells[5]).contains('Delete').click()
            cy.get('.modal-footer > .btn').click()

            cy.wait('@DeleteBlackOut').then((response) => {
                cy.wrap(response.status).should('eq', 200)
                modified = true
            })
        }
    }).then(() => {
        cy.wrap(modified).should('eq', true)
    })
}