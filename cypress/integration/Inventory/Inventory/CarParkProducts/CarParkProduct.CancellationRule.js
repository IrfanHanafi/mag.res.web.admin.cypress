import * as customFunctions from '../../../../support/functions.js'
import * as Data from '../../../../fixtures/AdminSetup.json'
import * as cppFunctions from '../../../../support/CarParkProductFunctions.js'

const productRoute = '**/api/product'
const productTariffRoute = '**/api/product/tariff'
const carParkRoute = '**/api/carpark'
const airportRoute = '**/api/airport'

const CarParkProduct = Data.Inventory.Inventory.CarParkProduct
const CancellationRules = {
    Original: CarParkProduct.Original.CancellationRules,
    Modified: CarParkProduct.Modified.CancellationRules
}

const Tab = Data.Enum.CarParkProductTab

describe('Car Park Product Cancellation Rule Test', function () {
    beforeEach(function () {
        cy.logInNoPre('inventory')

        cy.route('GET', productRoute).as('GetData')
        cy.route('GET', productTariffRoute).as('GetProductTariff')
        cy.route('GET', carParkRoute).as('GetCarPark')
        cy.route('GET', airportRoute).as('GetAirport')

        cy.get('[data-cy=sidebar-item-inventory]').click()
        cy.get('[data-cy=sidebar-item-inventory-car-park-products]').click()

        cy.wait('@GetData')
        cy.wait('@GetProductTariff')
        cy.wait('@GetCarPark')
        cy.wait('@GetAirport')

        cppFunctions.VisitTab(CarParkProduct.Original, Tab.CANCELLATIONRULES)

        cy.wait('@GetCancellationRule')
    })

    it('Add Cancellation Rule', function () {
        cppFunctions.AddRule(CancellationRules.Original, Tab.CANCELLATIONRULES)
    })

    it('Check Cancellation Rule', function () {
        cppFunctions.CheckRule(CancellationRules.Original, Tab.CANCELLATIONRULES)
    })

    it('Modify Cancellation Rule', function () {
        cppFunctions.ModifyRule(CancellationRules.Modified, Tab.CANCELLATIONRULES)
    })

    it('Check Modified Cancellation Rule', function () {
        cppFunctions.CheckRule(CancellationRules.Modified, Tab.CANCELLATIONRULES)
    })

    it('Delete Cancellation Rule', function () {
        cppFunctions.DeleteRule(Tab.CANCELLATIONRULES)
    })
})