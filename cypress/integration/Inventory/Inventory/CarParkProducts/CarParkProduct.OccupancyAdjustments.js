import * as customFunctions from '../../../../support/functions.js'
import * as Data from '../../../../fixtures/AdminSetup.json'
import * as cppFunctions from '../../../../support/CarParkProductFunctions.js'

const productRoute = '**/api/product'
const productTariffRoute = '**/api/product/tariff'
const carParkRoute = '**/api/carpark'
const airportRoute = '**/api/airport'

const CarParkProduct = Data.Inventory.Inventory.CarParkProduct
const OccupancyAdjustment = {
    Original: CarParkProduct.Original.OccupancyAdjustment,
    Modified: CarParkProduct.Modified.OccupancyAdjustment
}

const Tab = Data.Enum.CarParkProductTab

describe('Car Park Product Occupancy Adjustments Test', function () {
    beforeEach(function () {
        cy.logInNoPre('inventory')

        cy.route('GET', productRoute).as('GetData')
        cy.route('GET', productTariffRoute).as('GetProductTariff')
        cy.route('GET', carParkRoute).as('GetCarPark')
        cy.route('GET', airportRoute).as('GetAirport')

        cy.get('[data-cy=sidebar-item-inventory]').click()
        cy.get('[data-cy=sidebar-item-inventory-car-park-products]').click()

        cy.wait('@GetData')
        cy.wait('@GetProductTariff')
        cy.wait('@GetCarPark')
        cy.wait('@GetAirport')

        cppFunctions.VisitTab(CarParkProduct.Original, Tab.OCCUPANCYADJUSTMENTS)

        cy.wait('@GetOccupancyAdjustment')
        cy.wait('@GetOccupancyPricing')
    })

    it('Add Occupancy Adjustment', function () {
        cppFunctions.AddAdjustment(OccupancyAdjustment.Original, Tab.OCCUPANCYADJUSTMENTS)
    })

    it('Check Occupancy Adjustment', function () {
        cppFunctions.CheckAdjustment(OccupancyAdjustment.Original, Tab.OCCUPANCYADJUSTMENTS)
    })

    it('Delete Occupancy Adjustment', function () {
        cppFunctions.DeleteAdjustment(OccupancyAdjustment.Original, Tab.OCCUPANCYADJUSTMENTS)
    })
})