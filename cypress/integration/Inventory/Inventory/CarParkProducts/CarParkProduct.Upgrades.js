import * as customFunctions from '../../../../support/functions.js'
import * as Data from '../../../../fixtures/AdminSetup.json'
import * as cppFunctions from '../../../../support/CarParkProductFunctions.js'

const productRoute = '**/api/product'
const productTariffRoute = '**/api/product/tariff'
const carParkRoute = '**/api/carpark'
const airportRoute = '**/api/airport'

const CarParkProduct = Data.Inventory.Inventory.CarParkProduct
const Upgrades = {
    Original : CarParkProduct.Original.Upgrades,
    Modified : CarParkProduct.Modified.Upgrades
}

const Tab = Data.Enum.CarParkProductTab

describe('Car Park Product Upgrades Test', function () {
    beforeEach(function () {
        cy.logInNoPre('inventory')

        cy.route('GET', productRoute).as('GetData')
        cy.route('GET', productTariffRoute).as('GetProductTariff')
        cy.route('GET', carParkRoute).as('GetCarPark')
        cy.route('GET', airportRoute).as('GetAirport')

        cy.get('[data-cy=sidebar-item-inventory]').click()
        cy.get('[data-cy=sidebar-item-inventory-car-park-products]').click()

        cy.wait('@GetData')
        cy.wait('@GetProductTariff')
        cy.wait('@GetCarPark')
        cy.wait('@GetAirport')

        cppFunctions.VisitTab(CarParkProduct.Original, Tab.UPGRADES)

        cy.wait('@GetUpgrade')
        cy.wait('@GetAirportProduct')
        cy.wait('@GetProductBundles')
    })

    it('Add Upgrades', function () {
        AddUpgrades(Upgrades.Original)
    })

    it('Check Upgrades', function () {
        CheckUpgrades(Upgrades.Original)
    })

    it('Delete Upgrades', function () {
        DeleteUpgrades(Upgrades.Original)
    })
})

function AddUpgrades(testData) {
    cy.route('POST', productRoute + '/*/upgradeOption').as('PostUpgrade')

    var fromDate = customFunctions.GetDate(testData.ArrivalStartDate)
    var toDate = customFunctions.GetDate(testData.ArrivalEndDate)

    customFunctions.SetDate('#UpgradeStartDate', fromDate)
    customFunctions.SetDate('#UpgradeEndDate', toDate)

    cy.get('[data-cy=car-park-product-upgrades-select-upgrade]').select(testData.UpgradeTo)
    cy.get('[data-cy=car-park-p-upgrade-add-btn]').click()

    cy.wait('@PostUpgrade').then((response) => {
        cy.wrap(response.status).should('eq', 200)
    })
}

function CheckUpgrades(testData) {
    var fromDate = customFunctions.GetDate(testData.ArrivalStartDate)
    var toDate = customFunctions.GetDate(testData.ArrivalEndDate)

    var modified = false

    cy.get('[data-cy=car-park-product-upgrades-table-container] > .table-component__table-wrapper > .table-component__table > .table-component__table__body > tr').each((tr) => {
        debugger
        if (tr[0].cells[2].innerText == testData.UpgradeTo) {
            cy.wrap(tr[0].cells[0].innerText).should('eq', fromDate.format('YYYY-MM-DD HH:mm'))
            cy.wrap(tr[0].cells[1].innerText).should('eq', toDate.format('YYYY-MM-DD HH:mm'))
            cy.wrap(tr[0].cells[2].innerText).should('eq', testData.UpgradeTo)
            cy.wrap(tr[0].cells[3].innerText).should('eq', testData.Active)
            modified = true
        }
    }).then(() => {
        cy.wrap(modified).should('eq', true)
    })
}

function DeleteUpgrades(testData) {
    var modified = false
    cy.route('DELETE', productRoute + '/upgradeOption/*').as('DeleteUpgrade')

    cy.get('[data-cy=car-park-product-upgrades-table-container] > .table-component__table-wrapper > .table-component__table > .table-component__table__body > tr').each((tr) => {
        if (tr[0].cells[2].innerText == testData.UpgradeTo) {
            cy.wrap(tr[0].cells[4]).contains('Delete').click()
            cy.get('.modal-footer > .btn').click()

            cy.wait('@DeleteUpgrade').then((response) => {
                cy.wrap(response.status).should('eq', 204)
                modified = true
            })
        }
    }).then(() => {
        cy.wrap(modified).should('eq', true)
    })
}
