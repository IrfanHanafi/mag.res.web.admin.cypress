import * as customFunctions from '../../../../support/functions.js'
import * as Data from '../../../../fixtures/AdminSetup.json'
import * as cppFunctions from '../../../../support/CarParkProductFunctions.js'

const productRoute = '**/api/product'
const productTariffRoute = '**/api/product/tariff'
const carParkRoute = '**/api/carpark'
const airportRoute = '**/api/airport'

const CarParkProduct = Data.Inventory.Inventory.CarParkProduct
const ProductContent = {
    Original: CarParkProduct.Original.ProductContent,
    Modified: CarParkProduct.Modified.ProductContent
}

const Tab = Data.Enum.CarParkProductTab

describe('Car Park Product Product Content Test', function () {
    beforeEach(function () {
        cy.logInNoPre('inventory')

        cy.route('GET', productRoute).as('GetData')
        cy.route('GET', productTariffRoute).as('GetProductTariff')
        cy.route('GET', carParkRoute).as('GetCarPark')
        cy.route('GET', airportRoute).as('GetAirport')

        cy.get('[data-cy=sidebar-item-inventory]').click()
        cy.get('[data-cy=sidebar-item-inventory-car-park-products]').click()

        cy.wait('@GetData')
        cy.wait('@GetProductTariff')
        cy.wait('@GetCarPark')
        cy.wait('@GetAirport')

        cppFunctions.VisitTab(CarParkProduct.Original, Tab.PRODUCTCONTENT)

        cy.wait('@GetPageArea')
        cy.wait('@GetContent')
    })

    it('Add Product Content', function () {
        AddContent(ProductContent.Original)
    })

    it('Check Product Content', function () {
        CheckContent(ProductContent.Original)
    })

    it('Modify Product Content', function () {
        ModifyContent(ProductContent.Modified, ProductContent.Original)
    })

    it('Check Modified Product Content', function () {
        CheckContent(ProductContent.Modified)
    })

    it('Delete Product Content', function () {
        DeleteContent(ProductContent.Modified)
    })
})

function AddContent(testData) {
    cy.route('POST', productRoute + '/*/content').as('SendContent')
    cy.get('[data-cy=car-park-p-content-create-btn]').click()

    FillContent(testData)
}

function ModifyContent(testData, target) {
    cy.route('PUT', productRoute + '/content/*').as('SendContent')

    var modified = false

    cy.get('.table-component__table__body > tr').each((tr) => {
        if (tr[0].cells[1].innerText == target.Name) {
            cy.wrap(tr[0].cells[3]).contains('Change').click()
            FillContent(testData)

            modified = true
            cy.get('.container > div > .btn').click()
        }
    }).then(() => {
        cy.wrap(modified).should('eq', true)
    })
}

function DeleteContent(testData) {
    cy.route('Delete', productRoute + '/content/*').as('DeleteContent')

    var modified = false

    cy.get('.table-component__table__body > tr').each((tr) => {
        if (tr[0].cells[1].innerText == testData.Name) {
            cy.wrap(tr[0].cells[3]).contains('Delete').click()
            cy.get('.modal-footer > .btn').click()

            cy.wait('@DeleteContent').then((response) => {
                cy.wrap(response.status).should('eq', 200)
            })

            modified = true

        }
    }).then(() => {
        cy.wrap(modified).should('eq', true)
    })
}

function CheckContent(testData) {

    var modified = false
    cy.get('.table-component__table__body > tr').each((tr) => {
        if (tr[0].cells[1].innerText == testData.Name) {

            cy.wrap(tr[0].cells[0].innerText).should('eq', testData.DisplayOrder)
            cy.wrap(tr[0].cells[1].innerText).should('eq', testData.Name)
            cy.wrap(tr[0].cells[2].innerText).should('eq', testData.Content)

            modified = true

        }
    }).then(() => {
        cy.wrap(modified).should('eq', true)
    })
}

function FillContent(testData) {
    cy.get('[data-cy=car-park-p-content-input-name]').clear().type(testData.Name)

    cy.get('.editr--content').clear().type(testData.Content)

    cy.get('[data-cy=car-park-p-content-select-display-order]').select(testData.DisplayOrder)

    cy.get('[data-cy=car-park-p-content-save-btn]').click()

    cy.wait('@SendContent').then((response) => {
        cy.wrap(response.status).should('eq', 200)
    })
}