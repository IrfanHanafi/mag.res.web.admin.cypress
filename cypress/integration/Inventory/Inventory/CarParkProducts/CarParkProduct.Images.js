import * as customFunctions from '../../../../support/functions.js'
import * as Data from '../../../../fixtures/AdminSetup.json'
import * as cppFunctions from '../../../../support/CarParkProductFunctions.js'

const productRoute = '**/api/product'
const productTariffRoute = '**/api/product/tariff'
const carParkRoute = '**/api/carpark'
const airportRoute = '**/api/airport'

const CarParkProduct = Data.Inventory.Inventory.CarParkProduct
const Images = {
    Original: CarParkProduct.Original.Images,
    Modified: CarParkProduct.Modified.Images
}

const Tab = Data.Enum.CarParkProductTab

describe('Car Park Product Images Adjustments Test', function () {
    beforeEach(function () {
        cy.logInNoPre('inventory')

        cy.route('GET', productRoute).as('GetData')
        cy.route('GET', productTariffRoute).as('GetProductTariff')
        cy.route('GET', carParkRoute).as('GetCarPark')
        cy.route('GET', airportRoute).as('GetAirport')

        cy.get('[data-cy=sidebar-item-inventory]').click()
        cy.get('[data-cy=sidebar-item-inventory-car-park-products]').click()

        cy.wait('@GetData')
        cy.wait('@GetProductTariff')
        cy.wait('@GetCarPark')
        cy.wait('@GetAirport')

        cppFunctions.VisitTab(CarParkProduct.Original, Tab.IMAGES)
    })

    it('Add Images', function () {
        AddImages(Images.Original)
    })

    it('Check Images', function () {
        CheckImages(Images.Original)
    })

    it('Delete Images', function () {
        DeleteImages()
    })
})

function AddImages(testData) {

    cy.route('PUT', productRoute + '/*/images').as('PutImages')

    customFunctions.FileUpload('[data-cy=car-park-product-images-file-upload-product-icon]', testData.ProductIcon.Path, testData.ProductIcon.FileName)

    customFunctions.FileUpload('[data-cy=car-park-product-images-file-upload-list-view-logo]', testData.ListViewLogo.Path, testData.ListViewLogo.FileName)

    cy.get('[data-cy=car-park-p-save-images-btn]').click()

    cy.wait('@PutImages').then((response) => {
        cy.wrap(response.status).should('eq', 200)
    })
}

function CheckImages(testData) {
    var found = false
    cy.get(':nth-child(2) > :nth-child(1) > img').then((image) => {
        var imageSrcSplit = image[0].src.split("/")
        var imageSrc = imageSrcSplit[imageSrcSplit.length - 1]
        cy.wrap(imageSrc).should('eq', testData.ProductIcon.FileName)
    })

    cy.get(':nth-child(4) > :nth-child(1) > img').then((image) => {
        var imageSrcSplit = image[0].src.split("/")
        var imageSrc = imageSrcSplit[imageSrcSplit.length - 1]
        cy.wrap(imageSrc).should('eq', testData.ListViewLogo.FileName)
    })
}

function DeleteImages() {

    cy.route('PUT', productRoute + '/*/images').as('PutImages')

    cy.get('[data-cy=car-park-product-images-button-remove-product-icon]').click()

    cy.get('[data-cy=car-park-product-images-button-remove-list-view-logo]').click()

    cy.get('[data-cy=car-park-p-save-images-btn]').click()

    cy.wait('@PutImages').then((response) => {
        cy.wrap(response.status).should('eq', 200)
    })
}