import * as customFunctions from '../../../../support/functions.js'
import * as Data from '../../../../fixtures/AdminSetup.json'
import * as cppFunctions from '../../../../support/CarParkProductFunctions.js'

const productRoute = '**/api/product'
const productTariffRoute = '**/api/product/tariff'
const carParkRoute = '**/api/carpark'
const airportRoute = '**/api/airport'

const CarParkProduct = Data.Inventory.Inventory.CarParkProduct
const BookingDates = {
    Original : CarParkProduct.Original.BookingDates,
    Modified : CarParkProduct.Modified.BookingDates
}

const Tab = Data.Enum.CarParkProductTab

describe('Car Park Product Booking Dates Test', function () {
    beforeEach(function () {
        cy.logInNoPre('inventory')

        cy.route('GET', productRoute).as('GetData')
        cy.route('GET', productTariffRoute).as('GetProductTariff')
        cy.route('GET', carParkRoute).as('GetCarPark')
        cy.route('GET', airportRoute).as('GetAirport')

        cy.get('[data-cy=sidebar-item-inventory]').click()
        cy.get('[data-cy=sidebar-item-inventory-car-park-products]').click()

        cy.wait('@GetData')
        cy.wait('@GetProductTariff')
        cy.wait('@GetCarPark')
        cy.wait('@GetAirport')

        cppFunctions.VisitTab(CarParkProduct.Original, Tab.BOOKINGDATES)

        cy.wait('@GetAvailability')
    })

    it('Add Booking Dates', function () {
        AddBookingDates(BookingDates.Original)
    })

    it('Check Booking Dates', function () {
        CheckBookingDates(BookingDates.Original)
    })

    it('Modify Booking Dates', function () {
        ModifyBookingDates(BookingDates.Modified)
    })

    it('Check Booking Dates', function () {
        CheckBookingDates(BookingDates.Modified)
    })

    it('Delete Booking Dates', function () {
        DeleteBookingDates()
    })
})

function AddBookingDates(testData) {
    cy.log('Adding Booking Dates')
    cy.route('POST', productRoute + '/*/availability').as('SendAvailability')
    cy.get('[data-cy=car-park-p-create-b-date-btn]').click()

    FillBookingDates(testData)
}

function ModifyBookingDates(testData) {
    cy.log('Modify Booking Dates')
    cy.route('GET', productRoute + '/availability/*').as('GetProductAvailability')
    cy.route('PUT', productRoute + '/*/availability/*').as('SendAvailability')

    cy.get('[data-cy=car-park-product-booking-dates-table-container] > .table-component__table-wrapper > .table-component__table > .table-component__table__body > tr').then((tr) => {
        cy.wrap(tr[0].cells[2]).contains('Change').click()

        cy.wait('@GetProductAvailability')

        FillBookingDates(testData)

        cy.get('.container > div > .btn').click()
    })

}

function FillBookingDates(testData) {

    var fromDate = customFunctions.GetDate(testData.StartDate)
    var toDate = customFunctions.GetDate(testData.EndDate)

    customFunctions.SetDate('#startDate', fromDate)
    customFunctions.SetDate('#endDate', toDate)

    cy.get('[data-cy=car-park-p-save-b-date-btn]').click()

    cy.wait('@SendAvailability').then((response) => {
        cy.wrap(response.status).should('eq', 200)
    })
}

function CheckBookingDates(testData) {
    var modified = false
    var fromDate = customFunctions.GetDate(testData.StartDate).format('DD MMM YYYY')
    var toDate = customFunctions.GetDate(testData.EndDate).format('DD MMM YYYY')

    cy.get('[data-cy=car-park-product-booking-dates-table-container] > .table-component__table-wrapper > .table-component__table > .table-component__table__body > tr').then((tr) => {
        cy.wrap(tr[0].cells[0].innerText).should('eq', fromDate)
        cy.wrap(tr[0].cells[1].innerText).should('eq', toDate)
        modified = true
    }).then(() => {
        cy.wrap(modified).should('eq', true)
    })
}

function DeleteBookingDates() {
    cy.log('Delete Booking Dates')
    var modified = false
    cy.route('DELETE', productRoute + '/availability/*').as('DeleteAvailability')

    cy.get('[data-cy=car-park-product-booking-dates-table-container] > .table-component__table-wrapper > .table-component__table > .table-component__table__body > tr').then((tr) => {
        cy.wrap(tr[0].cells[2]).contains('Delete').click()
        cy.get('.modal-footer > .btn').click()

        cy.wait('@DeleteAvailability').then((response) => {
            cy.wrap(response.status).should('eq', 200)
            modified = true
        })
    }).then(() => {
        cy.wrap(modified).should('eq', true)
    })
}