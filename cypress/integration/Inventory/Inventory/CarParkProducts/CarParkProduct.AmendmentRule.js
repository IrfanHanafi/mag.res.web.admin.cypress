import * as customFunctions from '../../../../support/functions.js'
import * as Data from '../../../../fixtures/AdminSetup.json'
import * as cppFunctions from '../../../../support/CarParkProductFunctions.js'

const productRoute = '**/api/product'
const productTariffRoute = '**/api/product/tariff'
const carParkRoute = '**/api/carpark'
const airportRoute = '**/api/airport'

const CarParkProduct = Data.Inventory.Inventory.CarParkProduct
const AmendmentRules = {
    Original: CarParkProduct.Original.AmendmentRules,
    Modified: CarParkProduct.Modified.AmendmentRules
}

const Tab = Data.Enum.CarParkProductTab

describe('Car Park Product Amendment Rule Test', function () {
    beforeEach(function () {
        cy.logInNoPre('inventory')

        cy.route('GET', productRoute).as('GetData')
        cy.route('GET', productTariffRoute).as('GetProductTariff')
        cy.route('GET', carParkRoute).as('GetCarPark')
        cy.route('GET', airportRoute).as('GetAirport')

        cy.get('[data-cy=sidebar-item-inventory]').click()
        cy.get('[data-cy=sidebar-item-inventory-car-park-products]').click()

        cy.wait('@GetData')
        cy.wait('@GetProductTariff')
        cy.wait('@GetCarPark')
        cy.wait('@GetAirport')

        cppFunctions.VisitTab(CarParkProduct.Original, Tab.AMENDMENTRULES)

        cy.wait('@GetAmendmentRule')
    })

    it('Add Amendment Rule', function () {
        cppFunctions.AddRule(AmendmentRules.Original, Tab.AMENDMENTRULES)
    })

    it('Check Amendment Rule', function () {
        cppFunctions.CheckRule(AmendmentRules.Original, Tab.AMENDMENTRULES)
    })

    it('Modify Amendment Rule', function () {
        cppFunctions.ModifyRule(AmendmentRules.Modified, Tab.AMENDMENTRULES)
    })

    it('Check Modified Amendment Rule', function () {
        cppFunctions.CheckRule(AmendmentRules.Modified, Tab.AMENDMENTRULES)
    })

    it('Delete Amendment Rule', function () {
        cppFunctions.DeleteRule(Tab.AMENDMENTRULES)
    })
})