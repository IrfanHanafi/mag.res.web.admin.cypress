import * as customFunctions from '../../../../support/functions.js'
import * as cppFunctions from '../../../../support/CarParkProductFunctions'
import * as Data from '../../../../fixtures/AdminSetup.json'

const productRoute = '**/api/product'
const productTariffRoute = '**/api/product/tariff'
const carParkRoute = '**/api/carpark'
const airportRoute = '**/api/airport'

const CarParkProduct = Data.Inventory.Inventory.CarParkProduct

describe('Car Park Product Detail Test', function () {
    beforeEach(function () {
        cy.logInNoPre('inventory')

        cy.route('GET', productRoute).as('GetData')
        cy.route('GET', productTariffRoute).as('GetProductTariff')
        cy.route('GET', carParkRoute).as('GetCarPark')
        cy.route('GET', airportRoute).as('GetAirport')

        cy.get('[data-cy=sidebar-item-inventory]').click()
        cy.get('[data-cy=sidebar-item-inventory-car-park-products]').click()

        cy.wait('@GetData')
        cy.wait('@GetProductTariff')
        cy.wait('@GetCarPark')
        cy.wait('@GetAirport')
    })

    it('Modify Car Park Products', function () {
        ModifyDetail(CarParkProduct.Modified.Detail, CarParkProduct.Original)
    })

    it('Check Modified Car Park Products', function () {
        CheckDetail(CarParkProduct.Modified.Detail)
    })

    it('Revert Car Park Products', function () {
        ModifyDetail(CarParkProduct.Original.Detail, CarParkProduct.Modified)
    })

    it('Check Reverted Car Park Products', function () {
        CheckDetail(CarParkProduct.Original.Detail)
    })
})


function ModifyDetail(testData, search) {

    cy.route('PUT', productRoute + '/*').as('PutData')

    cppFunctions.RouteSetup()

    cy.get('.table-component__table__body > tr').each((tr) => {
        if (tr[0].cells[3].innerText == search.Detail.ProductName) {
            cy.wrap(tr[0].cells[5]).contains('Change').click()

            cppFunctions.RouteWait()

            cy.get('[data-cy=car-park-product-detail-input-product-code]').clear().type(testData.ProductCode)
            cy.get('[data-cy=car-park-product-detail-input-product-name]').clear().type(testData.ProductName)
            cy.get('[data-cy=car-park-product-detail-input-display-name]').clear().type(testData.DisplayName)
            cy.get('[data-cy=car-park-product-detail-select-reporting-product]').select(testData.ReportingProduct)

            customFunctions.SetCheckbox('[data-cy=car-park-product-detail-checkbox-active] > .input__checkbox-input', testData.Active)
            customFunctions.SetCheckbox('[data-cy=car-park-product-detail-checkbox-product-offer-only] > .input__checkbox-input', testData.OfferOnly)
            customFunctions.SetCheckbox('[data-cy=car-park-product-detail-checkbox-product-featured] > .input__checkbox-input', testData.FeaturedProduct)

            cy.get('[data-cy=car-park-product-detail-input-description] > .editr--content').clear().type(testData.Description)
            cy.get('[data-cy=car-park-product-detail-select-more-info]').select(testData.MoreInfo)

            if (testData.MoreInfo == "Product More Info") {
                cy.get('[data-cy=car-park-product-detail-input-more-info-description] > .editr--content').clear().type(testData.ProductMoreInfo)
            }

            cy.get('[data-cy=car-park-product-detail-select-priority]').select(testData.Priority)
            cy.get('[data-cy=car-park-product-detail-select-arrival-instructions]').select(testData.ArrivalInstrunctions)
            cy.get('[data-cy=car-park-product-detail-select-departure-instructions]').select(testData.DepartureInstrunctions)
            cy.get('[data-cy=car-park-product-detail-select-email-template]').select(testData.EmailTemplate)
            cy.get('.colour-palette__picker')

            cy.get('[data-cy=car-park-product-pricing-select-pricing-basis]').select(testData.PricingBasis)
            cy.get('[data-cy=car-park-product-pricing-select-lead-time-adjustment]').select(testData.LeadTimeAdjustment)
            cy.get('[data-cy=car-park-product-pricing-select-master-product]').select(testData.MasterProduct)
            cy.get('[data-cy=car-park-product-pricing-select-surcharge-type]').select(testData.SurchargeType)
            cy.get('[data-cy=car-park-product-pricing-input-surcharge-amount]').clear().type(testData.SurchargeAmount)
            cy.get('[data-cy=car-park-product-pricing-select-tax-name]')

            customFunctions.SetCheckbox('[data-cy=car-park-product-capacity-checkbox-movements-management] > .input__checkbox-input', testData.MovementsManagement)
            cy.get('[data-cy="car-park-product-capacity-input-default capacity"]').clear().type(testData.DefaultCapacity)

            cy.get('[data-cy=car-park-product-rules-select-product-rule]').select(testData.ProductRule)
            cy.get('[data-cy=car-park-product-rules-select-product-faq]').select(testData.ProductFAQ)
            cy.get('[data-cy=car-park-product-rules-select-tac]').select(testData.TAC)

            customFunctions.SetCheckbox('[data-cy=car-park-product-rules-checkbox-display-sold-out] > .input__checkbox-input', testData.DisplaySoldOut)
            customFunctions.SetCheckbox('[data-cy=car-park-product-rules-checkbox-unlock-promo-code] > .input__checkbox-input', testData.UnlockPromo)

            cy.get('[data-cy=car-park-product-rules-input-promo-code]').clear().type(testData.PromotionCode)
            cy.get('[data-cy=car-park-product-rules-select-show-cheapest]').select(testData.ShowCheapest)

            customFunctions.SetCheckbox('[data-cy=car-park-product-amendment-rules-checkbox-inherit-amendment-rules] > .input__checkbox-input', testData.InheritAmend)
            customFunctions.SetCheckbox('[data-cy=car-park-product-amendment-rules-checkbox-can-amend] > .input__checkbox-input', testData.CanAmend)
            customFunctions.SetCheckbox('[data-cy=car-park-product-amendment-rules-checkbox-can-cancel] > .input__checkbox-input', testData.CanCancel)
            customFunctions.SetCheckbox('[data-cy=car-park-product-amendment-rules-checkbox-inherit-cancel] > .input__checkbox-input', testData.InheritCancel)
            customFunctions.SetCheckbox('[data-cy=car-park-product-amendment-rules-checkbox-allow-time-amendments] > .input__checkbox-input', testData.AllowTimeAmend)

            customFunctions.SetCheckbox('[data-cy=car-park-product-rules-checkbox-show-turn-up-rate] > .input__checkbox-input', testData.ShowTurnUpRate)

            if (testData.ShowTurnUpRate) {
                cy.get('[data-cy=add-ons-offer-lines-select-tariff]').select(testData.Tariff)
                cy.wait('@GetLadder')
                cy.get('[data-cy=add-ons-offer-lines-select-pricing-ladder]').select(testData.DefaultPricingLadder)
                cy.get('#turnUpRateSavingThresholdInput').clear().type(testData.PercentageThreshold)
            }

            cy.get('[data-cy=car-park-p-save-btn]').click()

            cy.wait('@PutData').then((response) => {
                cy.wrap(response.status).should('eq', 200)
            })
        }
    })
}

function CheckDetail(testData) {

    var modified = false

    cy.get('.table-component__table__body > tr').each((tr) => {
        if (tr[0].cells[1].innerText == testData.CarParkName && tr[0].cells[2].innerText == testData.ProductCode) {
            cy.wrap(tr[0].cells[0].innerText).should('eq', testData.AirportName)
            cy.wrap(tr[0].cells[1].innerText).should('eq', testData.CarParkName)
            cy.wrap(tr[0].cells[2].innerText).should('eq', testData.ProductCode)
            cy.wrap(tr[0].cells[3].innerText).should('eq', testData.ProductName)
            cy.wrap(tr[0].cells[4].innerText).should('eq', testData.Active.toString())
            modified = true
        }
    }).then(() => {
        cy.wrap(modified).should('eq', true)
    })
}