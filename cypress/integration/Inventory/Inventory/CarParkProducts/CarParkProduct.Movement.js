import * as customFunctions from '../../../../support/functions.js'
import * as Data from '../../../../fixtures/AdminSetup.json'
import * as cppFunctions from '../../../../support/CarParkProductFunctions.js'

const productRoute = '**/api/product'
const productTariffRoute = '**/api/product/tariff'
const carParkRoute = '**/api/carpark'
const airportRoute = '**/api/airport'

const CarParkProduct = Data.Inventory.Inventory.CarParkProduct
const Movement = {
    Original: CarParkProduct.Original.Movement,
    Modified: CarParkProduct.Modified.Movement
}

const Tab = Data.Enum.CarParkProductTab

describe('Car Park Product Movement Adjustments Test', function () {
    beforeEach(function () {
        cy.logInNoPre('inventory')

        cy.route('GET', productRoute).as('GetData')
        cy.route('GET', productTariffRoute).as('GetProductTariff')
        cy.route('GET', carParkRoute).as('GetCarPark')
        cy.route('GET', airportRoute).as('GetAirport')

        cy.get('[data-cy=sidebar-item-inventory]').click()
        cy.get('[data-cy=sidebar-item-inventory-car-park-products]').click()

        cy.wait('@GetData')
        cy.wait('@GetProductTariff')
        cy.wait('@GetCarPark')
        cy.wait('@GetAirport')

        cppFunctions.VisitTab(CarParkProduct.Original, Tab.MOVEMENTS)

        cy.wait('@GetMovement')
        cy.wait('@GetCapacityPool')
    })

    it('Add Movement', function () {
        AddMovement(Movement.Original)
    })

    it('Check Movement', function () {
        CheckMovement(Movement.Original)
    })

    it('Modify Movement', function () {
        ModifyMovement(Movement.Modified)
    })

    it('Check Modified Movement', function () {
        CheckMovement(Movement.Modified)
    })

    it('Delete Movement', function () {
        DeleteMovement()
    })
})

function AddMovement(testData) {

    cy.route('POST', productRoute + '/*/movementmanagement').as('PostMovement')

    cy.get('[data-cy=car-park-product-movement-select-movement-type]').select(testData.MovementType)
    cy.get('[data-cy=car-park-product-movement-select-type]').select(testData.Type)
    cy.get('[data-cy=car-park-product-movement-select-capacity-pool]').select(testData.CapacityPool)

    cy.get('[data-cy=car-park-product-movement-button-add]').click()

    cy.wait('@PostMovement').then((response) => {
        cy.wrap(response.status).should('eq', 200)
    })
}

function ModifyMovement(testData) {
    var modified = false

    cy.route('GET', productRoute + '/movementmanagement/*').as('GetProductMovement')
    cy.route('PUT', productRoute + '/*/movementmanagement/*').as('PutMovement')

    cy.get('[data-cy=car-park-product-movement-table-container] > .table-component__table-wrapper > .table-component__table > .table-component__table__body > tr').then((tr) => {
        cy.wrap(tr[0].cells[3]).contains('Edit').click()

        cy.wait('@GetProductMovement')
        cy.wait('@GetCapacityPool')

        cy.get('[data-cy=car-park-product-movement-form-select-movement-type]').select(testData.MovementType)
        cy.get('[data-cy=car-park-product-movement-form-select-type]').select(testData.Type)
        cy.get('[data-cy=car-park-product-movement-form-select-capacity-pool]').select(testData.CapacityPool)

        cy.get('.btn-lg').click()

        cy.wait('@PutMovement').then((response) => {
            cy.wrap(response.status).should('eq', 200)
            modified = true
        })
    }).then(() => {
        cy.wrap(modified).should('eq', true)
    })

    cy.get('.container > div > .btn').click()
}

function DeleteMovement() {
    var modified = false
    cy.route('Delete', productRoute + '/*/movementmanagement/*').as('DeleteMovement')

    cy.get('[data-cy=car-park-product-movement-table-container] > .table-component__table-wrapper > .table-component__table > .table-component__table__body > tr').then((tr) => {
        cy.wrap(tr[0].cells[3]).contains('Delete').click()
        cy.get('.btn-danger').click()

        cy.wait('@DeleteMovement').then((response) => {
            cy.wrap(response.status).should('eq', 200)
            modified = true
        })
    }).then(() => {
        cy.wrap(modified).should('eq', true)
    })
}

function CheckMovement(testData) {
    var modified = false
    cy.get('[data-cy=car-park-product-movement-table-container] > .table-component__table-wrapper > .table-component__table > .table-component__table__body > tr').then((tr) => {
        cy.wrap(tr[0].cells[0].innerText).should('eq', testData.MovementType)
        cy.wrap(tr[0].cells[1].innerText).should('eq', testData.Type)
        cy.wrap(tr[0].cells[2].innerText).should('eq', testData.CapacityPool)
        modified = true
    }).then(() => {
        cy.wrap(modified).should('eq', true)
    })
}