import * as customFunctions from '../../../../support/functions.js'
import * as Data from '../../../../fixtures/AdminSetup.json'
import * as cppFunctions from '../../../../support/CarParkProductFunctions.js'

const productRoute = '**/api/product'
const productTariffRoute = '**/api/product/tariff'
const carParkRoute = '**/api/carpark'
const airportRoute = '**/api/airport'

const CarParkProduct = Data.Inventory.Inventory.CarParkProduct
const MovementAdjustment = {
    Original: CarParkProduct.Original.MovementAdjustment,
    Modified: CarParkProduct.Modified.MovementAdjustment
}

const Tab = Data.Enum.CarParkProductTab

describe('Car Park Product Movement Adjustments Test', function () {
    beforeEach(function () {
        cy.logInNoPre('inventory')

        cy.route('GET', productRoute).as('GetData')
        cy.route('GET', productTariffRoute).as('GetProductTariff')
        cy.route('GET', carParkRoute).as('GetCarPark')
        cy.route('GET', airportRoute).as('GetAirport')

        cy.get('[data-cy=sidebar-item-inventory]').click()
        cy.get('[data-cy=sidebar-item-inventory-car-park-products]').click()

        cy.wait('@GetData')
        cy.wait('@GetProductTariff')
        cy.wait('@GetCarPark')
        cy.wait('@GetAirport')

        cppFunctions.VisitTab(CarParkProduct.Original, Tab.MOVEMENTADJUSTMENTS)

        cy.wait('@GetMovementAdjustment')
        cy.wait('@GetMovementPricing')
    })

    it('Add Movement Adjustment', function () {
        cppFunctions.AddAdjustment(MovementAdjustment.Original, Tab.MOVEMENTADJUSTMENTS)
    })

    it('Check Movement Adjustment', function () {
        cppFunctions.CheckAdjustment(MovementAdjustment.Original, Tab.MOVEMENTADJUSTMENTS)
    })

    it('Delete Movement Adjustment', function () {
        cppFunctions.DeleteAdjustment(MovementAdjustment.Original, Tab.MOVEMENTADJUSTMENTS)
    })
})