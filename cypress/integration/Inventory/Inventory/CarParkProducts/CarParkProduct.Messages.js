import * as customFunctions from '../../../../support/functions.js'
import * as Data from '../../../../fixtures/AdminSetup.json'
import * as cppFunctions from '../../../../support/CarParkProductFunctions.js'

const productRoute = '**/api/product'
const productTariffRoute = '**/api/product/tariff'
const carParkRoute = '**/api/carpark'
const airportRoute = '**/api/airport'

const CarParkProduct = Data.Inventory.Inventory.CarParkProduct
const Messages = {
    Original: CarParkProduct.Original.Messages,
    Modified: CarParkProduct.Modified.Messages
}

const Tab = Data.Enum.CarParkProductTab

describe('Car Park Product Messages Test', function () {
    beforeEach(function () {
        cy.logInNoPre('inventory')

        cy.route('GET', productRoute).as('GetData')
        cy.route('GET', productTariffRoute).as('GetProductTariff')
        cy.route('GET', carParkRoute).as('GetCarPark')
        cy.route('GET', airportRoute).as('GetAirport')

        cy.get('[data-cy=sidebar-item-inventory]').click()
        cy.get('[data-cy=sidebar-item-inventory-car-park-products]').click()

        cy.wait('@GetData')
        cy.wait('@GetProductTariff')
        cy.wait('@GetCarPark')
        cy.wait('@GetAirport')

        cppFunctions.VisitTab(CarParkProduct.Original, Tab.MESSAGES)
    })

    it('Modify Messages', function () {
        ModifyMessages(Messages.Modified)
    })

    it('Check Modified Messages', function () {
        CheckMessages(Messages.Modified)
    })

    it('Revert Messages', function () {
        ModifyMessages(Messages.Original)
    })

    it('Check Reverted Messages', function () {
        CheckMessages(Messages.Original)
    })
})

function ModifyMessages(testData) {
    cy.route('PUT', productRoute + '/*').as('PutMessages')

    customFunctions.SetCheckbox('[data-cy=car-park-p-message-checkbox-enable-limited-space] > .input__checkbox-input', testData.EnableLimitedSpaces)
    cy.wait(500)
    if (testData.EnableLimitedSpaces) {
        cy.get('[data-cy=car-park-p-message-select-limited-spaces-type]').select(testData.LimitedSpaceTypes)
        cy.get('[data-cy=car-park-p-message-input-limited-spaces] > .row > .field > :nth-child(1) > .input > .form-control').clear().type(testData.LimitedSpaces)
        cy.get('[data-cy=car-park-p-message-input-limited-spaces-message] > .row > .field > :nth-child(1) > .input > .form-control').clear().type(testData.LimitedSpacesMessage)
    }


    customFunctions.SetCheckbox('[data-cy=car-park-p-message-checkbox-bucket-pricing-message] > .input__checkbox-input', testData.EnableBucketPricingMessages)
    cy.wait(500)
    if (testData.EnableBucketPricingMessages) {
        cy.get('[data-cy=car-park-p-message-select-bucket-pricing-type]').select(testData.BucketPricingType)
        cy.get('[data-cy=car-park-p-message-input-bucket-spaces] > .row > .field > :nth-child(1) > .input > .form-control').clear().type(testData.BucketSpaces)
        cy.get('[data-cy=car-park-p-message-input-bucket-space-message] > .row > .field > :nth-child(1) > .input > .form-control').clear().type(testData.BucketSpaceMessage)
    }
    cy.wait(1000)

    customFunctions.SetCheckbox('[data-cy=car-park-p-message-checkbox-enable-member-pricing-message] > .input__checkbox-input', testData.EnableMemberPricingMessages)

    cy.wait(500)
    if (testData.EnableMemberPricingMessages) {
        cy.get('[data-cy=car-park-p-message-input-member-tariff-text] > .editr--content').clear().type(testData.MemberTariffText)
        cy.get('[data-cy=car-park-p-message-input-member-text] > .editr--content').clear().type(testData.MemberText)
    }

    cy.get('[data-cy=car-park-p-message-save-btn]').click()

    cy.wait('@PutMessages').then((response) => {
        cy.wrap(response.status).should('eq', 200)
    })
}

function CheckMessages(testData) {
    customFunctions.SetCheckbox('[data-cy=car-park-p-message-checkbox-enable-limited-space] > .input__checkbox-input', testData.EnableLimitedSpaces)
    cy.wait(500)
    if (testData.EnableLimitedSpaces) {
        cy.get('[data-cy=car-park-p-message-select-limited-spaces-type]').then((content) => {
            cy.wrap(content[0].selectedOptions[0].label).should('eq', testData.LimitedSpaceTypes)
        })
        cy.get('[data-cy=car-park-p-message-input-limited-spaces] > .row > .field > :nth-child(1) > .input > .form-control').then((content) => {
            cy.wrap(content[0].value).should('eq', testData.LimitedSpaces.toString())
        })
        cy.get('[data-cy=car-park-p-message-input-limited-spaces-message] > .row > .field > :nth-child(1) > .input > .form-control').then((content) => {
            cy.wrap(content[0].value).should('eq', testData.LimitedSpacesMessage)
        })
    }

    customFunctions.SetCheckbox('[data-cy=car-park-p-message-checkbox-bucket-pricing-message] > .input__checkbox-input', testData.EnableBucketPricingMessages)
    cy.wait(500)
    if (testData.EnableBucketPricingMessages) {
        cy.get('[data-cy=car-park-p-message-select-bucket-pricing-type]').then((content) => {
            cy.wrap(content[0].selectedOptions[0].label).should('eq', testData.BucketPricingType)
        })
        cy.get('[data-cy=car-park-p-message-input-bucket-spaces] > .row > .field > :nth-child(1) > .input > .form-control').then((content) => {
            cy.wrap(content[0].value).should('eq', testData.BucketSpaces.toString())
        })
        cy.get('[data-cy=car-park-p-message-input-bucket-space-message] > .row > .field > :nth-child(1) > .input > .form-control').then((content) => {
            cy.wrap(content[0].value).should('eq', testData.BucketSpaceMessage)
        })
    }
    cy.wait(1000)

    customFunctions.SetCheckbox('[data-cy=car-park-p-message-checkbox-enable-member-pricing-message] > .input__checkbox-input', testData.EnableMemberPricingMessages)

    cy.wait(500)
    if (testData.EnableMemberPricingMessages) {
        cy.get('[data-cy=car-park-p-message-input-member-tariff-text] > .editr--content').then((content) => {
            cy.wrap(content[0].innerText).should('eq', testData.MemberTariffText)
        })
        cy.get('[data-cy=car-park-p-message-input-member-text] > .editr--content').then((content) => {
            cy.wrap(content[0].innerText).should('eq', testData.MemberText)
        })
    }
}