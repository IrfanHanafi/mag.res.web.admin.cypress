import * as customFunctions from '../../../../support/functions.js'
import * as Data from '../../../../fixtures/AdminSetup.json'
import * as cppFunctions from '../../../../support/CarParkProductFunctions.js'

const productRoute = '**/api/product'
const productTariffRoute = '**/api/product/tariff'
const carParkRoute = '**/api/carpark'
const airportRoute = '**/api/airport'

const CarParkProduct = Data.Inventory.Inventory.CarParkProduct
const BookingFields = {
    Original: CarParkProduct.Original.Fields.BookingFields,
    Modified: CarParkProduct.Modified.Fields.BookingFields
}

const CustomFields = {
    Original: CarParkProduct.Original.Fields.CustomFields,
    Modified: CarParkProduct.Modified.Fields.CustomFields
}

const Tab = Data.Enum.CarParkProductTab

describe('Car Park Product Fields Test', function () {
    beforeEach(function () {
        cy.logInNoPre('inventory')

        cy.route('GET', productRoute).as('GetData')
        cy.route('GET', productTariffRoute).as('GetProductTariff')
        cy.route('GET', carParkRoute).as('GetCarPark')
        cy.route('GET', airportRoute).as('GetAirport')

        cy.get('[data-cy=sidebar-item-inventory]').click()
        cy.get('[data-cy=sidebar-item-inventory-car-park-products]').click()

        cy.wait('@GetData')
        cy.wait('@GetProductTariff')
        cy.wait('@GetCarPark')
        cy.wait('@GetAirport')

        cppFunctions.VisitTab(CarParkProduct.Original, Tab.BOOKINGFIELDS)

        cy.wait('@GetBookingField')
        cy.wait('@GetCustomField')
        cy.wait('@GetFormFieldsRef')
        cy.wait('@GetCustomFieldsRef')
        cy.wait('@GetCustomWhiteLabelRef')

        customFunctions.SetCheckbox('[data-cy=car-park-product-booking-fields-checkbox-enable-booking-field] > .input__checkbox-input', true)

        cy.wait(500)
    })

    describe('Booking Fields Test', function () {
        it('Add Booking Fields', function () {
            AddFields(BookingFields.Original)
        })

        it('Check Booking Fields', function () {
            CheckFields(BookingFields.Original)
        })

        it('Modify Booking Fields', function () {
            ModifyFields(BookingFields.Modified)
        })

        it('Check Booking Fields', function () {
            CheckFields(BookingFields.Modified)
        })

        it('Delete Booking Fields', function () {
            DeleteFields(BookingFields.Modified)
        })
    })

    describe('Custom Fields Test', function () {
        it('Add Custom Fields', function () {
            AddFields(CustomFields.Original)
        })

        it('Check Custom Fields', function () {
            CheckFields(CustomFields.Original)
        })

        it('Modified Custom Fields', function () {
            ModifyFields(CustomFields.Modified)
        })

        it('Check Custom Fields', function () {
            CheckFields(CustomFields.Modified)
        })

        it('Delete Custom Fields', function () {
            DeleteFields(CustomFields.Modified)
        })
    })

})

function AddFields(testData) {

    var fieldType = ""
    var fieldURL = ""
    if (testData.Type == "Booking") {
        fieldType = "booking"
        fieldURL = "BookingField"
    }
    else if (testData.Type == "Custom") {
        fieldType = "custom"
        fieldURL = "customfield"

        cy.get('[data-cy=car-park-product-booking-fields-select-white-label]').select(testData.WhiteLabel)

        customFunctions.SetCheckbox('[data-cy=car-park-product-booking-fields-checkbox-custom-field-visible] > .input__checkbox-input', testData.Visible)
        customFunctions.SetCheckbox('[data-cy=car-park-product-booking-fields-checkbox-custom-field-mandatory] > .input__checkbox-input', testData.Mandatory)
    }

    cy.route('PUT', productRoute + '/*/' + fieldURL).as('PutField')

    cy.get('[data-cy=car-park-product-booking-fields-select-' + fieldType + '-field]').select(testData.Field)

    cy.get('[data-cy=car-park-product-booking-fields-button-add-' + fieldType + '-field]').click()

    cy.wait('@PutField').then((response) => {
        cy.wrap(response.status).should('eq', 200)
    })
}

function ModifyFields(testData) {

    var fieldType = ""
    var fieldURL = ""
    var n = 0
    if (testData.Type == "Booking") {
        fieldType = "booking"
        fieldURL = "BookingField"
    }
    else if (testData.Type == "Custom") {
        fieldType = "custom"
        fieldURL = "customfield"
        n = 2
    }

    cy.route('PUT', productRoute + '/*/' + fieldURL).as('PutField')

    cy.get('[data-cy=car-park-product-booking-fields-table-' + fieldType + '-field-container] > .table-component__table-wrapper > .table-component__table > .table-component__table__body > tr').each((tr) => {
        if (tr[0].cells[0].children[0].value == testData.Field) {

            if (fieldType == "custom") {
                customFunctions.SetCheckbox('[data-cy=' + tr[0].cells[2].children[0].dataset.cy + '] > .input__checkbox-input', testData.Visible)
                cy.wait('@PutField').then((response) => {
                    cy.wrap(response.status).should('eq', 200)
                })
                cy.wait('@GetBookingField')
                cy.wait('@GetCustomField')
            }

            cy.wait(1000)
            customFunctions.SetCheckbox('[data-cy=' + tr[0].cells[1 + n].children[0].dataset.cy + '] > .input__checkbox-input', testData.Mandatory)
            cy.wait('@PutField').then((response) => {
                cy.wrap(response.status).should('eq', 200)
            })
            cy.wait('@GetBookingField')
            cy.wait('@GetCustomField')
            cy.wait(1000)


        }
    })
}

function CheckFields(testData) {

    var modified = false
    var fieldType = ""
    var n = 0
    if (testData.Type == "Booking") {
        fieldType = "booking"
    }
    else if (testData.Type == "Custom") {
        fieldType = "custom"
        n = 2
    }

    cy.log('Checking ' + fieldType + ' field')

    cy.get('[data-cy=car-park-product-booking-fields-table-' + fieldType + '-field-container] > .table-component__table-wrapper > .table-component__table > .table-component__table__body > tr').each((tr) => {
        if (tr[0].cells[0].children[0].value == testData.Field) {
            cy.wrap(tr[0].cells[0].children[0].value).should('eq', testData.Field)
            cy.wrap(tr[0].cells[(1 + n)].children[0].children[1].control.checked).should('eq', testData.Mandatory)

            if (fieldType == "custom") {
                cy.wrap(tr[0].cells[1].children[0].value).should('eq', testData.WhiteLabel)
                cy.wrap(tr[0].cells[2].children[0].children[1].control.checked).should('eq', testData.Visible)
            }

            modified = true
        }
    }).then(() => {
        cy.wrap(modified).should('eq', true)
    })
}

function DeleteFields(testData) {

    cy.wait(500)

    var modified = false
    var fieldType = ""
    var fieldURL = ""
    var n = 0
    if (testData.Type == "Booking") {
        fieldType = "booking"
        fieldURL = "BookingField"
    }
    else if (testData.Type == "Custom") {
        fieldType = "custom"
        fieldURL = "customfield"
        n = 2
    }
    cy.log('Deleting ' + fieldType + ' field')
    cy.route('DELETE', productRoute + '/*/' + fieldURL + '/*').as('DeleteField')

    cy.get('[data-cy=car-park-product-booking-fields-table-' + fieldType + '-field-container] > .table-component__table-wrapper > .table-component__table > .table-component__table__body > tr').each((tr) => {
        if (tr[0].cells[0].children[0].value == testData.Field) {
            cy.wait(500) //wait for toast
            cy.wrap(tr[0].cells[2 + n]).contains('Delete').click({ force: true })

            cy.wait('@DeleteField').then((response) => {
                cy.wrap(response.status).should('eq', 200)
                modified = true
            })
        }
    }).then(() => {
        cy.wrap(modified).should('eq', true)
    })
}