import * as customFunctions from '../../../../support/functions.js'
import * as Data from '../../../../fixtures/AdminSetup.json'
import * as cppFunctions from '../../../../support/CarParkProductFunctions.js'

const productRoute = '**/api/product'
const productTariffRoute = '**/api/product/tariff'
const carParkRoute = '**/api/carpark'
const airportRoute = '**/api/airport'

const CarParkProduct = Data.Inventory.Inventory.CarParkProduct
const Channel = {
    Original: CarParkProduct.Original.Channel,
    Modified: CarParkProduct.Modified.Channel
}

const Tab = Data.Enum.CarParkProductTab

describe('Car Park Product Channel Test', function () {
    beforeEach(function () {
        cy.logInNoPre('inventory')

        cy.route('GET', productRoute).as('GetData')
        cy.route('GET', productTariffRoute).as('GetProductTariff')
        cy.route('GET', carParkRoute).as('GetCarPark')
        cy.route('GET', airportRoute).as('GetAirport')

        cy.get('[data-cy=sidebar-item-inventory]').click()
        cy.get('[data-cy=sidebar-item-inventory-car-park-products]').click()

        cy.wait('@GetData')
        cy.wait('@GetProductTariff')
        cy.wait('@GetCarPark')
        cy.wait('@GetAirport')

        cppFunctions.VisitTab(CarParkProduct.Original, Tab.CHANNELS)

        cy.route('GET', productRoute + '/channel/*').as('GetChannel')
        cy.route('GET', productRoute + '/channel/*/bookingDates').as('GetBookingDates')
        cy.route('GET', carParkRoute + '/*/capacitypool').as('GetCapacityPool')
        cy.route('GET', airportRoute + '/*/channel').as('GetAirportChannel')
    })

    it('Add Channel', function () {
        AddChannel(Channel.Original)
    })

    it('Check Channel', function () {
        CheckChannel(Channel.Original)
    })

    it('Add Channel Booking Dates', function () {
        AddChannelBookingDates(Channel.Original, Channel.Original)
    })

    it('Check Channel Booking Dates', function () {
        CheckChannelBookingDates(Channel.Original, Channel.Original)
    })

    it('Modify Channel Booking Dates', function () {
        ModifyChannelBookingDates(Channel.Modified, Channel.Original)
    })

    it('Check Modified Channel Booking Dates', function () {
        CheckChannelBookingDates(Channel.Modified, Channel.Original)
    })

    it('Modified Channel', function () {
        ModifyChannel(Channel.Modified, Channel.Original)
    })

    it('Check Channel', function () {
        CheckChannel(Channel.Modified)
    })

    it('Delete Channel', function () {
        DeleteChannel(Channel.Modified)
    })
})

function AddChannel(testData) {
    cy.route('POST', productRoute + '/channel').as('SendChannel')

    cy.get('[data-cy=car-park-p-channel-create-btn]').click()

    customFunctions.SelectCheckbox('[data-cy=car-park-product-channels-multi-check]', testData.Channel)

    FillChannel(testData)
}

function ModifyChannel(testData, tabData) {
    var modified = false
    cy.route('PUT', productRoute + '/channel/*').as('SendChannel')

    cy.get('[data-cy=car-park-product-channels-table-container] > .table-component__table-wrapper > .table-component__table > .table-component__table__body > tr').each((tr) => {

        if (tr[0].cells[0].innerText == testData.Channel) {

            cy.wrap(tr[0].cells[9]).contains('Change').click()

            cy.wait('@GetBookingDates')
            cy.wait('@GetLadder')
            cy.wait('@GetCapacityPool')
            cy.wait('@GetAirportChannel')
            FillChannel(testData)
            modified = true
        }
    }).then(() => {
        cy.wrap(modified).should('eq', true)
    })
}

function AddChannelBookingDates(testData, search) {

    var modified = false

    cy.route('POST', productRoute + '/channel/*/bookingDates').as('PostBookingDates')

    cy.get('[data-cy=car-park-product-channels-table-container] > .table-component__table-wrapper > .table-component__table > .table-component__table__body > tr').each((tr) => {

        if (tr[0].cells[0].innerText == search.Channel) {

            cy.wrap(tr[0].cells[9]).contains('Change').click()

            cy.log('Add Booking Dates')
            cy.get('[data-cy=car-park-product-channel-button-add-booking-dates]').click()

            FillChannelBookingDates(testData)

            cy.wait('@PostBookingDates').then((response) => {
                cy.wrap(response.status).should('eq', 200)
                modified = true
            })
        }
    }).then(() => {
        cy.wrap(modified).should('eq', true)
    })
}

function ModifyChannelBookingDates(testData, search) {

    var modified = false

    cy.route('PUT', productRoute + '/channel/*/bookingDates/*').as('PutBookingDates')

    cy.get('[data-cy=car-park-product-channels-table-container] > .table-component__table-wrapper > .table-component__table > .table-component__table__body > tr').each((tr) => {

        if (tr[0].cells[0].innerText == search.Channel) {
            cy.wrap(tr[0].cells[9]).contains('Change').click()

            cy.wait('@GetBookingDates')
            cy.wait('@GetLadder')
            cy.wait('@GetCapacityPool')
            cy.wait('@GetAirportChannel')

            cy.get('.table-component__table__body > tr').then((tr) => {
                cy.wrap(tr[0].cells[2]).contains('Change').click()

                FillChannelBookingDates(testData)

                cy.wait('@PutBookingDates').then((response) => {
                    cy.wrap(response.status).should('eq', 200)
                    modified = true
                })
            })
        }
    }).then(() => {
        cy.wrap(modified).should('eq', true)
    })
}

function FillChannelBookingDates(testData) {
    cy.wait(500)

    var StartDate = customFunctions.GetDate(testData.StartDate)
    var EndDate = customFunctions.GetDate(testData.EndDate)

    cy.get('#startDate').invoke('removeAttr', 'readonly')
    cy.get('#startDate').clear().type(StartDate.format('MMMM DD, YYYY'))

    cy.get('#channel-availability-modal___BV_modal_title_').click()

    cy.get('#endDate').invoke('removeAttr', 'readonly')
    cy.get('#endDate').clear().type(EndDate.format('MMMM DD, YYYY'))

    cy.get('#channel-availability-modal___BV_modal_title_').click()

    cy.get('#channel-availability-modal___BV_modal_footer_ > .btn').click()
}

function CheckChannelBookingDates(testData, search) {

    var modified = false

    var StartDate = customFunctions.GetDate(testData.StartDate).format('DD MMM YYYY')
    var EndDate = customFunctions.GetDate(testData.EndDate).format('DD MMM YYYY')

    cy.get('[data-cy=car-park-product-channels-table-container] > .table-component__table-wrapper > .table-component__table > .table-component__table__body > tr').each((tr) => {

        if (tr[0].cells[0].innerText == search.Channel) {
            cy.wrap(tr[0].cells[9]).contains('Change').click()

            cy.wait('@GetBookingDates')
            cy.wait('@GetLadder')
            cy.wait('@GetCapacityPool')
            cy.wait('@GetAirportChannel')

            cy.get('.table-component__table__body > tr').then((tr) => {
                cy.wrap(tr[0].cells[0].innerText).should('eq', StartDate)
                cy.wrap(tr[0].cells[1].innerText).should('eq', EndDate)

                modified = true
            })
        }
    }).then(() => {
        cy.wrap(modified).should('eq', true)
    })
}

function CheckChannel(testData) {
    var modified = false
    cy.get('[data-cy=car-park-product-channels-table-container] > .table-component__table-wrapper > .table-component__table > .table-component__table__body > tr').each((tr) => {

        if (tr[0].cells[0].innerText == testData.Channel) {
            cy.wrap(tr[0].cells[1].innerText).should('eq', testData.Tariff)
            cy.wrap(tr[0].cells[2].innerText).should('eq', testData.DefaultPriceLadder)
            cy.wrap(tr[0].cells[3].innerText).should('eq', testData.MemberTariff)
            cy.wrap(tr[0].cells[4].innerText).should('eq', testData.MemberTariffPriceLadder)
            cy.wrap(tr[0].cells[5].innerText).should('eq', testData.POFTariff)
            cy.wrap(tr[0].cells[6].innerText).should('eq', testData.POFPriceLadder)
            cy.wrap(tr[0].cells[7].innerText).should('eq', testData.CarParkCapacityPool)
            cy.wrap(tr[0].cells[8].innerText).should('eq', testData.ProductCapacityPool)
            modified = true
        }
    }).then(() => {
        cy.wrap(modified).should('eq', true)
    })
}

function FillChannel(testData) {
    cy.wait('@GetTariff')
    cy.wait('@GetCapacityPool')
    cy.wait('@GetChannel')
    cy.wait('@GetCarParkProduct')
    cy.get('[data-cy=car-park-product-channels-select-tariff]').select(testData.Tariff)
    cy.wait('@GetLadder')
    cy.get('[data-cy=car-park-product-channels-select-default-price-ladder]').select(testData.DefaultPriceLadder)

    cy.get('[data-cy=car-park-product-channels-select-member-tariff]').select(testData.MemberTariff)
    cy.wait('@GetLadder')
    cy.get('[data-cy=car-park-product-channels-select-member-tariff-price-ladder]').select(testData.MemberTariffPriceLadder)

    cy.get('[data-cy=car-park-product-channels-select-pof-tariff]').select(testData.POFTariff)
    cy.wait('@GetLadder')
    cy.get('[data-cy=car-park-product-channels-select-pof-price-ladder]').select(testData.POFPriceLadder)

    cy.get('[data-cy=car-park-product-channels-select-car-park-capacity-pool]').select(testData.CarParkCapacityPool)
    cy.get('[data-cy=car-park-product-channels-select-product-capacity-pool]').select(testData.ProductCapacityPool)

    cy.wait(500) //wait for toast to disappear
    cy.get('[data-cy=car-park-product-channel-button-save]').click()

    cy.wait('@SendChannel').then((response) => {
        cy.wrap(response.status).should('eq', 200)
    })
}

function DeleteChannel(testData) {

    var modified = false
    cy.route('Delete', productRoute + '/channel/*').as('DeleteChannel')

    cy.get('[data-cy=car-park-product-channels-table-container] > .table-component__table-wrapper > .table-component__table > .table-component__table__body > tr').each((tr) => {

        if (tr[0].cells[0].innerText == testData.Channel) {

            cy.wrap(tr[0].cells[9]).contains('Unlink').click()

            cy.get('.modal-footer > .btn').click()

            cy.wait('@DeleteChannel').then((response) => {
                cy.wrap(response.status).should('eq', 200)
                modified = true
            })
        }
    }).then(() => {
        cy.wrap(modified).should('eq', true)
    })
}