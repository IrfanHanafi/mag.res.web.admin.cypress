import moment from 'moment'
import * as customFunctions from '../../../support/functions.js'
import * as Data from '../../../fixtures/AdminSetup.json'

describe('Occupancy Pricing Tests', function () {

    const occupancyPricingRoute = '/inventory/api/occupancypricing/'
    const tariffRoute = '/inventory/api/tariff/'

    const AdjustmentTypes = Data.Enum.AdjustmentTypes

    const OccupancyPricing = Data.Inventory.Pricing.OccupancyPricing

    var fromDate = {
        Original: customFunctions.GetDate(OccupancyPricing.Original.ArrivalDateStart),
        Modified: customFunctions.GetDate(OccupancyPricing.Modified.ArrivalDateStart)
    }

    var toDate = {
        Original: customFunctions.GetDate(OccupancyPricing.Original.ArrivalDateEnd),
        Modified: customFunctions.GetDate(OccupancyPricing.Modified.ArrivalDateEnd)
    }

    beforeEach(function () {
        cy.logInNoPre("inventory")

        cy.route('GET', occupancyPricingRoute).as('GetData')

        cy.get('[data-cy=sidebar-item-pricing]').click()
        cy.get('[data-cy=sidebar-item-pricing-occupancy-pricing]').click()

        cy.wait('@GetData')
    })

    it('Create Occupancy Price', function () {

        cy.route('POST', occupancyPricingRoute).as('PostData')

        cy.get('[data-cy=create-occupancy-pricing-btn]').click()
        cy.get('#RuleName').type(OccupancyPricing.Original.RuleName)

        customFunctions.SetDate('#arrivalDateStart', fromDate.Original)

        customFunctions.SetDate('#arrivalDateEnd', toDate.Original)

        cy.get('#AdjustmentType').select(OccupancyPricing.Original.AdjustmentType)

        cy.get('#ManageType').select(OccupancyPricing.Original.ManageType)

        //Turn Off Notification?
        cy.wait(1000)

        cy.get('[data-cy=create-pricing-rule-btn]').click()

        cy.wait('@PostData').then((response) => {
            expect(response.status).to.eq(200)
        })

    })

    it('Filters Table', function () {
        cy.get('.table-component__filter__field').type(OccupancyPricing.Original.RuleName)

        cy.get('.table-component__table__body')
            .find('tr').each((tr) => {
                cy.wrap(tr[0].cells[0].innerText).should('contain', OccupancyPricing.Original.RuleName)
            })
    })

    it('Checks Table for Created Occupancy Price', function () {
        CheckTable(OccupancyPricing.Original, fromDate.Original, toDate.Original)
    })


    it('Modify Occupancy Price', function () {
        var modified = false
        cy.get('.table-component__table__body')
            .find('tr').each((tr) => {
                if (tr[0].cells[0].innerText == OccupancyPricing.Original.RuleName) {
                    cy.wrap(tr[0].cells[6]).contains('Edit').click()


                    cy.route('PUT', occupancyPricingRoute + '/*').as('PutData')


                    cy.get('#RuleName').clear().type(OccupancyPricing.Modified.RuleName)

                    customFunctions.SetDate('#arrivalDateStart', fromDate.Modified)

                    customFunctions.SetDate('#arrivalDateEnd', toDate.Modified)

                    cy.get('#AdjustmentType').select(OccupancyPricing.Modified.AdjustmentType)

                    cy.get('#ManageType').select(OccupancyPricing.Modified.ManageType)

                    customFunctions.SetCheckbox('.input__checkbox-input', OccupancyPricing.Modified.Active)

                    var RowLoop = Array.from({ length: OccupancyPricing.Modified.Table.From.length - 1 }, (v, k) => k)
                    cy.wrap(RowLoop).each((i) => {
                        cy.get('[data-cy=add-pricing-rule-adjustment-btn]').click()
                    })

                    cy.get('tbody').then((table) => {
                        cy.wrap(table[0].childNodes).each((tr, i) => {
                            switch (OccupancyPricing.Modified.AdjustmentType) {
                                case AdjustmentTypes.TARIFF: cy.wrap(tr[0].cells[0].childNodes[0]).type(OccupancyPricing.Modified.Table.From[i])

                                    cy.route('GET', tariffRoute + '/*').as('GetTariff')

                                    cy.wrap(tr[0].cells[1].childNodes[0].childNodes[0]).select(OccupancyPricing.Modified.Table.Adjustment[i])

                                    cy.wait('@GetTariff')
                                    break

                                default: cy.wrap(tr[0].cells[0].childNodes[0]).type(OccupancyPricing.Modified.Table.From[i])
                                    cy.wrap(tr[0].cells[1].childNodes[0]).type(OccupancyPricing.Modified.Table.Adjustment[i])
                                    break
                            }
                        })

                        if (OccupancyPricing.Modified.AdjustmentType == AdjustmentTypes.TARIFF) {
                            cy.get('#StayDuration').select(OccupancyPricing.Modified.StayDuration)

                            cy.wrap(table[0].childNodes).each((tr, i) => {
                                cy.wrap(tr[0].cells[2].childNodes[0].childNodes[0]).select(OccupancyPricing.Modified.Table.Ladder[i])
                            })
                        }

                    })

                    cy.get('.text-center > .btn')

                    cy.get('[data-cy=add-pricing-rule-adjustment-btn]')

                    cy.get('[data-cy=save-pricing-rule-btn]').click()

                    cy.wait('@PutData').then((response) => {
                        cy.wrap(response.status).should('eq', 200)
                        modified = true
                    })
                }
            }).then(() => {
                cy.wrap(modified).should('eq', true)
            })
    })

    it('Checks Table for Modified Occupancy Price', function () {
        CheckTable(OccupancyPricing.Modified, fromDate.Modified, toDate.Modified)
    })


    it('Delete Created Occupancy Price', function () {
        var deleted = false

        cy.get('.table-component__table__body')
            .find('tr').each((tr, i) => {
                if (tr[0].cells[0].innerText == OccupancyPricing.Modified.RuleName) {

                    cy.route('DELETE', occupancyPricingRoute + '/*').as('DeleteData')

                    cy.wrap(tr[0].cells[6]).contains('Delete').click()
                    cy.get('.modal-footer > .btn').click()

                    cy.wait('@DeleteData').then((response) => {
                        cy.wrap(response.status).should('eq', 200)
                        deleted = true
                    })
                }
            }).then(() => {
                cy.wrap(deleted).should('eq', true)
            })

        cy.wait('@GetData')

        cy.get('.table-component__table__body')
            .find('tr').each((tr, i) => {
                expect(tr[0].cells[0].innerText).to.not.equal(OccupancyPricing.Original.RuleName)
                expect(tr[0].cells[0].innerText).to.not.equal(OccupancyPricing.Modified.RuleName)
            })
    })
})

function CheckTable(testData, DateFrom, DateTo) {
    var found = false
    cy.get('.table-component__table__body')
        .find('tr').each((tr) => {
            if (tr[0].cells[0].innerText == testData.RuleName) {
                cy.wrap(tr[0].cells[0].innerText).should('eq', testData.RuleName)
                cy.wrap(tr[0].cells[1].innerText).should('eq', moment(DateFrom).format('DD MMM YYYY'))
                cy.wrap(tr[0].cells[2].innerText).should('eq', moment(DateTo).format('DD MMM YYYY'))
                cy.wrap(tr[0].cells[3].innerText).should('eq', testData.AdjustmentType)
                cy.wrap(tr[0].cells[4].innerText).should('eq', testData.Active.toString())
                found = true
            }
        }).then(() => {
            cy.wrap(found).should('eq', true)
        })
}