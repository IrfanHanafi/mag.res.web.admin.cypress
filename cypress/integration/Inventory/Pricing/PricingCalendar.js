import moment from 'moment'
import * as customFunctions from '../../../support/functions.js'
import * as Data from '../../../fixtures/AdminSetup.json'

describe('Pricing Calendar Test', function () {

    const PricingCalendar = Data.Inventory.Pricing.PricingCalendar

    if (PricingCalendar.DateFrom.Date) {
        var fromDate = customFunctions.ParseDate(PricingCalendar.DateFrom.Date)
    }
    else {
        var fromDate = customFunctions.ParseDate(null, PricingCalendar.DateFrom.Amount, PricingCalendar.DateFrom.Period)
    }

    if (PricingCalendar.DateTo.Date) {
        var toDate = customFunctions.ParseDate(PricingCalendar.DateTo.Date)
    }
    else {
        var toDate = customFunctions.ParseDate(null, PricingCalendar.DateTo.Amount, PricingCalendar.DateTo.Period)
    }

    var ladderRange = {
        From: customFunctions.ParseDate(PricingCalendar.Ladder.DateFrom),
        To: customFunctions.ParseDate(PricingCalendar.Ladder.DateTo)
    }

    beforeEach(() => {
        cy.logInNoPre("inventory")

        cy.get('[data-cy=sidebar-item-pricing]').click()
        cy.get('[data-cy=sidebar-item-pricing-pricing-calendar]').click()
    })

    it('Price Date Search', function () {
        cy.get('#selectedTarriff').select(PricingCalendar.Tariff)

        cy.route('GET', '/inventory/api/tariff/*').as('GetTariff')
        cy.route('GET', '/inventory/api/tariff/*/pricetype?*').as('GetPriceType')
        cy.route('GET', '/inventory/api/tariff/*/pricing/calendar?*').as('GetPricingCalendar')


        cy.get('#searchDateFrom').invoke('removeAttr', 'readonly').clear().type(moment(fromDate).format('MMMM DD, YYYY'))
        cy.get('.page-heading__title').click()

        cy.get('#searchDateTo').invoke('removeAttr', 'readonly').clear().type(moment(toDate).format('MMMM DD, YYYY'))
        cy.get('.page-heading__title').click()

        cy.get('[data-cy=pricing-calendar-search-button]').click()

        cy.wait('@GetTariff')
        cy.wait('@GetPriceType')
        cy.wait('@GetPricingCalendar')
        cy.wait('@GetFeatures')

        cy.get('.card-body').get('.table-calendar').each((calendar) => {
            var currentMonth = ""
            cy.wrap(calendar).siblings('.table-header').then((header) => {

                cy.log(header[0].innerText)
                currentMonth = moment(header[0].innerText, 'MMMM YYYY')

                var firstDay = calendar[0].childNodes[0].childNodes[2].childNodes[0].cells[0].textContent
                var tableCalendar = moment(firstDay + " " + moment(currentMonth).format('MMMM'), 'Do MMMM YYYY')

                if (firstDay != "1st") {
                    tableCalendar = moment(tableCalendar).subtract(1, 'month')
                }
                cy.wrap(calendar[0].childNodes[0].childNodes[2].childNodes).each((row) => {
                    cy.wrap(row[0].cells).each((cells) => {
                        if (moment(tableCalendar).isSame(currentMonth, 'month')) {
                            if (moment(tableCalendar).isAfter(moment(fromDate).subtract(1, 'day')) && moment(tableCalendar).isBefore(toDate)) {
                                if (moment(tableCalendar).isAfter(moment(ladderRange.From).subtract(1, 'day')) && moment(tableCalendar).isBefore(moment(ladderRange.To).add(1, 'day'))) {
                                    cy.wrap(cells[0].textContent).should('eq', tableCalendar.format('Do') + ' ' + PricingCalendar.Ladder.Name)
                                }
                                else {
                                    cy.wrap(cells[0].textContent).should('eq', tableCalendar.format('Do') + ' ')
                                }
                            }
                        }
                        tableCalendar = moment(tableCalendar).add(1, 'day')
                    })
                })
            })
        })
    })
})