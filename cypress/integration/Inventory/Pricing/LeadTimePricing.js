import * as customFunctions from '../../../support/functions.js'
import * as Data from '../../../fixtures/AdminSetup.json'

describe('Lead Time Pricing Tests', function () {

    const leadTimeRoute = '/inventory/api/leadtime'

    const LeadTimePricing = Data.Inventory.Pricing.LeadTimePricing

    beforeEach(function () {
        cy.logInNoPre("inventory")

        cy.route('GET', leadTimeRoute).as('GetData')

        cy.get('[data-cy=sidebar-item-pricing]').click()
        cy.get('[data-cy=sidebar-item-pricing-lead-time-pricing]').click()

        cy.wait('@GetData')
    })

    it('Create Lead Time Price', function () {
        cy.route('POST', leadTimeRoute).as('PostData')

        cy.get('[data-cy=lead-time-pricing-create-button]').click()
        cy.get('#RuleName').type(LeadTimePricing.Original.RuleName)

        cy.get('[data-cy=lead-time-pricing-create-button]').click()

        cy.wait('@PostData').then((response) => {
            expect(response.status).to.eq(200)
        })
    })

    it('Filters Table', function () {

        cy.get('#LeadTimeName').type(LeadTimePricing.Original.RuleName)

        var found = false
        cy.get('.table-component__table__body')
            .find('tr').each((tr) => {
                if (tr[0].cells[0].innerText == LeadTimePricing.Original.RuleName) {
                    cy.wrap(tr[0].cells[0].innerText).should('eq', LeadTimePricing.Original.RuleName)
                    found = true
                }
                else {
                    cy.wrap(tr[0].cells[0].innerText).should('contain', LeadTimePricing.Original.RuleName)
                }
            }).then(() => {
                cy.wrap(found).should('eq', true)
            })
    })

    it('Checks Table for Created Lead Time Price', function () {
        CheckTable(LeadTimePricing.Original)
    })

    it('Modify Lead Time Price', function () {

        var modified = false

        cy.get('.table-component__table__body')
            .find('tr').each((tr) => {
                if (tr[0].cells[0].innerText == LeadTimePricing.Original.RuleName) {
                    cy.wrap(tr[0].cells[4]).contains('Edit').click()


                    cy.route('PUT', leadTimeRoute + '/*').as('PutData')

                    cy.wait(2000) //fix issue where type is not being executed properly
                    cy.get('#RuleName').clear().type(LeadTimePricing.Modified.RuleName)

                    cy.get('#AdjustmentType').select(LeadTimePricing.Modified.Type)

                    customFunctions.SetCheckbox('.input__checkbox-input', LeadTimePricing.Modified.Active)

                    var RowLoop = Array.from({ length: LeadTimePricing.Modified.Table.From.length - 1 }, (v, k) => k)
                    cy.wrap(RowLoop).each((i) => {
                        cy.get('[data-cy=lead-time-pricing-form-add-btn]').click()
                    })

                    cy.get('tbody').then((table) => {
                        cy.wrap(table[0].childNodes).each((tr, i) => {
                            cy.wrap(tr[0].cells[0].childNodes[0]).type(LeadTimePricing.Modified.Table.From[i])
                            cy.wrap(tr[0].cells[1].childNodes[0]).type(LeadTimePricing.Modified.Table.Adjustment[i])
                        })
                    })

                    cy.get('[data-cy=lead-time-pricing-update-button]').click()

                    modified = true
                }
            }).then(() => {
                cy.wrap(modified).should('eq', true)
            })
    })

    it('Checks Table for Modified Lead Time Price', function () {
        CheckTable(LeadTimePricing.Modified)
    })

    it('Delete Created Lead Time Price', function () {

        var deleted = false

        cy.get('.table-component__table__body')
            .find('tr').each((tr) => {
                if (tr[0].cells[0].innerText == LeadTimePricing.Modified.RuleName) {

                    cy.route('DELETE', leadTimeRoute + '/*').as('DeleteData')

                    cy.wrap(tr[0].cells[4]).contains('Delete').click()
                    cy.get('.btn-danger').click()

                    cy.wait('@DeleteData').then((response) => {
                        cy.wrap(response.status).should('eq', 200)
                        deleted = true
                    })
                }
            }).then(() => {
                cy.wrap(deleted).should('eq', true)
            })


        cy.wait('@GetData')

        cy.get('.table-component__table__body')
            .find('tr').each((tr, i) => {
                expect(tr[0].cells[0].innerText).to.not.equal(LeadTimePricing.Original.RuleName)
                expect(tr[0].cells[0].innerText).to.not.equal(LeadTimePricing.Modified.RuleName)
            })
    })
})

function CheckTable(testData) {
    var found = false

    cy.get('.table-component__table__body')
        .find('tr').each((tr) => {
            if (tr[0].cells[0].innerText == testData.RuleName) {
                cy.wrap(tr[0].cells[0].innerText).should('eq', testData.RuleName)
                cy.wrap(tr[0].cells[1].innerText).should('eq', testData.Type)
                cy.wrap(tr[0].cells[2].innerText).should('eq', testData.Active.toString())
                found = true
            }
        }).then(() => {
            cy.wrap(found).should('eq', true)
        })
}