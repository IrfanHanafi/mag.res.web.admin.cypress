import * as Data from '../../../fixtures/AdminSetup.json'

describe('Tariff Test', function () {

    const Tariff = Data.Inventory.Pricing.Tariff

    beforeEach(function () {
        cy.logInNoPre("inventory")

        cy.route('GET', '/inventory/api/tariff').as('GetData')

        cy.get('[data-cy=sidebar-item-pricing]').click()
        cy.get('[data-cy=sidebar-item-pricing-tariffs]').click()

        cy.wait('@GetData')
    })

    it('Filter Tariff', function () {
        var found = false
        cy.get('#TariffName').clear().type(Tariff.Original.Name)

        cy.get('tbody')
            .find('tr').each((tr, i) => {
                if (tr[0].cells[1].innerText == Tariff.Original.Name) {
                    expect(tr[0].cells[1].innerText).to.equal(Tariff.Original.Name)
                    expect(tr[0].cells[2].innerText).to.equal(Tariff.Original.Code)
                    found = true
                }
            }).then(() => {
                cy.wrap(found).should('eq', true)
            })
    })

    it('Modify Tariff', function () {
        Modify(Tariff.Modified, Tariff.Original.Name)
    })

    it('Checks Table for Modified Tariff', function () {
        CheckTable(Tariff.Modified)
    })

    it('Revert Modified Tariff', function () {
        Modify(Tariff.Original, Tariff.Modified.Name)
    })

    it('Checks Reverted Table', function () {
        CheckTable(Tariff.Original)
    })
})

function Modify(testData, search) {
    var LadderLoop = {
        Column: Array.from({ length: testData.NumLadders }, (v, k) => k),
        Row: Array.from({ length: (testData.Duration.To - testData.Duration.From) + 2 }, (v, k) => k)
    }

    var reverted = false
    cy.get('tbody')
        .find('tr').each((tr, i) => {
            if (tr[0].cells[1].innerText == search) {
                cy.wrap(tr[0].cells[4]).contains('Change').click()


                cy.route('PUT', '/inventory/api/tariff/*').as('PutTariff')


                cy.route('GET', '/inventory/api/pricetype').as('GetPriceType')

                cy.get('#TariffNameInput').clear().type(testData.Name)
                cy.get('#TariffCodeInput').clear().type(testData.Code)

                cy.get('#TariffLadderNum').clear().type(testData.NumLadders)

                cy.get('.row.mb-3 > .col-sm-12 > .btn').click()

                cy.wait('@GetPriceType')

                cy.get('#TariffLadderNumFrom').clear().type(testData.Duration.From)
                cy.get('#TariffLadderNumTo').clear().type(testData.Duration.To)

                cy.get(':nth-child(4) > .col-sm-12 > .btn').click()

                cy.wait('@GetPriceType').then(() => {
                    cy.get('tbody').then((row) => {
                        cy.wrap(LadderLoop.Row).each((j) => {
                            cy.wrap(LadderLoop.Column).each((i) => {
                                if (i + 1 <= LadderLoop.Column.length) {
                                    debugger
                                    cy.wrap(row[0].childNodes[j + 1].cells[i + 1].childNodes[2]).clear().type(testData.Ladders[j][i])
                                }
                            })
                        })
                    })

                    cy.get('[data-cy=save-tariff-btn]').click()
                    cy.wait('@PutTariff').then((response) => {
                        cy.wrap(response.status).should('eq', 200)
                        reverted = true
                    })
                })
            }
        }).then(() => {
            cy.wrap(reverted).should('eq', true)
        })
}

function CheckTable(testData) {
    var found = false
    cy.get('tbody')
        .find('tr').each((tr, i) => {
            if (tr[0].cells[1].innerText == testData.Name) {
                expect(tr[0].cells[1].innerText).to.equal(testData.Name)
                expect(tr[0].cells[2].innerText).to.equal(testData.Code)
                found = true
            }
        }).then(() => {
            cy.wrap(found).should('eq', true)
        })
}