import moment from 'moment'
import * as customFunctions from '../../../support/functions.js'
import * as Data from '../../../fixtures/AdminSetup.json'

const movementPricingRoute = '/inventory/api/movementpricing'
const tariffRoute = '/inventory/api/tariff/'

const AdjustmentTypes = Data.Enum.AdjustmentTypes

describe('Movement Pricing Tests', function () {

    const MovementPricing = Data.Inventory.Pricing.MovementPricing

    var fromDate = {
        Original: customFunctions.GetDate(MovementPricing.Original.ArrivalDateStart),
        Modified: customFunctions.GetDate(MovementPricing.Modified.ArrivalDateStart)
    }

    var toDate = {
        Original: customFunctions.GetDate(MovementPricing.Original.ArrivalDateEnd),
        Modified: customFunctions.GetDate(MovementPricing.Modified.ArrivalDateEnd)
    }

    beforeEach(function () {
        cy.logInNoPre("inventory")

        cy.route('GET', movementPricingRoute).as('GetData')

        cy.get('[data-cy=sidebar-item-pricing]').click()
        cy.get('[data-cy=sidebar-item-pricing-movement-pricing]').click()

        cy.wait('@GetData')
    })

    it('Create Movement Price', function () {

        cy.route('POST', movementPricingRoute).as('PostData')

        cy.get('[data-cy=create-movement-pricing-btn]').click()

        FillForm(MovementPricing.Original, fromDate.Original, toDate.Original, false)

        cy.get('[data-cy=create-pricing-rule-btn]').click()

        cy.wait('@PostData').then((response) => {
            expect(response.status).to.eq(200)
        })
    })

    it('Checks Table for Created Movement Price', function () {
        CheckTable(MovementPricing.Original, fromDate.Original, toDate.Original)
    })

    it('Filters Table', function () {

        var found = false

        cy.get('.table-component__filter__field').type(MovementPricing.Original.RuleName)

        cy.get('.table-component__table__body')
            .find('tr').each((tr) => {
                if (tr[0].cells[0].innerText == MovementPricing.Original.RuleName) {
                    cy.wrap(tr[0].cells[0].innerText).should('eq', MovementPricing.Original.RuleName)
                    found = true
                }
                else {
                    cy.wrap(tr[0].cells[0].innerText).should('contain', MovementPricing.Original.RuleName)
                }
            }).then(() => {
                cy.wrap(found).should('eq', true)
            })

    })

    it('Modify Movement Price', function () {

        var modified = false
        cy.get('.table-component__table__body')
            .find('tr').each((tr) => {
                if (tr[0].cells[0].innerText == MovementPricing.Original.RuleName) {
                    cy.wrap(tr[0].cells[6]).contains('Edit').click()

                    cy.route('PUT', '/inventory/api/movementpricing/*').as('PutData')

                    FillForm(MovementPricing.Modified, fromDate.Modified, toDate.Modified, true)

                    cy.get('[data-cy=save-pricing-rule-btn]').click()

                    cy.wait('@PutData').then((response) => {
                        cy.wrap(response.status).should('eq', 200)
                        modified = true
                    })

                }
            }).then(() => {
                cy.wrap(modified).should('eq', true)
            })
    })



    it('Checks Table for Modified Movement Price', function () {
        CheckTable(MovementPricing.Modified, fromDate.Modified, toDate.Modified)
    })

    it('Delete Created Movement Price', function () {
        var deleted = false
        cy.get('.table-component__table__body')
            .find('tr').each((tr) => {
                if (tr[0].cells[0].innerText == MovementPricing.Modified.RuleName) {

                    cy.route('DELETE', movementPricingRoute + '/*').as('DeleteData')

                    cy.wrap(tr[0].cells[6]).contains('Delete').click()

                    cy.get('.modal-footer > .btn').click()

                    cy.wait('@DeleteData').then((response) => {
                        cy.wrap(response.status).should('eq', 200)
                        deleted = true
                    })
                    cy.wait('@GetData')
                }
            }).then(() => {
                cy.wrap(deleted).should('eq', true)
            })

        cy.get('.table-component__table__body')
            .find('tr').each((tr) => {
                expect(tr[0].cells[0].innerText).to.not.equal(MovementPricing.Original.RuleName)
                expect(tr[0].cells[0].innerText).to.not.equal(MovementPricing.Modified.RuleName)
            })
    })
})

function FillForm(testData, DateFrom, DateTo, modify = false) {

    cy.get('#RuleName').clear().type(testData.RuleName)

    customFunctions.SetDate('#arrivalDateStart', DateFrom)

    customFunctions.SetDate('#arrivalDateEnd', DateTo)

    cy.get('#AdjustmentType').select(testData.AdjustmentType)

    cy.get('#ManageType').select(testData.ManageType)

    if (modify) {
        customFunctions.SetCheckbox('.input__checkbox-input', testData.Active)

        var RowLoop = Array.from({ length: testData.Table.From.length - 1 }, (v, k) => k)
        cy.wrap(RowLoop).each((i) => {
            cy.get('[data-cy=add-pricing-rule-adjustment-btn]').click()
        })

        cy.get('tbody').then((table) => {
            cy.wrap(table[0].childNodes).each((tr, i) => {
                switch (testData.AdjustmentType) {
                    case AdjustmentTypes.TARIFF: cy.wrap(tr[0].cells[0].childNodes[0]).type(testData.Table.From[i])

                        cy.route('GET', tariffRoute + '/*').as('GetTariff')

                        cy.wrap(tr[0].cells[1].childNodes[0].childNodes[0]).select(testData.Table.Adjustment[i])

                        cy.wait('@GetTariff')
                        break

                    default: cy.wrap(tr[0].cells[0].childNodes[0]).type(testData.Table.From[i])
                        cy.wrap(tr[0].cells[1].childNodes[0]).type(testData.Table.Adjustment[i])
                        break
                }
            })

            if (testData.AdjustmentType == AdjustmentTypes.TARIFF) {
                cy.get('#StayDuration').select(testData.StayDuration)

                cy.wrap(table[0].childNodes).each((tr, i) => {
                    cy.wrap(tr[0].cells[2].childNodes[0].childNodes[0]).select(testData.Table.Ladder[i])
                })
            }

        })

        cy.get('.text-center > .btn')

        cy.get('[data-cy=add-pricing-rule-adjustment-btn]')
    }
}

function CheckTable(testData, DateFrom, DateTo) {
    var found = false
    cy.get('.table-component__table__body')
        .find('tr').each((tr) => {
            if (tr[0].cells[0].innerText == testData.RuleName) {
                cy.wrap(tr[0].cells[0].innerText).should('eq', testData.RuleName)
                cy.wrap(tr[0].cells[1].innerText).should('eq', moment(DateFrom).format('DD MMM YYYY'))
                cy.wrap(tr[0].cells[2].innerText).should('eq', moment(DateTo).format('DD MMM YYYY'))
                cy.wrap(tr[0].cells[3].innerText).should('eq', testData.AdjustmentType)
                cy.wrap(tr[0].cells[4].innerText).should('eq', testData.Active.toString())
                found = true
            }
        }).then(() => {
            cy.wrap(found).should('eq', true)
        })
}