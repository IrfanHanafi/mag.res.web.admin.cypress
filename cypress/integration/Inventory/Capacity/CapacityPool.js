import * as Data from '../../../fixtures/AdminSetup.json'

const capacityPoolRoute = '/inventory/api/capacitypool'
const poolTypeRoute = 'inventory/api/capacitypool/pooltype'


describe('Capacity Pool Tests', function () {

    const CapacityPool = Data.Inventory.Capacity.CapacityPool

    beforeEach(function () {
        cy.logInNoPre("/inventory")

        cy.route('GET', capacityPoolRoute).as('GetData')
        cy.route('GET', poolTypeRoute).as('GetPoolType')

        cy.get('[data-cy=sidebar-item-capacity]').click()
        cy.get('[data-cy=sidebar-item-capacity-capacity-pool]').click()

        cy.wait('@GetData')
        cy.wait('@GetPoolType')
    })

    it('Filter Table for Capacity Pool', function () {

        var found = false
        cy.get('#CapacityPoolName').type(CapacityPool.Original.PoolName)

        cy.get('.table-component__table__body > tr').each((tr) => {
            if (tr[0].cells[0].innerText == CapacityPool.Original.PoolName) {
                cy.wrap(tr[0].cells[0]).contains(CapacityPool.Original.PoolName)
                found = true
            }
        }).then(() => {
            cy.wrap(found).should('eq', true)
        })
    })

    it('Modify Capacity Pool', function () {
        Modify(CapacityPool.Modified, CapacityPool.Original.PoolName)

    })

    it('Checks Table for Modified Capacity Pool', function () {
        CheckTable(CapacityPool.Modified)
    })

    it('Reverts Modified Capacity Pool', function () {
        Modify(CapacityPool.Original, CapacityPool.Modified.PoolName)
    })

    it('Checks Table for Reverted Capacity Pool', function () {
        CheckTable(CapacityPool.Original)
    })
})

function Modify(testData, search) {
    var modified = false

    cy.get('.table-component__table__body > tr').each((tr, i) => {
        if (tr[0].cells[0].innerText == search) {

            cy.route('GET', capacityPoolRoute + '/*').as('GetPool')
            cy.route('PUT', capacityPoolRoute + '/*').as('PutData')


            cy.wrap(tr[0].cells[5]).contains('Change').click()

            cy.wait('@GetPool')

            cy.get('.card-body > :nth-child(1) > .row > .field > :nth-child(1) > .input > .form-control').clear().type(testData.PoolName)

            cy.get(':nth-child(6) > .row > .field > :nth-child(1) > .input > .form-control').clear().type(testData.Capacity)

            cy.get('#AlertPercentage').clear().type(testData.Alert)

            cy.get('[data-cy=capacity-pool-form-button-submit]').click()

            cy.wait('@PutData').then((response) => {
                cy.wrap(response.status).should('eq', 200)
                modified = true
            })
        }
    }).then(() => {
        cy.wrap(modified).should('eq', true)
    })
}

function CheckTable(testData) {
    var found = false
    cy.get('.table-component__table__body > tr').each((tr) => {
        if (tr[0].cells[0].innerText == testData.PoolName) {
            cy.wrap(tr[0].cells[0].innerText).should('eq', testData.PoolName)
            cy.wrap(tr[0].cells[1].innerText).should('eq', testData.SegmentationType)
            cy.wrap(tr[0].cells[2].innerText).should('eq', testData.PoolType)
            cy.wrap(tr[0].cells[3].innerText).should('eq', testData.Capacity.toString())
            cy.wrap(tr[0].cells[4].innerText).should('eq', testData.Alert.toString())
            found = true
        }
    }).then(() => {
        cy.wrap(found).should('eq', true)
    })
}