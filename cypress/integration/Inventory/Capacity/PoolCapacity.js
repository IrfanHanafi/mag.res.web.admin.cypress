import moment from 'moment'
import * as Data from '../../../fixtures/AdminSetup.json'
import * as customFunctions from '../../../support/functions.js'

const capacityPoolRoute = '**/api/capacitypool'

describe('Pool Capacity Test', function () {
    const CapacityPool = Data.Inventory.Capacity.PoolCapacity

    var fromDate = customFunctions.GetDate(CapacityPool.DateFrom)

    var toDate = customFunctions.GetDate(CapacityPool.DateTo)

    beforeEach(() => {
        cy.logInNoPre("/inventory")

        cy.route('GET', capacityPoolRoute).as('GetData')

        cy.get('[data-cy=sidebar-item-capacity]').click()
        cy.get('[data-cy=sidebar-item-capacity-pool-capacity]').click()

        cy.wait('@GetData')
    })

    it('Capacity Pool Traversal', function () {

        cy.route('GET', capacityPoolRoute + '/*/*').as('GetCapacityPool')

        cy.get('[data-cy=pool-capacity-form-select]').select(CapacityPool.Name)

        cy.get('#searchDateFrom').invoke('removeAttr', 'readonly').clear().type(moment(fromDate).format('MMMM DD, YYYY'))
        cy.get('.page-heading__title').click()

        cy.get('#searchDateTo').invoke('removeAttr', 'readonly').clear().type(moment(toDate).format('MMMM DD, YYYY'))
        cy.get('.page-heading__title').click()

        cy.get('[data-cy=pool-capacity-form-button-search]').click()

        var monthCapacity = cy.wait('@GetCapacityPool').then((capacitypool) => {

            cy.wrap(capacitypool.response.body).each((month, i) => {
                monthCapacity[i] = month.value
            })
            return monthCapacity;
        })

        cy.wait('@GetFeatures')

        cy.get('.card-body').get('.table-calendar').each((calendar, i) => {
            var currentMonth = ""
            cy.wrap(calendar).siblings('.table-header').then((header) => {

                cy.log(header[0].innerText)
                currentMonth = moment(header[0].innerText, 'MMMM YYYY')

                var firstDay = calendar[0].childNodes[0].childNodes[2].childNodes[0].cells[0].textContent
                var tableCalendar = moment(firstDay + " " + moment(currentMonth).format('MMMM'), 'Do MMMM YYYY')

                var j = 0

                if (firstDay != "1st") {
                    tableCalendar = moment(tableCalendar).subtract(1, 'month')
                }
                cy.wrap(calendar[0].childNodes[0].childNodes[2].childNodes).each((row) => {
                    cy.wrap(row[0].cells).each((cells) => {
                        if (moment(tableCalendar).isSame(currentMonth, 'month')) {
                            if (moment(tableCalendar).isAfter(moment(fromDate).subtract(1, 'day')) && moment(tableCalendar).isBefore(toDate)) {
                                debugger
                                var InputValue = parseInt(cells[0].childNodes[0].childNodes[2].childNodes[0].value)
                                cy.wrap(InputValue).should('eq', monthCapacity[i][j].value.capacity)
                                j++
                            }
                        }
                        tableCalendar = moment(tableCalendar).add(1, 'day')
                    })
                })
            })
        })
    })
})