import * as customFunctions from '../../support/functions.js'
import * as Data from '../../fixtures/AdminSetup.json'

const airportReferenceRoute = 'distribution/api/reference/airport'
const channelRoute = 'distribution/api/channel'
const airportRoute = 'distribution/api/airport'
const channelAddonRoute = 'distribution/api/channelAddon'
const channelCapacityPoolRoute = 'distribution/api/capacityPool/channelCapacityPool'
const AddOnLinkReferenceRoute = 'distribution/api/reference/GetAddOnsForAirportAndChannel'

describe('Channels to Add-Ons Test', function () {

    const ChannelToAddOn = Data.Distribution.ChannelToAddOn

    beforeEach(function () {
        cy.logInNoFeature("/distribution")

        cy.route('GET', airportReferenceRoute).as('GetAirportRef')
        cy.route('GET', channelRoute).as('GetChannel')

        cy.get('[data-cy=sidebar-item-distribution]').click()
        cy.get('[data-cy=sidebar-item-distribution-channel-to-addons]').click()

        cy.wait('@GetChannel')
    })

    /*describe('Filter Test', function () {
        it('Airport Filter', function () {

            Object.keys(Airport).forEach(function (key) {
                cy.get('[data-cy=link-airport-to-channels-list-airport-select] > .field > :nth-child(1) > .input > .form-control').then((select) => {
                    var options = Array.from({ length: select[0].length }, (v, k) => k)
                    cy.wrap(options).each((i) => {
                        if (select[0][i].childNodes[0].textContent == Airport[key]) {
                            var id = select[0][i].value
                            cy.route('GET', airportRoute + '/*' + '/channel/unlinked').as('GetAirportID')
                            cy.route('GET', channelAirportRoute + '/*').as('GetAirportChannel')
                        }
                    })
                })

                cy.get('[data-cy=link-airport-to-channels-list-airport-select] > .field > :nth-child(1) > .input > .form-control').select(Airport[key])
                cy.wait("@GetAirportID")
                cy.wait("@GetAirportChannel")

                cy.get('tbody')
                    .find('tr').each((tr) => {
                        cy.wrap(tr[0].cells[0].innerText).should('eq', Airport[key])
                    })
            })
        })

        it('Channel Filter', function () {

            SearchLink(ChannelToAddOn.Filter)

            cy.get('tbody')
                .find('tr').each((tr) => {
                    cy.wrap(tr[0].cells[1].innerText).should('eq', ChannelToAddOn.Filter.Channel)
                })
        })
    })*/

    describe('FeatureTest', function () {

        it('Create Channel to Airport Link', function () {

            cy.route('GET', channelCapacityPoolRoute).as('GetChannelCapacity')
            cy.route('POST', channelAddonRoute).as('PostData')

            cy.get('[data-cy=link-channels-to-addons-list-create-button]').click()

            cy.wait('@GetAirportRef')
            cy.wait('@GetChannelCapacity')

            FillForm(ChannelToAddOn.Original, '@PostData', false)

        })

        it('Check Created Table', function () {
            CheckTable(ChannelToAddOn.Original)
        })

        it('Modify Channel to Airport', function () {
            ModifyChannelToAirport(ChannelToAddOn.Modified, ChannelToAddOn.Original)
        })

        it('Check Modified Table', function () {

            CheckTable(ChannelToAddOn.Modified)
        })

        it('Unlink', function () {

            var deleted = false

            SearchLink(ChannelToAddOn.Modified)

            cy.get('tbody > tr').each((tr) => {
                if (tr[0].cells[0].innerText == ChannelToAddOn.Modified.Airport && tr[0].cells[1].innerText == ChannelToAddOn.Modified.Channel && tr[0].cells[2].innerText == ChannelToAddOn.Modified.AddOn) {

                    cy.route('DELETE', channelAddonRoute + '/*').as('DeleteData')

                    //wait for pop-up to disappear
                    cy.wait(1500)
                    cy.wrap(tr[0].cells[5]).contains('Unlink').click()

                    cy.get('.modal-footer > .btn-primary').click()

                    cy.wait('@DeleteData').then((response) => {
                        cy.wrap(response.status).should('eq', 204)
                        deleted = true
                    })
                }
            }).then(() => {
                cy.wrap(deleted).should('eq', true)
            })
        })
    })

})

function ModifyChannelToAirport(testData, target) {

    cy.route('GET', channelCapacityPoolRoute).as('GetChannelCapacityPool')
    cy.route('GET', channelAddonRoute + '/*').as('GetChannelAddOn')
    cy.route('GET', AddOnLinkReferenceRoute + '?airportId=*&channelId=*').as('GetLinkAddOn')

    cy.route('PUT', channelAddonRoute + '/*').as('PutData')
    SearchLink(testData)

    cy.get('tbody > tr').each((tr) => {
        if (tr[0].cells[0].innerText == target.Airport && tr[0].cells[1].innerText == target.Channel && tr[0].cells[2].innerText == target.AddOn) {
            cy.wrap(tr[0].cells[5]).contains('Change').click()

            cy.wait('@GetAirportRef')
            cy.wait('@GetChannelCapacityPool')
            cy.wait('@GetChannelAddOn')
            cy.wait('@GetAirportID')
            cy.wait('@GetLinkAddOn')

            FillForm(testData, '@PutData', true)
        }
    })
}

function SearchLink(testData) {

    cy.route('GET', airportRoute + '/*' + '/channel').as('GetAirportID')

    cy.get('[data-cy=link-channels-to-addons-list-airport-dropdown] > .field > :nth-child(1) > .input > .form-control').select(testData.Airport)

    cy.wait('@GetAirportID')

    cy.route('GET', channelAddonRoute + '?airportId=*&channelId=*').as('GetLink')

    cy.get('[data-cy=link-channels-to-addons-list-channel-dropdown] > .field > :nth-child(1) > .input > .form-control').select(testData.Channel)

    cy.wait('@GetLink')
}

function FillForm(testData, call, modify = false) {
    if (!modify) {

        cy.route('GET', airportRoute + '/*/channel').as('GetAirportID')

        cy.get('[data-cy=link-channels-to-add-ons-select-airport] > .field > :nth-child(1) > .input > .form-control').select(testData.Airport)

        cy.wait('@GetAirportID')

        cy.route('GET', AddOnLinkReferenceRoute + '?airportId=*&channelId=*').as('GetLink')

        cy.get('[data-cy=link-channels-to-add-ons-select-channels] > .field > :nth-child(1) > .input > .form-control').select(testData.Channel)

        cy.wait('@GetLink')

        cy.get('[data-cy=link-channels-to-add-ons-select-add-on] > .field > :nth-child(1) > .input > .form-control').select(testData.AddOn)
    }
    else {
        customFunctions.SetCheckbox('.input__checkbox-input', testData.ProductAvailability)

        if (!testData.ProductAvailability) {

            var Loop = Array.from({ length: testData.Product.length }, (v, k) => k)

            cy.wrap(Loop).each((i) => {
                cy.get('[data-cy=link-channels-to-add-ons-select-product] > .field > :nth-child(1) > .input > .form-control').select(testData.Product[i])
                cy.get('.col-2 > .btn').click()
            })
        }
    }

    cy.get('[data-cy=link-channels-to-add-ons-select-capacity-pool] > .field > :nth-child(1) > .input > .form-control').select(testData.CapacityPool)

    cy.get('[data-cy=link-channels-to-addons-action-button]').click()

    cy.wait(call).then((response) => {
        cy.wrap(response.status).should('eq', 200)
    })
}

function CheckTable(testData) {

    var found = false

    SearchLink(testData)

    cy.get('tbody > tr').each((tr) => {
        if (tr[0].cells[0].innerText == testData.Airport && tr[0].cells[1].innerText == testData.Channel && tr[0].cells[2].innerText == testData.AddOn) {
            cy.wrap(tr[0].cells[0].innerText).should('eq', testData.Airport)
            cy.wrap(tr[0].cells[1].innerText).should('eq', testData.Channel)
            cy.wrap(tr[0].cells[2].innerText).should('eq', testData.AddOn)
            var CapacityPool = (testData.CapacityPool == "Please Select") ? "Unassigned" : testData.CapacityPool
            cy.wrap(tr[0].cells[3].innerText).should('eq', CapacityPool)
            cy.wrap(tr[0].cells[4].innerText).should('eq', testData.ProductAvailability.toString())
            found = true
        }
    }).then(() => {
        cy.wrap(found).should('eq', true)
    })
}