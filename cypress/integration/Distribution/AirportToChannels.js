import * as customFunctions from '../../support/functions.js'
import * as Data from '../../fixtures/AdminSetup.json'

const airportReferenceRoute = 'distribution/api/reference/airport'
const airportRoute = 'distribution/api/airport'
const channelRoute = 'distribution/api/channel'
const emailDeliveryBookingRoute = 'distribution/api/emailDelivery/Booking'
const emailDeliveryPreConfirmationRoute = 'distribution/api/emailDelivery/PreConfirmation'
const emailDeliveryReminderRoute = 'distribution/api/emailDelivery/Reminder'
const emailDeliveryBookingCancellationRoute = 'distribution/api/emailDelivery/bookingCancellation'
const emailDeliveryNonCarParkBookingRoute = 'distribution/api/emailDelivery/NonCarparkBooking'
const channelAirportRoute = '/distribution/api/channelAirport'

describe('Airport To Channels Test', function () {

    const Airport = Data.Enum.Airport

    const AirportToChannel = Data.Distribution.AirportToChannel

    beforeEach(function () {
        cy.logIn("/distribution")

        cy.route('GET', airportReferenceRoute).as('GetAirportReference')
        cy.route('GET', channelRoute).as('GetChannel')

        cy.get('[data-cy=sidebar-item-distribution]').click()
        cy.get('[data-cy=sidebar-item-distribution-airport-to-channels]').click()

        cy.wait('@GetAirportReference')
        cy.wait('@GetChannel')
    })

    describe('Filter Test', function () {
        it('Airport Filter', function () {
            Object.keys(Airport).forEach(function (key) {
                cy.route('GET', airportRoute + '/*/channel/unlinked').as('GetAirportID')
                cy.route('GET', channelAirportRoute + '/*').as('GetAirportChannel')


                cy.get('[data-cy=link-airport-to-channels-list-airport-select] > .field > :nth-child(1) > .input > .form-control').select(Airport[key])
                cy.wait("@GetAirportID")
                cy.wait("@GetAirportChannel")

                cy.get('tbody > tr').each((tr) => {
                    cy.wrap(tr[0].cells[0].innerText).should('eq', Airport[key])
                })
            })
        })

        it('Channel Filter', function () {

            SearchLink(AirportToChannel.Filter)

            cy.get('tbody > tr').each((tr) => {
                cy.wrap(tr[0].cells[1].innerText).should('eq', AirportToChannel.Filter.Channel)
            })
        })
    })

    describe('FeatureTest', function () {

        it('Create Channel to Airport Link', function () {
            cy.route('GET', emailDeliveryBookingRoute).as('GetEmailBooking')
            cy.route('GET', emailDeliveryPreConfirmationRoute).as('GetEmailPreConfirmation')
            cy.route('GET', emailDeliveryReminderRoute).as('GetEmailReminder')
            cy.route('GET', emailDeliveryBookingCancellationRoute).as('GetEmailBookingCancellation')
            cy.route('GET', emailDeliveryNonCarParkBookingRoute).as('GetEmailNonCarParkBooking')

            cy.get('[data-cy=link-airport-to-channels-list-button-create]').click()

            cy.wait('@GetEmailBooking')
            cy.wait('@GetEmailPreConfirmation')
            cy.wait('@GetEmailReminder')
            cy.wait('@GetEmailBookingCancellation')
            cy.wait('@GetEmailNonCarParkBooking')

            FillForm(AirportToChannel.Original, '@PostData', false)

        })

        it('Check Created Table', function () {
            var found = false
            SearchLink(AirportToChannel.Modified)

            cy.get('tbody > tr').each((tr) => {
                if (tr[0].cells[0].innerText == AirportToChannel.Modified.Airport && tr[0].cells[1].innerText == AirportToChannel.Modified.Channel) {
                    cy.wrap(tr[0].cells[2].innerText).should('eq', AirportToChannel.Original.OnSale.toString())
                    found = true
                }
            }).then(() => {
                cy.wrap(found).should('eq', true)
            })
        })

        it('Modify Channel to Airport', function () {
            SearchLink(AirportToChannel.Modified)

            cy.route('GET', emailDeliveryBookingRoute).as('GetEmailBooking')
            cy.route('GET', emailDeliveryPreConfirmationRoute).as('GetEmailPreConfirmation')
            cy.route('GET', emailDeliveryReminderRoute).as('GetEmailReminder')
            cy.route('GET', emailDeliveryBookingCancellationRoute).as('GetEmailBookingCancellation')
            cy.route('GET', emailDeliveryNonCarParkBookingRoute).as('GetEmailNonCarParkBooking')

            cy.get('tbody > tr').each((tr) => {
                if (tr[0].cells[0].innerText == AirportToChannel.Modified.Airport && tr[0].cells[1].innerText == AirportToChannel.Modified.Channel) {
                    cy.wrap(tr[0].cells[3]).contains('Change').click()

                    cy.wait('@GetEmailBooking')
                    cy.wait('@GetEmailPreConfirmation')
                    cy.wait('@GetEmailReminder')
                    cy.wait('@GetEmailBookingCancellation')
                    cy.wait('@GetEmailNonCarParkBooking')
                    cy.wait('@GetLink')

                    FillForm(AirportToChannel.Modified, '@PutData', true)
                }
            })
        })

        it('Check Modified Table', function () {
            var found = false
            SearchLink(AirportToChannel.Modified)

            cy.get('tbody > tr').each((tr) => {
                if (tr[0].cells[0].innerText == AirportToChannel.Modified.Airport && tr[0].cells[1].innerText == AirportToChannel.Modified.Channel) {
                    cy.wrap(tr[0].cells[2].innerText).should('eq', AirportToChannel.Modified.OnSale.toString())
                    found = true
                }
            }).then(() => {
                cy.wrap(found).should('eq', true)
            })
        })

        it('Unlink', function () {
            SearchLink(AirportToChannel.Modified)

            cy.get('tbody > tr').each((tr) => {
                if (tr[0].cells[0].innerText == AirportToChannel.Modified.Airport && tr[0].cells[1].innerText == AirportToChannel.Modified.Channel) {
                    cy.wrap(tr[0].cells[3]).contains('Unlink').click()

                    cy.get('.modal-footer > .btn-primary').click()

                    cy.wait('@DeleteData').then((response) => {
                        cy.wrap(response.status).should('eq', 200)
                    })
                }
            })
        })
    })

    function SearchLink(testData) {

        cy.route('GET', airportRoute + '/*/channel/unlinked').as('GetAirportID')
        cy.route('GET', channelAirportRoute + '/*').as('GetAirportChannel')

        cy.get('[data-cy=link-airport-to-channels-list-airport-select] > .field > :nth-child(1) > .input > .form-control').select(testData.Airport)

        cy.wait('@GetAirportID')
        cy.wait('@GetAirportChannel')

        cy.route('GET', channelRoute + '/*').as('GetChannelID')

        cy.get('.md-3 > .field > :nth-child(1) > .input > .form-control').select(testData.Channel)


        cy.route('GET', channelAirportRoute + '/*/links/channel/*').as('GetLink')
        cy.route('POST', channelAirportRoute + '/*/links/channel/*').as('PostData')
        cy.route('PUT', channelAirportRoute + '/*/links/channel/*').as('PutData')
        cy.route('DELETE', channelAirportRoute + '/*/links/channel/*').as('DeleteData')



        cy.wait('@GetChannelID')
    }
    function FillForm(testData, call, modify = false) {

        if (!modify) {

            cy.route('GET', airportRoute + '/*/channel/unlinked').as('GetAirportID')

            cy.get('[value="[object Object]"] > .field > :nth-child(1) > .input > .form-control').select(testData.Airport)

            cy.wait('@GetAirportID')

            cy.get('[placeholder="df"] > .field > :nth-child(1) > .input > .form-control').select(testData.Channel)

            cy.route('POST', channelAirportRoute + '/*/links/channel/*').as('PostData')
        }

        customFunctions.SetCheckbox('.input__checkbox-input', testData.OnSale)

        cy.get(':nth-child(4) > .row > .field > :nth-child(1) > .input > .form-control').clear().type(testData.OffSaleText)

        cy.get(':nth-child(5) > .row > .field > :nth-child(1) > .input > .form-control').clear().type(testData.MerchantAccount)

        cy.get(':nth-child(6) > .field > :nth-child(1) > .input > .form-control').select(testData.BookingTemplate)

        cy.get(':nth-child(7) > .field > :nth-child(1) > .input > .form-control').select(testData.PreConfirmationTemplate)

        cy.get(':nth-child(8) > .field > :nth-child(1) > .input > .form-control').select(testData.ReminderTemplate)

        cy.get(':nth-child(9) > .field > :nth-child(1) > .input > .form-control').select(testData.CancellationTemplate)

        cy.get(':nth-child(10) > .field > :nth-child(1) > .input > .form-control').select(testData.ExtrasOnlyBookingTemplate)

        cy.get(':nth-child(11) > .field > :nth-child(1) > .input > .form-control').select(testData.AccessMethodAvailableReminder)

        cy.get('[data-cy=link-airport-to-channels-form-button-submit]').click()

        cy.wait(call).then((response) => {
            cy.wrap(response.status).should('eq', 200)
        })
    }
})