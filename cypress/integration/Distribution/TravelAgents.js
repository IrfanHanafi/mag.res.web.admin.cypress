import * as customFunctions from "../../support/functions.js"
import * as Data from '../../fixtures/AdminSetup.json'

const travelAgentRoute = '/distribution/api/travelAgent'
const channelRoute = '/distribution/api/channel'
const travelAgentCategoryRoute = '/distribution/api/AgentCategory'

const commissionTypeRoute = '/distribution/api/commissionType'

describe('Travel Agent Test', function () {

    const TravelAgent = Data.Distribution.TravelAgent

    beforeEach(() => {
        cy.logInNoFeature('/distribution')

        cy.route('GET', travelAgentRoute).as('GetData')

        cy.get('[data-cy=sidebar-item-distribution]').click()
        cy.get('[data-cy=sidebar-item-distribution-travel-agents]').click()

        cy.wait('@GetData')
    })

    describe('Filter Tests', function () {
        it('Search Term Channel Filter', function () {

            cy.get('[data-cy=travel-agent-list-filter]').clear().type(TravelAgent.Original.Channel)

            var found = false
            cy.get('tbody > tr').each((tr) => {
                    if (tr[0].cells[1].innerText == TravelAgent.Original.Channel) {
                        cy.wrap(tr[0].cells[1].innerText).should('eq', TravelAgent.Original.Channel)
                        found = true
                    }
                }).then(() => {
                    cy.wrap(found).should('eq', true)
                })

        })

        it('Search Term Name Filter', function () {

            cy.get('[data-cy=travel-agent-list-filter]').clear().type(TravelAgent.Original.Name)

            var found = false
            cy.get('tbody > tr').each((tr) => {
                    if (tr[0].cells[0].innerText == TravelAgent.Original.Name) {
                        cy.wrap(tr[0].cells[0].innerText).should('eq', TravelAgent.Original.Name)
                        found = true
                    }
                }).then(() => {
                    cy.wrap(found).should('eq', true)
                })

        })

        describe('Feature Test', function () {
            it('Modifies TravelAgent', function () {
                Modify(TravelAgent.Modified, TravelAgent.Original.Name)
            })

            it('Checks Table for Modified TravelAgent', function () {
                CheckTable(TravelAgent.Modified)
            })

            it('Reverts TravelAgent', function () {
                Modify(TravelAgent.Original, TravelAgent.Modified.Name)
            })

            it('Checks Table for Reverted TravelAgent', function () {
                CheckTable(TravelAgent.Original)
            })
        })
    })
})

function Modify(testData, search) {
    var modified = false

    cy.route('GET', channelRoute).as('GetChannel')
    cy.route('GET', travelAgentCategoryRoute).as('GetTravelAgentCategory')

    cy.route('GET', commissionTypeRoute).as('GetCommissionType')

    cy.get('tbody > tr').each((tr) => {
        if (tr[0].cells[0].innerText == search) {
            cy.route('GET', travelAgentRoute + '/*').as('GetTravelAgent')

            cy.wait(1000)
            cy.wrap(tr[0].cells[2]).contains('Change').click()

            cy.route('PUT', travelAgentRoute + '/*').as('PutData')

            cy.wait('@GetChannel')
            cy.wait('@GetTravelAgent')
            cy.wait('@GetTravelAgentCategory')

            cy.wait('@GetCommissionType')

            cy.get('[data-cy=distribution-channel-select] > .field > :nth-child(1) > .input > .form-control').select(testData.Channel)
            cy.get('[data-cy=company-name-input] > .row > .field > :nth-child(1) > .input > .form-control').clear().type(testData.Name)
            cy.get('[data-cy=company-address-input] > .field > :nth-child(1) > .input > .form-control').clear().type(testData.Address)
            cy.get('[data-cy=company-phone-input] > .row > .field > :nth-child(1) > .input > .form-control').clear().type(testData.PhoneNumber)
            cy.get('[data-cy=agent-category-select] > .field > :nth-child(1) > .input > .form-control').select(testData.Category)

            customFunctions.SetCheckbox('[data-cy=credit-agent-checkbox] > .row > .field > .form-check > .input__checkbox-label > .input__checkbox-input', testData.CreditAgent)

            customFunctions.SetCheckbox('[data-cy=apply-discount-checkbox] > .row > .field > .form-check > .input__checkbox-label > .input__checkbox-input', testData.ApplyDiscount.Checked)

            if (testData.ApplyDiscount.Checked) {
                cy.get('[data-cy=discount-type-select] > .field > :nth-child(1) > .input > .form-control').select(testData.ApplyDiscount.DiscountType)
                cy.get('[data-cy=discount-amount-input] > .row > .field > :nth-child(1) > .input > .form-control').clear().type(testData.ApplyDiscount.DiscountAmount)
            }

            customFunctions.SetCheckbox('[data-cy=calculate-commission-checkbox] > .row > .field > .form-check > .input__checkbox-label > .input__checkbox-input', testData.CalculateCommission.Checked)

            if (testData.CalculateCommission.Checked) {
                cy.get('[data-cy=commission-type-select] > .field > :nth-child(1) > .input > .form-control').select(testData.CalculateCommission.CommissionType)
                cy.get('[data-cy=commission-amount-input] > .row > .field > :nth-child(1) > .input > .form-control').clear().type(testData.CalculateCommission.CommissionAmount)
            }
            customFunctions.SetCheckbox('[data-cy=display-parking-charge-checkbox] > .row > .field > .form-check > .input__checkbox-label > .input__checkbox-input', testData.DisplayParkingChargeConfirmation)
            customFunctions.SetCheckbox('[data-cy=send-confirmation-email-checkbox] > .row > .field > .form-check > .input__checkbox-label > .input__checkbox-input', testData.ConfirmationEmail)
            customFunctions.SetCheckbox('[data-cy=allow-cancellation-checkbox] > .row > .field > .form-check > .input__checkbox-label > .input__checkbox-input', testData.AllowCancellations)
            customFunctions.SetCheckbox('[data-cy=restrict-amendments-checkbox] > .row > .field > .form-check > .input__checkbox-label > .input__checkbox-input', testData.RestrictAmendments)
            customFunctions.SetCheckbox('[data-cy=active-checkbox] > .row > .field > .form-check > .input__checkbox-label > .input__checkbox-input', testData.Active)
            customFunctions.SetCheckbox('[data-cy=allow-save-card-details-checkbox] > .row > .field > .form-check > .input__checkbox-label > .input__checkbox-input', testData.SaveCardDetails)

            cy.get('[data-cy=save-travel-agent-btn]').click()

            cy.wait('@PutData').then((response) => {
                cy.wrap(response.status).should('eq', 200)
                modified = true
            })
        }
    }).then(() => {
        cy.wrap(modified).should('eq', true)
    })
}

function CheckTable(testData) {
    var found = false
    cy.get('tbody > tr').each((tr) => {
        if (tr[0].cells[0].innerText == testData.Name) {
            cy.wrap(tr[0].cells[0].innerText).should('eq', testData.Name)
            cy.wrap(tr[0].cells[1].innerText).should('eq', testData.Channel)
            found = true
        }
    }).then(() => {
        cy.wrap(found).should('eq', true)
    })
}