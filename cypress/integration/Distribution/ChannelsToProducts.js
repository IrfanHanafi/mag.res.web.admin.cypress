import * as customFunctions from '../../support/functions.js'
import * as Data from '../../fixtures/AdminSetup.json'

const airportReferenceRoute = '**/api/reference/airport'
const airportRoute = '**/api/airport'
const channelProductRoute = '**/api/channelProduct'
const productTypeRoute = '**/api/productType'
const pricingBasisRoute = '**/api/pricingBasis'
const productCapacityPool = '**/api/capacityPool/productCapacityPool'
const productSearchStatusRoute = '**/api/productSearchStatus'
const productRoute = '**/api/product'
const capacityPoolRoute = '**/api/capacityPool'
const pricePerDayTariffRoute = '**/api/pricePerDayTariff'
const contractTariffRoute = '**/api/contractTariff'
const tariffReferenceRoute = '**/api/reference/tariff'

const Airport = Data.Enum.Airport

const ChannelToProduct = {
    Original: {
        Airport: "Manchester Airport",
        Channel: "AutoTest Channel",
        ProductType: "Pre Book Products",
        PricingBasis: "Daily Pricing",
        CarParkName: "STAFF Jetparks1",
        Product: "JP1",
        ProductSearch: "Standard",

        StandardPriceTariff: "AutoTest Tariff",
        StandardLadder: "A1Asif",
        MemberPriceTariff: "CB Test",
        MemberLadder: "00",
        PayOnFootPriceTariff: "Asif_Test",
        PayOnFootLadder: "A4",

        CarParkCapacityPool: "StaffJP1",
        ProductCapacityPool: "EMAPickup",

        BookingDates: {
            StartDate: {
                Amount: 1,
                Period: "week"
            },
            EndDate: {
                Amount: 1,
                Period: "month"
            },
            ChannelDisplayName: "AutoTest Book Date",
            ChannelProductDetail: "this is a test",
            BookingFee: 0,
            SMSFee: 0,
            ChannelCommission: false,
            CancellatonFee: 0,
            Commission: 0
        }
    },
    Modified: {
        Airport: "Manchester Airport",
        Channel: "AutoTest Channel",
        ProductType: "Pre Book Products",
        PricingBasis: "Daily Pricing",
        CarParkName: "STAFF Jetparks1",
        Product: "JP1",
        ProductSearch: "Associated",

        StandardPriceTariff: "ABC01",
        StandardLadder: "A10",
        MemberPriceTariff: "CB Test 2",
        MemberLadder: "A3",
        PayOnFootPriceTariff: "Asif Long Stay",
        PayOnFootLadder: "A1Asif",

        CarParkCapacityPool: "StaffJP1",
        ProductCapacityPool: "PremiAir Arrive",

        BookingDates: {
            StartDate: {
                Amount: 2,
                Period: "week"
            },
            EndDate: {
                Amount: 2,
                Period: "month"
            },
            ChannelDisplayName: "Modified Book Date",
            ChannelProductDetail: "this is a modified test",
            BookingFee: 0,
            SMSFee: 0,
            ChannelCommission: false,
            CancellatonFee: 0,
            Commission: 0
        }
    }
}

const LinkAction = {
    LINK: "Link",
    UNLINK: 'Unlink'
}

const Tab = {
    DETAILS: 1,
    BOOKINGDATES: 2
}

describe('Channels To Products Test', function () {


    beforeEach(function () {
        cy.logIn("/distribution")

        cy.route('GET', airportReferenceRoute).as('GetAirportRef')
        cy.route('GET', productTypeRoute).as('GetProductType')
        cy.route('GET', pricingBasisRoute).as('GetPricingBasis')

        cy.get('[data-cy=sidebar-item-distribution]').click()
        cy.get('[data-cy=sidebar-item-distribution-channels-to-products]').click()

        cy.wait('@GetAirportRef')
        cy.wait('@GetProductType')
        cy.wait('@GetPricingBasis')
    })

    describe('FeatureTest', function () {

        it('Modify Channel to Airport Details', function () {
            ModifyChannelToProduct(ChannelToProduct.Modified)
        })

        it('Check Modified Channel to Airport Details', function () {
            CheckDetails(ChannelToProduct.Modified)
        })

        it('Revert Channel to Airport Details', function () {
            ModifyChannelToProduct(ChannelToProduct.Original)
        })

        it('Check Reverted Channel to Airport Details', function () {
            CheckDetails(ChannelToProduct.Original)
        })

        it('Unlink', function () {
            ModifyLink(ChannelToProduct.Original, LinkAction.UNLINK)
        })

        it('Link', function () {
            ModifyLink(ChannelToProduct.Original, LinkAction.LINK)
        })

        it('Modify Booking Dates', function () {
            ModifyBookingDates(ChannelToProduct.Modified.BookingDates, ChannelToProduct.Original)
        })

        it('Revert Booking Dates', function () {
            ModifyBookingDates(ChannelToProduct.Original.BookingDates, ChannelToProduct.Original)
        })
    })

})

function ModifyBookingDates(testData, target) {

    var modified = false

    cy.route('GET', tariffReferenceRoute + '/*/ladder').as('GetTariff')

    var dateFrom = customFunctions.GetDate(testData.StartDate)
    var dateTo = customFunctions.GetDate(testData.EndDate)

    RouteSetup()

    cy.route('PUT', channelProductRoute + '/*/bookingDate/*').as('PutBookingDates')
    SearchLink(target)

    cy.get('tbody > tr').each((tr) => {
        if (tr[0].cells[0].innerText == target.Airport && tr[0].cells[1].innerText == target.Channel) {
            cy.wrap(tr[0].cells[13]).contains('Change').click()

            RouteWait()
            cy.wait('@GetTariff')

            customFunctions.GetTab(Tab.BOOKINGDATES)
            cy.wait(1000)

            cy.get('tbody > tr').then((tr) => {
                cy.wrap(tr[0].cells[7]).contains('Change').click()

                customFunctions.SetDate('[data-cy=link-channel-to-product-booking-dates-calendar-starting-date] > .field > .input > .date-picker', dateFrom)
                customFunctions.SetDate('[data-cy=link-channel-to-product-booking-dates-calendar-ending-date] > .field > .input > .date-picker', dateTo)

                cy.get('[data-cy=link-channel-to-product-booking-dates-input-channel-display-name] > .row > .field > :nth-child(1) > .input > .form-control').clear().type(testData.ChannelDisplayName)
                cy.get('.editr--content').clear().type(testData.ChannelProductDetail)

                cy.get('[data-cy=link-channel-to-product-booking-dates-btn]').click()

                cy.wait('@PutBookingDates').then((response) => {
                    cy.wrap(response.status).should('eq', 200)
                })

                cy.get('.container > div > .btn').click()

                RouteWait()
                cy.wait('@GetTariff')

                CheckBookingDates(testData)
                modified = true
            })
        }
    }).then(() => {
        cy.wrap(modified).should('eq', true)
    })
}

function CheckBookingDates(testData) {

    var modified = false
    var dateFrom = customFunctions.GetDate(testData.StartDate).format('D/M/YYYY')
    var dateTo = customFunctions.GetDate(testData.EndDate).format('D/M/YYYY')

    cy.get('tbody > tr').then((tr) => {
        cy.wrap(tr[0].cells[0].innerText).should('eq', dateFrom)
        cy.wrap(tr[0].cells[1].innerText).should('eq', dateTo)
        cy.wrap(tr[0].cells[2].innerText).should('eq', testData.BookingFee.toString())
        cy.wrap(tr[0].cells[3].innerText).should('eq', testData.SMSFee.toString())
        cy.wrap(tr[0].cells[4].innerText).should('eq', testData.ChannelCommission.toString())
        debugger
        cy.wrap(tr[0].cells[5].innerText).should('eq', testData.Commission.toString())
        modified = true
    }).then(() => {
        cy.wrap(modified).should('eq', true)
    })
}

function ModifyChannelToProduct(testData) {

    RouteSetup()
    cy.route('PUT', channelProductRoute + '/*').as('SendData')

    var modified = false

    SearchLink(testData)

    cy.get('tbody > tr').each((tr) => {
        if (tr[0].cells[0].innerText == testData.Airport && tr[0].cells[1].innerText == testData.Channel) {
            cy.wrap(tr[0].cells[13]).contains('Change').click()

            RouteWait()

            FillForm(testData, true)
            modified = true
        }
    }).then(() => {
        cy.wrap(modified).should('eq', true)
    })

}

function SearchLink(testData) {


    cy.route('GET', airportRoute + '/*/channel').as('GetAirportID')
    cy.route('GET', channelProductRoute + '?airportId=*&channelId=*&productType=*pricingBasis=*').as('GetLink')


    cy.get('[data-cy=link-channels-to-products-select-airport] > .field > :nth-child(1) > .input > .form-control').select(testData.Airport)

    cy.wait('@GetAirportID')

    cy.get('[data-cy=link-channels-to-products-select-channel] > .field > :nth-child(1) > .input > .form-control').select(testData.Channel)

    cy.get('[data-cy=link-channels-to-products-select--product-type] > .field > :nth-child(1) > .input > .form-control').select(testData.ProductType)

    cy.get('[data-cy=link-channels-to-products-select-pricing-basis] > .field > :nth-child(1) > .input > .form-control').select(testData.PricingBasis)

    cy.get('[data-cy=link-channels-to-products-search-btn]').click({ force: true })

    cy.wait('@GetLink')
}

function FillForm(testData, modify = false) {

    cy.route('GET', tariffReferenceRoute + '/*/ladder').as('GetTariff')

    if (!modify) {
        cy.get('[data-cy=link-channel-to-product-create-select-airport] > .field > :nth-child(1) > .input > .form-control').select(testData.Airport)
        cy.wait('@GetAirportChannel')

        cy.get('[data-cy=link-channel-to-product-create-select-channel] > .field > :nth-child(1) > .input > .form-control').select(testData.Channel)
        cy.wait('@GetChannel')

        cy.get('[data-cy=link-channel-to-product-create-select-product-type] > .field > :nth-child(1) > .input > .form-control').select(testData.ProductType)
        cy.get('[data-cy=link-channel-to-product-create-select-pricing-basis] > .field > :nth-child(1) > .input > .form-control').select(testData.PricingBasis)

        cy.get('[data-cy=link-channel-to-product-create-select-product] > .field > :nth-child(1) > .input > .form-control').select(testData.Product)
        cy.wait('@GetCarParkCapacityPool')

        cy.get('[data-cy=link-channels-to-products-details-select-product-search] > .field > :nth-child(1) > .input > .form-control').select(testData.ProductSearch)

        cy.get('[data-cy=link-channel-to-product-create-select-car-park-capacity-pool] > .field > :nth-child(1) > .input > .form-control').select(testData.CarParkCapacityPool)
        cy.get('[data-cy=link-channel-to-product-create-select-product-capacity-pool] > .field > :nth-child(1) > .input > .form-control').select(testData.ProductCapacityPool)


        customFunctions.SetCheckbox('.input__checkbox-input', testData.PriceImport)
    }

    if (modify) {
        cy.get('[data-cy=link-channels-to-products-details-select-product-search] > .field > :nth-child(1) > .input > .form-control').select(testData.ProductSearch)
        cy.get('[data-cy=link-channels-to-products-details-select-standard-price-tariff] > .field > :nth-child(1) > .input > .form-control').select(testData.StandardPriceTariff)
        cy.wait('@GetTariff')
        cy.get('[data-cy=link-channels-to-products-details-select-stadard-ladder] > .field > :nth-child(1) > .input > .form-control').select(testData.StandardLadder)

        cy.get('[data-cy=link-channels-to-products-details-select-member-price-tariff] > .field > :nth-child(1) > .input > .form-control').select(testData.MemberPriceTariff)
        cy.wait('@GetTariff')

        cy.get('[data-cy=link-channels-to-products-details-select-member-ladder] > .field > :nth-child(1) > .input > .form-control').select(testData.MemberLadder)

        cy.get('[data-cy=link-channels-to-products-details-select-foot-price-tariff] > .field > :nth-child(1) > .input > .form-control').select(testData.PayOnFootPriceTariff)
        cy.wait('@GetTariff')

        cy.get('[data-cy=link-channels-to-products-details-select-foot-ladder] > .field > :nth-child(1) > .input > .form-control').select(testData.PayOnFootLadder)

        cy.get('[data-cy=link-channels-to-products-details-select-car-park-capacity-pool] > .field > :nth-child(1) > .input > .form-control').select(testData.CarParkCapacityPool)
        cy.get('[data-cy=link-channels-to-products-details-select-product-capacity-pool] > .field > :nth-child(1) > .input > .form-control').select(testData.ProductCapacityPool)
        //Change CapacityPool to Same data-cy value
    }


    cy.get('[data-cy=create-link-channel-to-product-btn]').click()

    cy.wait("@SendData").then((response) => {
        cy.wrap(response.status).should('eq', 200)
    })
}

function CheckDetails(testData) {
    var modified = false

    SearchLink(testData)
    //need to make something that turns "Please Select" into "", can add to common functionality
    cy.get('tbody > tr').each((tr) => {
        if (tr[0].cells[0].innerText == testData.Airport && tr[0].cells[1].innerText == testData.Channel) {
            cy.wrap(tr[0].cells[0].innerText).should('eq', testData.Airport)
            cy.wrap(tr[0].cells[1].innerText).should('eq', testData.Channel)
            cy.wrap(tr[0].cells[2].innerText).should('eq', testData.CarParkName)
            cy.wrap(tr[0].cells[3].innerText).should('eq', testData.Product)
            cy.wrap(tr[0].cells[4].innerText).should('eq', testData.ProductSearch)
            cy.wrap(tr[0].cells[5].innerText).should('eq', testData.StandardPriceTariff)
            cy.wrap(tr[0].cells[6].innerText).should('eq', testData.StandardLadder)
            cy.wrap(tr[0].cells[7].innerText).should('eq', testData.MemberPriceTariff)
            cy.wrap(tr[0].cells[8].innerText).should('eq', testData.MemberLadder)
            cy.wrap(tr[0].cells[9].innerText).should('eq', testData.PayOnFootPriceTariff)
            cy.wrap(tr[0].cells[10].innerText).should('eq', testData.PayOnFootLadder)
            cy.wrap(tr[0].cells[11].innerText).should('eq', testData.CarParkCapacityPool)
            cy.wrap(tr[0].cells[12].innerText).should('eq', testData.ProductCapacityPool)
            modified = true
        }
    }).then(() => {
        cy.wrap(modified).should('eq', true)
    })
}

function ModifyLink(target, action) {

    var modified = false

    cy.route('PUT', channelProductRoute + '/*/' + action.toLowerCase()).as('LinkAction')
    SearchLink(ChannelToProduct.Modified)

    cy.get('tbody > tr').each((tr) => {
        if (tr[0].cells[0].innerText == target.Airport && tr[0].cells[1].innerText == target.Channel) {
            cy.wrap(tr[0].cells[13].children[0].children[2].innerText).should('eq', action)
            cy.wrap(tr[0].cells[13]).contains(action).click({ force: true })

            cy.get('.modal-footer > .btn-primary').click()

            cy.wait('@LinkAction').then((response) => {
                cy.wrap(response.status).should('eq', 204)
                modified = true
            })
        }
    }).then(() => {
        cy.wrap(modified).should('eq', true)
    })
}

function RouteSetup() {
    cy.route('GET', channelProductRoute + '/*/bookingDate').as('GetBookingDate')
    cy.route('GET', productSearchStatusRoute).as('GetProductSearchStatus')
    cy.route('GET', airportRoute + '/*/channel').as('GetAirportChannel')
    cy.route('GET', airportRoute + '/*/product?channelId=*').as('GetChannel')
    cy.route('GET', productRoute + '/*/carParkCapacityPool').as('GetCarParkCapacityPool')
    cy.route('GET', capacityPoolRoute + '/productCapacityPool').as('GetProductCapacityPool')
    cy.route('GET', contractTariffRoute).as('GetContractTariff')
    cy.route('GET', tariffReferenceRoute).as('GetTariffRef')
    cy.route('GET', pricePerDayTariffRoute).as('GetPricePerDayTariff')
    cy.route('GET', channelProductRoute + '/*').as('GetChannelProduct')
}

function RouteWait() {
    cy.wait('@GetBookingDate')
    cy.wait('@GetProductSearchStatus')
    cy.wait('@GetCarParkCapacityPool')
    cy.wait('@GetProductCapacityPool')
    cy.wait('@GetContractTariff')
    cy.wait('@GetTariffRef')
    cy.wait('@GetPricePerDayTariff')
    cy.wait('@GetChannelProduct')
}