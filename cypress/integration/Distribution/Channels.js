import * as customFunctions from "../../support/functions.js"
import * as Data from '../../fixtures/AdminSetup.json'

describe('Channel Test', function () {

    const channelRoute = '/distribution/api/channel'

    const commissionTypeRoute = '/distribution/api/commissionType'

    const airportGroupReference = '/distribution/api/reference/airportGroup'

    const emailDeliveryMemberRegistrationRoute = '/distribution/api/emailDelivery/memberRegistration'
    const emailDeliveryForgottenPasswordRoute = '/distribution/api/emailDelivery/forgottenPassword'
    const emailDeliveryBookingCancellationRoute = '/distribution/api/emailDelivery/bookingCancellation'

    const Channel = Data.Distribution.Channel

    beforeEach(() => {
        cy.logIn('/distribution')

        cy.route('GET', channelRoute).as('GetData')

        cy.get('[data-cy=sidebar-item-distribution]').click()
        cy.wait(500)
        cy.get('[data-cy=sidebar-item-distribution-channels] > .sidebar__link').click()

        cy.wait('@GetData')
    })

    describe('Filter Tests', function () {
        it('Search Term Code Filter', function () {

            cy.get('[data-cy=channels-list-filter]').clear().type(Channel.Original.Code)

            var found = false
            cy.get('tbody > tr').each((tr) => {
                if (tr[0].cells[0].innerText == Channel.Original.Code) {
                    cy.wrap(tr[0].cells[0].innerText).should('eq', Channel.Original.Code)
                    found = true
                }
            }).then(() => {
                cy.wrap(found).should('eq', true)
            })

        })

        it('Search Term Name Filter', function () {

            cy.get('[data-cy=channels-list-filter]').clear().type(Channel.Original.Name)

            var found = false
            cy.get('tbody > tr').each((tr) => {
                if (tr[0].cells[1].innerText == Channel.Original.Name) {
                    cy.wrap(tr[0].cells[1].innerText).should('eq', Channel.Original.Name)
                    found = true
                }
            }).then(() => {
                cy.wrap(found).should('eq', true)
            })

        })
    })

    describe('Feature Test', function () {
        it('Modifies Channel', function () {
            var modified = false

            cy.route('GET', airportGroupReference).as('GetAirportGroupReference')

            cy.route('GET', commissionTypeRoute).as('GetCommissionType')

            cy.route('GET', emailDeliveryMemberRegistrationRoute).as('GetMemberRegistration')
            cy.route('GET', emailDeliveryForgottenPasswordRoute).as('GetForgottenPassword')
            cy.route('GET', emailDeliveryBookingCancellationRoute).as('GetBookingCancellation')

            cy.get('tbody')
                .find('tr').each((tr) => {
                    if (tr[0].cells[1].innerText == Channel.Original.Name) {

                        cy.route('GET', channelRoute + '/*').as('GetChannel')
                        cy.route('PUT', channelRoute + '/*').as('PutData')

                        cy.wrap(tr[0].cells[3]).contains('Change').click()

                        cy.wait('@GetChannel')

                        cy.wait('@GetAirportGroupReference')

                        cy.wait('@GetCommissionType')

                        cy.wait('@GetMemberRegistration')
                        cy.wait('@GetForgottenPassword')
                        cy.wait('@GetBookingCancellation')

                        cy.get('[data-cy=channel-details-code-input] > .row > .field > :nth-child(1) > .input > .form-control').clear().type(Channel.Modified.Code)

                        cy.get('[data-cy=channel-details-name-input] > .row > .field > :nth-child(1) > .input > .form-control').clear().type(Channel.Modified.Name)

                        cy.get('[data-cy=channel-details-type-select] > .field > :nth-child(1) > .input > .form-control').select(Channel.Modified.ChannelType)
                        cy.get('[data-cy=channel-details-category-select] > .field > :nth-child(1) > .input > .form-control').select(Channel.Modified.ChannelCategory)

                        customFunctions.SetCheckbox('[data-cy=channel-details-process-payment-checkbox] > .row > .field > .form-check > .input__checkbox-label > .input__checkbox-input', Channel.Modified.ProcessPayment)
                        customFunctions.SetCheckbox('[data-cy=channel-details-booking-confirmation-checkbox] > .row > .field > .form-check > .input__checkbox-label > .input__checkbox-input', Channel.Modified.BookingConfirmationEmail.Checked)

                        cy.get('[data-cy=channel-details-precision-input] > .row > .field > :nth-child(1) > .input > .form-control').clear().type(Channel.Modified.MonetaryPrecision)
                        cy.get('[data-cy=channel-details-booking-fee-input] > .row > .field > :nth-child(1) > .input > .form-control').clear().type(Channel.Modified.BookingFee)

                        customFunctions.SetCheckbox('[data-cy=channel-details-calc-commission-checkbox] > .row > .field > .form-check > .input__checkbox-label > .input__checkbox-input', Channel.Modified.CalculateCommission.Checked)

                        if (Channel.Modified.CalculateCommission.Checked) {
                            cy.get('[data-cy=channel-details-commission-type-select] > .field > :nth-child(1) > .input > .form-control').select(Channel.Modified.CalculateCommission.CommissionType)
                            cy.get('[data-cy=channel-details-commission-amount-input] > .row > .field > :nth-child(1) > .input > .form-control').clear().type(Channel.Modified.CalculateCommission.CommissionAmount)
                        }

                        if (Channel.Modified.BookingConfirmationEmail.Checked) {
                            cy.get('[data-cy=channel-details-member-signup-select] > .field > :nth-child(1) > .input > .form-control').select(Channel.Modified.BookingConfirmationEmail.MemberSignupTemplate)
                            cy.get('[data-cy=channel-details-password-reset-email-template-select] > .field > :nth-child(1) > .input > .form-control').select(Channel.Modified.BookingConfirmationEmail.PasswordReset)
                            cy.get('[data-cy=channel-details-cancel-template-select] > .field > :nth-child(1) > .input > .form-control').select(Channel.Modified.BookingConfirmationEmail.CancelBooking)
                        }

                        cy.get('[data-cy=channel-action-button]').click()

                        cy.wait('@PutData').then((response) => {
                            cy.wrap(response.status).should('eq', 200)
                            modified = true
                        })
                    }
                }).then(() => {
                    cy.wrap(modified).should('eq', true)
                })
        })

        it('Checks Table for Modified Channel', function () {
            var found = false
            cy.get('tbody > tr').each((tr) => {
                if (tr[0].cells[1].innerText == Channel.Modified.Name) {
                    cy.wrap(tr[0].cells[0].innerText).should('eq', Channel.Modified.Code)
                    cy.wrap(tr[0].cells[1].innerText).should('eq', Channel.Modified.Name)
                    cy.wrap(tr[0].cells[2].innerText).should('eq', Channel.Modified.ChannelType)
                    found = true
                }
            }).then(() => {
                cy.wrap(found).should('eq', true)
            })
        })

        it('Reverts Channel', function () {

            var modified = false

            cy.route('GET', airportGroupReference).as('GetAirportGroupReference')

            cy.route('GET', commissionTypeRoute).as('GetCommissionType')

            cy.route('GET', emailDeliveryMemberRegistrationRoute).as('GetMemberRegistration')
            cy.route('GET', emailDeliveryForgottenPasswordRoute).as('GetForgottenPassword')
            cy.route('GET', emailDeliveryBookingCancellationRoute).as('GetBookingCancellation')

            cy.get('tbody > tr').each((tr) => {
                if (tr[0].cells[1].innerText == Channel.Modified.Name) {

                    cy.route('GET', channelRoute + '/*').as('GetChannel')
                    cy.route('PUT', channelRoute + '/*').as('PutData')

                    cy.wrap(tr[0].cells[3]).contains('Change').click()

                    cy.wait('@GetChannel')

                    cy.wait('@GetAirportGroupReference')

                    cy.wait('@GetCommissionType')

                    cy.wait('@GetMemberRegistration')
                    cy.wait('@GetForgottenPassword')
                    cy.wait('@GetBookingCancellation')

                    cy.get('[data-cy=channel-details-code-input] > .row > .field > :nth-child(1) > .input > .form-control').clear().type(Channel.Original.Code)

                    cy.get('[data-cy=channel-details-name-input] > .row > .field > :nth-child(1) > .input > .form-control').clear().type(Channel.Original.Name)

                    cy.get('[data-cy=channel-details-type-select] > .field > :nth-child(1) > .input > .form-control').select(Channel.Original.ChannelType)
                    cy.get('[data-cy=channel-details-category-select] > .field > :nth-child(1) > .input > .form-control').select(Channel.Original.ChannelCategory)

                    customFunctions.SetCheckbox('[data-cy=channel-details-process-payment-checkbox] > .row > .field > .form-check > .input__checkbox-label > .input__checkbox-input', Channel.Original.ProcessPayment)
                    customFunctions.SetCheckbox('[data-cy=channel-details-booking-confirmation-checkbox] > .row > .field > .form-check > .input__checkbox-label > .input__checkbox-input', Channel.Original.BookingConfirmationEmail.Checked)

                    cy.get('[data-cy=channel-details-precision-input] > .row > .field > :nth-child(1) > .input > .form-control').clear().type(Channel.Original.MonetaryPrecision)
                    cy.get('[data-cy=channel-details-booking-fee-input] > .row > .field > :nth-child(1) > .input > .form-control').clear().type(Channel.Original.BookingFee)

                    customFunctions.SetCheckbox('[data-cy=channel-details-calc-commission-checkbox] > .row > .field > .form-check > .input__checkbox-label > .input__checkbox-input', Channel.Original.CalculateCommission.Checked)

                    if (Channel.Original.CalculateCommission.Checked) {
                        cy.get('[data-cy=channel-details-commission-type-select] > .field > :nth-child(1) > .input > .form-control').select(Channel.Original.CalculateCommission.CommissionType)
                        cy.get('[data-cy=channel-details-commission-amount-input] > .row > .field > :nth-child(1) > .input > .form-control').clear().type(Channel.Original.CalculateCommission.CommissionAmount)
                    }

                    if (Channel.Original.BookingConfirmationEmail.Checked) {
                        cy.get('[data-cy=channel-details-member-signup-select] > .field > :nth-child(1) > .input > .form-control').select(Channel.Original.BookingConfirmationEmail.MemberSignupTemplate)
                        cy.get('[data-cy=channel-details-password-reset-email-template-select] > .field > :nth-child(1) > .input > .form-control').select(Channel.Original.BookingConfirmationEmail.PasswordReset)
                        cy.get('[data-cy=channel-details-cancel-template-select] > .field > :nth-child(1) > .input > .form-control').select(Channel.Original.BookingConfirmationEmail.CancelBooking)
                    }

                    cy.get('[data-cy=channel-action-button]').click()

                    cy.wait('@PutData').then((response) => {
                        cy.wrap(response.status).should('eq', 200)
                        modified = true
                    })
                }
            }).then(() => {
                cy.wrap(modified).should('eq', true)
            })
        })

        it('Checks Table for Reverted Channel', function () {
            var found = false
            cy.get('tbody > tr').each((tr) => {
                if (tr[0].cells[1].innerText == Channel.Original.Name) {
                    cy.wrap(tr[0].cells[0].innerText).should('eq', Channel.Original.Code)
                    cy.wrap(tr[0].cells[1].innerText).should('eq', Channel.Original.Name)
                    cy.wrap(tr[0].cells[2].innerText).should('eq', Channel.Original.ChannelType)
                    found = true
                }
            }).then(() => {
                cy.wrap(found).should('eq', true)
            })
        })
    })
})