import moment from 'moment'

export function ParseDate(dateString = null, amount = null, period = null, time = null) {
    if (dateString) {
        if (dateString && amount && period && time) {
            var date = moment(dateString).add(amount, period).format('YYYY-MM-DD')

            return moment(date + ' ' + time)
        }
        else if (dateString && amount && period) {
            return moment(dateString).add(amount, period)
        }
        else {
            return moment(dateString, 'DD-MM-YYYY')
        }
    }
    else {
        var currentDate = new Date()

        if (time) {
            var date = moment(currentDate).add(amount, period).format('YYYY-MM-DD')

            return moment(date + ' ' + time)
        }
        else {
            return moment(currentDate).add(amount, period)
        }
    }

}

export function GetDate(Data) {
    if (Data.Date) {
        if (Data.Amount && Data.Period) {
            if (Data.Time) {
                return ParseDate(Data.Date, Data.Amount, Data.Period, Data.Time)
            }
            else {
                return ParseDate(Data.Date, Data.Amount, Data.Period)
            }
        }
        else {
            if (Data.Time) {
                return ParseDate(Data.Date, null, null, Data.Time)
            }
            else {
                return ParseDate(Data.Date)
            }
        }
    }
    else if(Data.Amount && Data.Period){
        if (Data.Time) {
            return ParseDate(null, Data.Amount, Data.Period, Data.Time)
        }
        else {
            return ParseDate(null, Data.Amount, Data.Period)
        }
    }
    else {
        return moment(new Date())
    }
}

export function SetDate(target, date) {
    cy.get(target).invoke('removeAttr', 'readonly')
    cy.get(target).clear().type(date.format('MMMM DD, YYYY'))

    cy.get('.page-heading__title').click()
}

export function FileUpload(target, filePath, fileName) {
    cy.fixture(filePath).then(fileContent => {
        cy.get(target).upload({ fileContent, fileName, mimeType: 'application/json' })
    })
}

export function SetCheckbox(target, setValue) {
    if (setValue != null) {
        cy.get(target).then((check) => {
            var checkValue = check[0].control.checked

            if (setValue && !checkValue || !setValue && checkValue) {
                cy.get(target).click({force: true})
            }
        })
    }
}

export function SelectCheckbox(target, checkObj) {
    if (checkObj.length > 0) {
        cy.get(target).find('.input__checkbox-input').each((n) => {
            SetCheckbox(n, false)
        })

        cy.wrap(checkObj).each((obj, k) => {
            cy.get(target).find('.input__checkbox-input').each((n) => {
                if (n[0].innerText == obj) {
                    SetCheckbox(n, true)
                }
            })
        })
    }
}

export function TableSortAscending(Loop, Test, targetHead, targetBody) {
    cy.wrap(Loop).each((n) => {
        cy.log('Testing ' + Test[n])
        var before = ""
        var current = ""
        cy.get(targetHead + ' > tr > :nth-child(' + (n + 1) + ')').click()
        cy.get(targetBody)
            .find('tr').each((tr, i) => {
                before = current
                if (!isNaN(tr[0].cells[n].innerText))
                    current = parseInt(tr[0].cells[n].innerText)
                else
                    current = tr[0].cells[n].innerText.toUpperCase()

                expect(before).to.be.at.most(current)
            })
    })
}

export function TableSortDescending(Loop, Test, targetHead, targetBody) {
    cy.wrap(Loop).each((n) => {
        cy.log('Testing ' + Test[n])
        var before = ""
        var current = ""
        cy.get(targetHead + ' > tr > :nth-child(' + (n + 1) + ')').click()
        cy.get(targetHead + ' > tr > :nth-child(' + (n + 1) + ')').click()
        cy.get(targetBody)
            .find('tr').each((tr, i) => {

                before = current
                if (!isNaN(tr[0].cells[n].innerText))
                    current = parseInt(tr[0].cells[n].innerText)
                else
                    current = tr[0].cells[n].innerText.toUpperCase()

                if (i > 0)
                    expect(before).to.be.at.least(current)
            })
    })
}

export function Capitalize(word) {
    return word.charAt(0).toUpperCase() + word.slice(1)
}

export function GetTab(target) {
    cy.get('.card-header').then((tab) => {
        cy.wrap(tab[0].childNodes[0].childNodes[target]).click()
    })
}