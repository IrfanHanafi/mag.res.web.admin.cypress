// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//

import * as users from '../fixtures/users.json'
import 'cypress-file-upload'
import 'moment'


// -- This is a parent command --
Cypress.Commands.add("setup", () => {
    cy.route({
        method: 'GET',
        url: `/admin/api/auth/user-authenticated`,
        status: 200,
        response: {
            "clientSessionExists": true
        }
    }).as('getAuthenticationState');
    cy.route({
        method: 'GET',
        url: `/admin/api/permission/owner`,
        status: 200,
        response: []
    }).as('getPermissions')
    cy.route({
        method: 'GET',
        url: `/admin/api/feature`,
        status: 200,
        response: []
    }).as('getFeatures')
})

Cypress.Commands.add("allPermissions", () => {
    cy.route({
        method: 'GET',
        url: `/admin/api/permission/owner`,
        status: 200,
        response: [
            {
                "id": "fe849198-16d4-4c2f-80db-75a2ec99165b",
                "name": "inventory.airports.create"
            },
            {
                "id": "73a36efe-c15e-480b-8037-5513740bf12f",
                "name": "inventory.movement-pricing.delete"
            },
            {
                "id": "18b01ea6-d5b4-43da-a94a-177ee54785de",
                "name": "admin.white-labels.update"
            },
            {
                "id": "c67f45a1-d254-4cfc-b1e9-dba20c9067b1",
                "name": "inventory.product-categories.read"
            },
            {
                "id": "8bf0b0d2-da74-4190-a626-5d1ff9dcf8a0",
                "name": "inventory.content-matrices.read"
            },
            {
                "id": "7164bc3c-eafa-421c-af36-3d6391b3cba1",
                "name": "admin.credit-card-types.create"
            },
            {
                "id": "3f74a4a7-6f6b-4ea5-b091-18e59030341b",
                "name": "inventory.ancillaries.read"
            },
            {
                "id": "46b9bf58-98eb-448e-a3a7-b10028775e30",
                "name": "inventory.occupancy-pricing.read"
            },
            {
                "id": "ba600b80-a93e-40d4-8e8e-0479f60c5ecc",
                "name": "inventory.product-bundles.update"
            },
            {
                "id": "8ed6b8c5-c840-40f6-af6f-15d12cb7673e",
                "name": "inventory.movement-pricing.read"
            },
            {
                "id": "7bb5ed7c-df85-405d-ad54-56869f5da267",
                "name": "admin.tax-codes.create"
            },
            {
                "id": "ec1954eb-93eb-4c18-9a86-8a503d2eb36f",
                "name": "admin.terms-and-conditions.update"
            },
            {
                "id": "75585427-abd8-46f1-b6b8-0266ff2ae743",
                "name": "inventory.occupancy-pricing.create"
            },
            {
                "id": "6cbbfe75-49f8-4ac4-963b-500226137d61",
                "name": "inventory.product-rules.update"
            },
            {
                "id": "4a236e90-e655-46b9-bd70-5e3ceb7e6a19",
                "name": "admin.airport-groups.create"
            },
            {
                "id": "e3bf77ae-a113-4057-a291-3ad8f464fa80",
                "name": "inventory.product-categories.update"
            },
            {
                "id": "9d941aa8-5a9a-4f4b-be37-b1dc8fd1b8b1",
                "name": "inventory.movement-pricing.update"
            },
            {
                "id": "a80eedec-1df8-43f3-b05d-3807974f2aaa",
                "name": "inventory.product-rules.read"
            },
            {
                "id": "f5eca333-e188-48a6-b0dd-9d8e4e47ae42",
                "name": "admin.airport-groups.read"
            },
            {
                "id": "a39d2034-ac2e-4db0-ac76-457754d004fc",
                "name": "admin.terms-and-conditions.read"
            },
            {
                "id": "cc16a367-4f60-486e-aa5e-9e81847cd41f",
                "name": "inventory.daily-pricing.create"
            },
            {
                "id": "9323b999-e620-436d-a621-8fe9e1fe0c8c",
                "name": "inventory.pool-capacities.read"
            },
            {
                "id": "8e8a50e0-41a9-4546-ab09-95da75cb7ac9",
                "name": "inventory.ancillary-groups.create"
            },
            {
                "id": "9f08d009-c0dc-42c8-b53e-3da65872a304",
                "name": "inventory.product-rules.create"
            },
            {
                "id": "bb68e028-c191-4bbf-a291-c5318dfdda8e",
                "name": "admin.terms-and-conditions.create"
            },
            {
                "id": "deebb493-554e-404d-9841-50aed28e9a95",
                "name": "inventory.occupancy-pricing.delete"
            },
            {
                "id": "581f0bf7-f366-4dbb-a174-172169e1829c",
                "name": "admin.airport-groups.update"
            },
            {
                "id": "4832b362-51df-4079-b1ce-9d79246ec114",
                "name": "inventory.carparks.create"
            },
            {
                "id": "2c2f1f44-1c60-4647-9cae-5d5fa8e108ab",
                "name": "admin.custom-fields.update"
            },
            {
                "id": "68e7638c-03d7-4d2d-a008-eae2a769a355",
                "name": "inventory.airports.update"
            },
            {
                "id": "d7cf473c-f222-4187-9bc6-abb77d8a6ee5",
                "name": "admin.transfer-methods.read"
            },
            {
                "id": "a04cbdb4-4d15-4a74-8070-06b091ea98ba",
                "name": "admin.tax-codes.read"
            },
            {
                "id": "99105d23-0f8f-461e-b967-633d69d485fe",
                "name": "inventory.content-matrices.update"
            },
            {
                "id": "aa954b5b-f623-4bf4-ba47-21cb525d3104",
                "name": "inventory.carpark-products.read"
            },
            {
                "id": "e5488517-c3cd-4c70-b0b5-903b11a3c9bf",
                "name": "admin.custom-fields.read"
            },
            {
                "id": "b47b3f3c-720c-4e8e-b452-5240c003fc39",
                "name": "admin.credit-card-types.update"
            },
            {
                "id": "246b5455-f833-4e2e-8417-5bac51990837",
                "name": "admin.transfer-methods.create"
            },
            {
                "id": "dfa75cbd-c436-403d-b2fb-d80f3db86a1c",
                "name": "admin.custom-fields.create"
            },
            {
                "id": "6b8a0fc1-c588-4801-b9ae-d0bc54e23539",
                "name": "inventory.terminals.create"
            },
            {
                "id": "895830b2-eb85-4441-a8b7-6b6a694d968b",
                "name": "admin.delivery-instructions.update"
            },
            {
                "id": "4b2b6741-578d-448e-ae44-6c56e62210ba",
                "name": "admin.transfer-methods.update"
            },
            {
                "id": "6e12a7bb-ab28-4254-a32c-6221e368c9fa",
                "name": "inventory.occupancy-pricing.update"
            },
            {
                "id": "35a41b00-912e-41ba-930f-2e2d400eba17",
                "name": "inventory.daily-pricing.read"
            },
            {
                "id": "4c0b028e-6c50-40d4-92c5-4f0beaaa3fd3",
                "name": "inventory.ancillaries.update"
            },
            {
                "id": "affcd614-feb8-4cc9-bb31-20125d7c67f3",
                "name": "inventory.carparks.read"
            },
            {
                "id": "e6ebda4d-3c6f-4897-9bd7-0bed324b903d",
                "name": "inventory.airports.read"
            },
            {
                "id": "74ec4c23-4130-42dc-9919-87ebc4394cce",
                "name": "inventory.movement-pricing.create"
            },
            {
                "id": "fa529cc9-652a-43ca-b55e-97e6e21c7c85",
                "name": "admin.credit-card-types.delete"
            },
            {
                "id": "eb3d743e-0833-4974-ba0d-4fd598dd0d34",
                "name": "admin.roles"
            },
            {
                "id": "315b5730-b992-4b05-9647-8b6696f3c5c0",
                "name": "admin.tax-rates.read"
            },
            {
                "id": "5d916d2a-c931-48a0-acc7-8e257ee13c0c",
                "name": "inventory.content-matrices.create"
            },
            {
                "id": "132299b0-54d8-41ef-8e85-91defa0e4e95",
                "name": "inventory.product-bundles.create"
            },
            {
                "id": "e1814549-55a7-4799-aa4a-364de34b9bd9",
                "name": "inventory.product-categories.create"
            },
            {
                "id": "97e4b88c-42aa-4681-8562-ef00033e2ccf",
                "name": "admin.tax-rates.create"
            },
            {
                "id": "9330d02e-fee0-4045-be51-7f1c396b126c",
                "name": "inventory.terminals.update"
            },
            {
                "id": "1ef303c3-6842-402e-a9ff-581d12659096",
                "name": "inventory.pool-capacities.create"
            },
            {
                "id": "6820fb10-bf47-4882-9270-f19229376faf",
                "name": "admin.reporting-products.update"
            },
            {
                "id": "94a69891-5627-4902-9d86-0c026a77d92e",
                "name": "inventory.content-matrices.deactivate"
            },
            {
                "id": "7413f0d8-a54a-4736-8cdb-e9b1beda5097",
                "name": "admin.white-labels.create"
            },
            {
                "id": "688e1ef4-7e9f-49ab-ac04-33df0292ad5d",
                "name": "inventory.ancillaries.create"
            },
            {
                "id": "beb497e2-e1d7-45fa-be6a-c82be7e41972",
                "name": "admin.payment-types.read"
            },
            {
                "id": "64e2bf66-7b72-45b3-aa49-115d721c91fa",
                "name": "admin.credit-card-types.read"
            },
            {
                "id": "afd77da6-1b46-4d0b-92c6-a13a9e26d9a6",
                "name": "inventory.product-bundles.read"
            },
            {
                "id": "27f5e87e-e005-4abd-8aad-629a16620b3c",
                "name": "admin.tax-rates.update"
            },
            {
                "id": "0de6a12d-3a2c-40e8-a146-533322f32b32",
                "name": "admin.tax-codes.update"
            },
            {
                "id": "43dc23b7-1efc-4db8-9cde-f71ca3dad776",
                "name": "inventory.ancillary-groups.read"
            },
            {
                "id": "4fd738e6-7e7c-4722-9396-d00dccc52e96",
                "name": "inventory.pool-capacities.update"
            },
            {
                "id": "cef0fa4b-ed01-435d-9157-7d1c598f5d73",
                "name": "inventory.terminals.read"
            },
            {
                "id": "d12c9044-8543-41b0-b85d-59e554532be4",
                "name": "inventory.daily-pricing.update"
            },
            {
                "id": "6770ff21-e4cb-447b-9438-f4ec91143bcd",
                "name": "admin.white-labels.read"
            }
        ]
    }).as('getPermissions')
})

Cypress.Commands.add("allFeatures", () => {
    cy.route({
        method: 'GET',
        url: `/admin/api/feature`,
        status: 200,
        response: [
            {
                "name": "AirportGroupEnabled",
                "active": true
            },
            {
                "name": "AncillaryTicketRangesEnabled",
                "active": true
            },
            {
                "name": "AncillaryCreateEnabled",
                "active": true
            },
            {
                "name": "AncillaryUpdateCancellationRulesEnabled",
                "active": true
            },
            {
                "name": "AncillaryUpdateTicketContentEnabled",
                "active": true
            },
            {
                "name": "CarParkProductMovementsManagementEnabled",
                "active": true
            },
            {
                "name": "CarParkProductBookingFieldsEnabled",
                "active": true
            },
            {
                "name": "CarParkProductCreateEnabled",
                "active": true
            },
            {
                "name": "CarParkCreateEnabled",
                "active": true
            },
            {
                "name": "CarParkUpdateMapEnabled",
                "active": true
            },
            {
                "name": "ProductBundleCreateEnabled",
                "active": true
            },
            {
                "name": "ProductBundleUpdateEnabled",
                "active": true
            },
            {
                "name": "ProductRuleRestrictionsEnabled",
                "active": true
            },
            {
                "name": "ProductRuleCreateEnabled",
                "active": true
            },
            {
                "name": "PricingCalendarSegmentsEnabled",
                "active": true
            },
            {
                "name": "TariffLeadTimeCreateEnabled",
                "active": true
            },
            {
                "name": "ProductCategoriesEnabled",
                "active": true
            },
            {
                "name": "AirportsViewEnabled",
                "active": true
            },
            {
                "name": "AirportsCreateEnabled",
                "active": true
            },
            {
                "name": "AirportsChangeEnabled",
                "active": true
            },
            {
                "name": "AncillaryGroupCreateEnabled",
                "active": true
            },
            {
                "name": "TerminalChangeEnabled",
                "active": true
            },
            {
                "name": "TerminalCreateEnabled",
                "active": true
            },
            {
                "name": "TariffCopyEnabled",
                "active": true
            },
            {
                "name": "UserPermissionsEnabled",
                "active": true
            }
        ]
    }).as('getFeatures')
})

Cypress.Commands.add("logIn", (url) => {
    cy.server()
    cy.route('GET', url + '/api/auth/user-authenticated').as('GetAuth')
    cy.route('GET', url + '/api/permission/owner').as('GetPermissions')
    cy.route('GET', url + '/api/feature').as('GetFeatures')
    cy.visit(url + '/#')

    cy.wait('@GetAuth')
    cy.wait('@GetFeatures')
    cy.wait('@GetPermissions')
    cy.wait(1000)

    cy.get('#UserName').type(users.email)
    cy.get('#Password').type(users.password)
    cy.get('.button').click()

    cy.wait('@GetAuth')
    cy.wait('@GetPermissions')
    cy.wait('@GetFeatures')

})

Cypress.Commands.add("logInNoFeature", (url) => {
    cy.server()
    cy.route('GET', url + '/api/auth/user-authenticated').as('GetAuth')
    cy.route('GET', url + '/api/permission/owner').as('GetPermissions')
    cy.visit(url + '/#')

    cy.wait('@GetAuth')
    cy.wait(1000)
    cy.wait('@GetPermissions')

    cy.get('#UserName').type(users.email)
    cy.get('#Password').type(users.password)
    cy.get('.button').click()

    cy.wait('@GetAuth')
    cy.wait('@GetPermissions')
})

Cypress.Commands.add("logInNoPre", (url) => {
    cy.server()
    cy.route('GET', url + '/api/auth/user-authenticated').as('GetAuth')
    cy.route('GET', url + '/api/permission/owner').as('GetPermissions')
    cy.route('GET', url + '/api/feature').as('GetFeatures')
    cy.visit(url + '/#')

    cy.wait('@GetAuth')
    cy.wait(1000)

    cy.get('#UserName').type(users.email)
    cy.get('#Password').type(users.password)
    cy.get('.button').click()

    cy.wait('@GetAuth')
    cy.wait('@GetPermissions')
    cy.wait('@GetFeatures')
})
//
//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This is will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })
