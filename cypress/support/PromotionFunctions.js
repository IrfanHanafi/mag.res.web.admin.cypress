import moment from 'moment'
import * as customFunctions from './functions'
import * as Data from '../fixtures/AdminSetup.json'

const Promotion = Data.Marketing.Promotion

const PromotionTab = {
    DETAIL: 1,
    CHANNELS: 2,
    AFFILIATES: 3,
    DATES: 4,
    PRODUCTS: 5,
    ADDONS: 6,
    VARYINGDISCOUNTS: 7,
    PROMOTIONAVAILABILITY: 8,
    SUMMARY: 9
}

const ProductAddOnType = {
    PRODUCT: "product",
    ADDON: "addon"
}

const promotionRoute = 'marketing/api/promotion'
const promotionTypeRoute = 'marketing/api/promotion/type'
const addOnRoute = 'marketing/api/addon'
const airportReferenceRoute = 'marketing/api/reference/airport'
const carParkReferenceRoute = 'marketing/api/reference/carpark'
const channelReferenceRoute = 'marketing/api/reference/channel'
const productReferenceRoute = 'marketing/api/reference/product'
const productCategoryReferenceRoute = 'marketing/api/reference/product/category'
const addOnGroupReferenceRoute = 'marketing/api/reference/addon/group'
const productRuleReferenceRoute = 'marketing/api/reference/productrule'

export function before() {
    cy.logInNoFeature('/marketing')

    cy.route('GET', promotionRoute).as('GetData')
    cy.route('GET', carParkReferenceRoute).as('GetRefCarPark')
    cy.route('GET', airportReferenceRoute).as('GetRefAirport')
    cy.route('GET', channelReferenceRoute).as('GetRefChannel')

    cy.get('[data-cy=sidebar-item-marketing]').click()
    cy.get('[data-cy=sidebar-item-marketing-promotions]').click()

    cy.wait('@GetData')
    cy.wait('@GetRefCarPark')
    cy.wait('@GetRefAirport')
    cy.wait('@GetRefChannel')
}

export function ModifyDetail(testData, search) {
    var modified = false
    cy.get('.table-component__table__body')
        .find('tr').each((tr) => {
            if (tr[0].cells[0].innerText == search) {

                ModifyRouteSetup(tr)

                cy.wait(1000)
                cy.wrap(tr[0].cells[3]).contains('Update').click()

                ModifyRouteWait()

                cy.get('[data-cy=promotion-input-name] > .row > .field > :nth-child(1) > .input > .form-control').clear().type(testData.Detail.Name)

                cy.get('[data-cy=promotion-input-description] > .field > .input > .form-control').clear().type(testData.Detail.Description)

                cy.get('[data-cy=promotion-select-apply-promotion] > .field > :nth-child(1) > .input > .form-control').select(testData.Detail.ApplyPromotionTo)

                customFunctions.SetCheckbox('[data-cy=promotion-checkbox-show-discounts] > .row > .field > .form-check > .input__checkbox-label > .input__checkbox-input', testData.Detail.ShowDiscountProducts)

                customFunctions.SetCheckbox('[data-cy=promotion-checkbox-archived] > .row > .field > .form-check > .input__checkbox-label > .input__checkbox-input', testData.Detail.Archived)

                cy.get('[data-cy=promotion-select-promotion-type] > .field > :nth-child(1) > .input > .form-control').select(testData.Detail.PromotionType)

                cy.get('[data-cy=promotion-input-promotion-amount] > .row > .field > :nth-child(1) > .input > .form-control').clear().type(testData.Detail.PromotionAmount)

                cy.get('[data-cy=promotion-input-promotion-minimum] > .row > .field > :nth-child(1) > .input > .form-control').clear().type(testData.Detail.PromotionMinimum)

                customFunctions.SetCheckbox('[data-cy=promotion-checkbox-members-only] > .row > .field > .form-check > .input__checkbox-label > .input__checkbox-input', testData.Detail.MembersOnly)

                cy.get('[data-cy=promotion-input-booking-fee-discount] > .row > .field > :nth-child(1) > .input > .form-control').clear().type(testData.Detail.BookingFeeDiscount)

                customFunctions.SetCheckbox('[data-cy=promotion-checkbox-active] > .row > .field > .form-check > .input__checkbox-label > .input__checkbox-input', testData.Detail.Active)

                cy.get('[data-cy=promotion-save-btn]').click()

                cy.wait('@PutData').then((response) => {
                    cy.wrap(response.status).should('eq', 200)
                    modified = true
                })
            }
        }).then(() => {
            cy.wrap(modified).should('eq', true)
        })
}

export function ModifyTab(testData, target, search, VaryingDiscountState = null) {
    var modified = false
    cy.get('.table-component__table__body > tr').each((tr) => {
        if (tr[0].cells[0].innerText == search.Detail.Name) {
            ModifyRouteSetup(tr)

            cy.wait(1500)
            cy.wrap(tr[0].cells[3]).contains('Update').click()

            ModifyRouteWait()

            customFunctions.GetTab(target)

            switch (target) {
                case
                    PromotionTab.CHANNELS: customFunctions.SelectCheckbox('[data-cy=promotion-channels-multi-checkbox] > .checkbox-scroll-container', testData.Channels)
                    modified = true
                    break;
                case PromotionTab.AFFILIATES: customFunctions.SetCheckbox('[data-cy=promotion-affiliates-checkbox-all-affiliates] > .row > .field > .form-check > .input__checkbox-label > .input__checkbox-input', testData.AllAffiliates)
                    if (testData.AllAffiliates) {
                        customFunctions.SetCheckbox('[data-cy=promotion-affiliates-checkbox-all-affiliates] > .row > .field > .form-check > .input__checkbox-label > .input__checkbox-input', testData.AllAffiliates)
                        modified = true
                    }
                    else {
                        customFunctions.SelectCheckbox('[data-cy=promotion-affiliates-multi-checkbox] > .checkbox-scroll-container', testData.Affiliates)
                        modified = true
                    }
                    break;
                case PromotionTab.DATES:
                    ModifyDate(testData.BookingDates, '[data-cy=promotion-booking-dates-container] > .table-component__table-wrapper > .table-component__table > .table-component__table__body > tr', '@PutBooking')
                    cy.wait('@GetBookingDate')
                    CheckDate(testData.BookingDates, '[data-cy=promotion-booking-dates-container] > .table-component__table-wrapper > .table-component__table > .table-component__table__body > tr')

                    ModifyDate(testData.ArrivalDates, '[data-cy=promotion-arrival-dates-container] > .table-component__table-wrapper > .table-component__table > .table-component__table__body > tr', '@PutArrival')
                    cy.wait('@GetArrivalDate')
                    CheckDate(testData.BookingDates, '[data-cy=promotion-arrival-dates-container] > .table-component__table-wrapper > .table-component__table > .table-component__table__body > tr')

                    ModifyDate(testData.ExitDates, '[data-cy=promotion-exit-dates-container] > .table-component__table-wrapper > .table-component__table > .table-component__table__body > tr', '@PutExit')
                    cy.wait('@GetExitDate')
                    CheckDate(testData.BookingDates, '[data-cy=promotion-exit-dates-container] > .table-component__table-wrapper > .table-component__table > .table-component__table__body > tr')

                    ModifyTime(testData.BookingTimes, '[data-cy=promotion-booking-time-container] > .table-component__table-wrapper > .table-component__table > .table-component__table__body > tr', '@PutBookingTime')
                    cy.wait('@GetBookingTime')
                    CheckTime(testData.BookingTimes, '[data-cy=promotion-booking-time-container] > .table-component__table-wrapper > .table-component__table > .table-component__table__body > tr')
                    modified = true
                    break;
                case PromotionTab.PRODUCTS:
                    ModifyProductAddOn(testData.Products, 'data-cy=promotion-products', ProductAddOnType.PRODUCT)
                    modified = true
                    break;
                case PromotionTab.ADDONS:
                    ModifyProductAddOn(testData.AddOns, 'data-cy=promotion-add-ons', ProductAddOnType.ADDON)
                    modified = true
                    break;
                case PromotionTab.VARYINGDISCOUNTS:
                    if (VaryingDiscountState == 'CREATE') {

                        cy.route('POST', promotionRoute + '/*/varyingdiscount').as('SendDiscount')

                        cy.get('[data-cy=promotion-v-discounts-create-btn]').click()
                        cy.wait('@GetRefProductCategory')
                        cy.wait('@GetRefAddOnGroup')
                        FillVaryingDiscount(testData.VaryingDiscount.Create)

                        cy.wait('@GetVaryingDiscount')
                        CheckVaryingDiscount(testData.VaryingDiscount.Create)
                        modified = true
                    }

                    if (VaryingDiscountState == 'MODIFY') {
                        cy.get('.table-component__table__body > tr').each((tr) => {
                            if (tr[0].cells.length == 10) {

                                cy.route('PUT', promotionRoute + '/*/varyingdiscount/*').as('SendDiscount')

                                cy.wrap(tr[0].cells[9]).contains('Change').scrollIntoView().click()
                                FillVaryingDiscount(testData.VaryingDiscount.Change, true)
                                cy.get('.container > :nth-child(1) > .btn').click()

                                cy.wait('@GetVaryingDiscount')
                                CheckVaryingDiscount(testData.VaryingDiscount.Change)
                                modified = true
                            }
                        })
                    }
                    //if product is Permanent Product then do stuff
                    if (VaryingDiscountState == 'DELETE') {
                        cy.get('.table-component__table__body > tr').each((tr, i) => {
                            cy.log('repeat ' + i)
                            if (tr[0].cells.length == 10) {

                                cy.route('DELETE', promotionRoute + '/*/varyingdiscount/*').as('DeleteDiscount')

                                cy.wrap(tr[0].cells[9]).contains('Delete').scrollIntoView().click()
                                cy.get('.btn-danger').click()

                                cy.wait('@DeleteDiscount').then((response) => {
                                    cy.wrap(response.status).should('eq', 200)
                                    modified = true
                                })
                                cy.wait('@GetVaryingDiscount')
                            }
                        })
                    }
                    break;
                case PromotionTab.PROMOTIONAVAILABILITY:
                    ModifyPromotionAvailability(testData.PromotionAvailability)
                    modified = true
                    break;
            }
        }
    }).then(() => {
        cy.wrap(modified).should('eq', true)
    })
}

export function ModifyDate(testData, target, call) {

    var modified = false
    var DateFrom = customFunctions.GetDate(testData.FromDate)
    var DateTo = customFunctions.GetDate(testData.ToDate)

    cy.get(target).then((tr) => {
        cy.wait(1000)
        cy.wrap(tr[0].cells[2]).contains('Change').click()

        cy.get('#startDate').invoke('removeAttr', 'readonly')
        cy.get('#startDate').clear()
        cy.get('#startDate').type(DateFrom.format('DD/MM/YYYY'))

        cy.get('#endDate').invoke('removeAttr', 'readonly')
        cy.get('#endDate').clear()
        cy.get('#endDate').type(DateTo.format('DD/MM/YYYY'))

        cy.get('.modal-header').click()
        cy.get('.modal-footer > .btn').click()

        cy.wait(call).then((response) => {
            cy.wrap(response.status).should('eq', 200)
            modified = true
        })
    }).then(() => {
        cy.wrap(modified).should('eq', true)
    })
}

export function CheckDate(testData, target) {

    var modified = false

    var DateFrom = customFunctions.GetDate(testData.FromDate)
    var DateTo = customFunctions.GetDate(testData.ToDate)

    cy.get(target).then((tr) => {
        cy.wrap(tr[0].cells[0].innerText).should('eq', DateFrom.format('DD MMM YYYY'))
        cy.wrap(tr[0].cells[1].innerText).should('eq', DateTo.format('DD MMM YYYY'))
        modified = true
    }).then(() => {
        cy.wrap(modified).should('eq', true)
    })
}

export function ModifyTime(testData, target, call) {
    var modified = false

    cy.get(target).each((tr) => {
        cy.wrap(tr[0].cells[2]).contains('Change').click()

        cy.get('#hourFromHours').select(testData.TimeFrom.Hours)
        cy.get('#hourFromMinutes').select(testData.TimeFrom.Minutes)
        cy.get('#hourToHours').select(testData.TimeTo.Hours)
        cy.get('#hourToMinutes').select(testData.TimeTo.Minutes)
        cy.get('.modal-footer > .btn').click()

        cy.wait(call).then((response) => {
            cy.wrap(response.status).should('eq', 200)
            modified = true
        })
    }).then(() => {
        cy.wrap(modified).should('eq', true)
    })
}

export function CheckTime(testData, target) {

    var modified = false

    var TimeFrom = testData.TimeFrom.Hours + ':' + testData.TimeFrom.Minutes
    var TimeTo = testData.TimeTo.Hours + ':' + testData.TimeTo.Minutes

    cy.get(target).each((tr) => {
        cy.wrap(tr[0].cells[0].innerText).should('eq', TimeFrom.format('HH:mm:ss'))
        cy.wrap(tr[0].cells[1].innerText).should('eq', TimeTo.format('HH:mm:ss'))
        modified = true
    }).then(() => {
        cy.wrap(modified).should('eq', true)
    })
}

export function ModifyProductAddOn(testData, target, type) {

    cy.route('PUT', promotionRoute + '/*/*').as('PutItem')

    if (testData.Filter) {
        cy.get('[' + target + '-select-airport] > .field > :nth-child(1) > .input > .form-control').select(testData.Filter.Airport)
        cy.wait(500)
        cy.get('[' + target + '-select-category] > .field > :nth-child(1) > .input > .form-control').select(testData.Filter.Category)
        cy.wait(500)
        cy.get('[' + target + '-input-search] > .row > .field > :nth-child(1) > .input > .form-control').clear().type(testData.Filter.Search)
        cy.wait(500)
    }

    cy.get('[' + target + '-multi-check] > .checkbox-scroll-container').then((data) => {
        if (data[0].children[0].children[0].children.length > 0) {
            customFunctions.SelectCheckbox('[' + target + '-multi-check] > .checkbox-scroll-container', testData.Items)
        }
    })

    cy.get('[' + target + '-save-btn]').click()

    cy.wait('@PutItem').then((response) => {
        cy.wrap(response.status).should('eq', 200)
    })
    if (testData.Delete == null) {
        cy.get('[' + target + '-delete-all-btn]').click()
        cy.get('.btn-danger').click()
    } else if (testData.Delete != null) {
        if (testData.Delete.length > 0) {
            var Loop = Array.from({ length: testData.Delete.length }, (v, k) => k)
            cy.get('[' + target + '-container] > .table-component__table-wrapper > .table-component__table > .table-component__table__body > tr').each((tr) => {
                cy.wrap(Loop).each((i) => {
                    if (tr[0].cells[0].innerText == testData.Delete[i]) {
                        cy.wait(1500) //wait for notifications to disappear
                        cy.wrap(tr[0].cells[4]).contains('Delete').click()
                    }
                })
            })
        }
    }
}

export function FillVaryingDiscount(testData, modify = false) {

    var BookingDate = {
        Start: customFunctions.GetDate(testData.BookingDateStart),
        End: customFunctions.GetDate(testData.BookingDateEnd)
    }

    var ArrivalDate = {
        Start: customFunctions.GetDate(testData.ArrivalDateStart),
        End: customFunctions.GetDate(testData.ArrivalDateEnd)
    }

    cy.route('GET', promotionRoute + '/*/product?bookingDateStart=*').as('GetProductDate')


    cy.get('[data-cy=promotion-select-type] > .field > :nth-child(1) > .input > .form-control').select(testData.Type)

    cy.get('[data-cy=promotion-select-airport] > .field > :nth-child(1) > .input > .form-control').select(testData.Airport)

    if (!modify) {
        if (testData.Type == "Product") {
            cy.get('[data-cy=promotion-select-product-categories] > .field > :nth-child(1) > .input > .form-control').select(testData.Category)
        }
        else if (testData.Type == "Add On") {
            cy.get('[data-cy=promotion-select-add-on-group] > .field > :nth-child(1) > .input > .form-control').select(testData.Category)
        }
    }
    else {
        cy.get('[data-cy=promotion-select-product-add-on] > .field > :nth-child(1) > .input > .form-control')
    }

    customFunctions.SetDate('#bookingDateStart', BookingDate.Start)
    customFunctions.SetDate('#bookingDateEnd', BookingDate.End)
    customFunctions.SetDate('#arrivalDateStart', ArrivalDate.Start)
    customFunctions.SetDate('#arrivalDateEnd', ArrivalDate.End)

    if (!modify) {
        cy.wait('@GetProductDate')
        cy.get('.checkbox-scroll-container').then((data) => {
            if (data[0].children[0].children[0].children.length > 0) {
                customFunctions.SelectCheckbox('.checkbox-scroll-container', testData.Item)
            }
        })
    }

    cy.get('[data-cy=promotion-select-discount-type] > .field > :nth-child(1) > .input > .form-control').select(testData.DiscountType)
    cy.get('[data-cy=promotion-input-discount-value] > .row > .field > :nth-child(1) > .input > .form-control').clear().type(testData.DiscountValue)

    cy.get('[data-cy=promotion-v-discount-save-btn]').click()

    cy.wait('@SendDiscount').then((response) => {
        cy.wrap(response.status).should('eq', 200)
    })
}

export function CheckVaryingDiscount(testData) {
    var modified = false
    var BookingStart = customFunctions.GetDate(testData.BookingDateStart).format('DD MMM YYYY')
    var BookingEnd = customFunctions.GetDate(testData.BookingDateEnd).format('DD MMM YYYY')
    var ArrivalStart = customFunctions.GetDate(testData.ArrivalDateStart).format('DD MMM YYYY')
    var ArrivalEnd = customFunctions.GetDate(testData.ArrivalDateEnd).format('DD MMM YYYY')

    cy.get('[data-cy=promotion-varying-discounts-container] > .table-component__table-wrapper > .table-component__table > .table-component__table__body > tr').then((tr) => {
        cy.wrap(tr[0].cells[0].innerText).should('eq', BookingStart)
        cy.wrap(tr[0].cells[1].innerText).should('eq', BookingEnd)
        cy.wrap(tr[0].cells[2].innerText).should('eq', ArrivalStart)
        cy.wrap(tr[0].cells[3].innerText).should('eq', ArrivalEnd)
        cy.wrap(tr[0].cells[4].innerText).should('eq', testData.Airport)
        cy.wrap(tr[0].cells[5].innerText).should('eq', testData.Item.toString())
        cy.wrap(tr[0].cells[6].innerText).should('eq', testData.Offerline)
        cy.wrap(tr[0].cells[7].innerText).should('eq', testData.DiscountType)
        cy.wrap(tr[0].cells[8].innerText).should('eq', testData.DiscountValue.toString())
        modified = true
    }).then(() => {
        cy.wrap(modified).should('eq', true)
    })
}

export function ModifyPromotionAvailability(testData) {
    cy.get('[data-cy=promotion-availability-select-promotion-availability] > .field > :nth-child(1) > .input > .form-control').select(testData.PromotionAvailability)
    cy.get('[data-cy=promotion-availability-input-daily-limit] > .row > .field > :nth-child(1) > .input > .form-control').clear().type(testData.DailyLimit)
    cy.get('[data-cy=promotion-availability-input-daily-limit-value] > .row > .field > :nth-child(1) > .input > .form-control').clear().type(testData.DailyLimitValue)
    cy.get('[data-cy=promotion-availability-input-campaign-limit] > .row > .field > :nth-child(1) > .input > .form-control').clear().type(testData.CampaignLimit)
    cy.get('[data-cy=promotion-availability-input-campaign-limit-value] > .row > .field > :nth-child(1) > .input > .form-control').clear().type(testData.CampaignLimitValue)
    customFunctions.SetCheckbox('[data-cy=promotion-availability-availability-checkbox-active] > .row > .field > .form-check > .input__checkbox-label > .input__checkbox-input', testData.Active)
    cy.get('[data-cy=promotion-availability-availability-save-btn]').click()

    cy.wait('@PutPromotionAvailability').then((response) => {
        cy.wrap(response.status).should('eq', 200)
    })
}

export function CheckTable(testData) {
    var modified = false
    cy.get('.table-component__table__body > tr').each((tr) => {
        if (tr[0].cells[0].innerText == testData.Detail.Name) {
            cy.wrap(tr[0].cells[0].innerText).should('eq', testData.Detail.Name)
            cy.wrap(tr[0].cells[1].innerText).should('eq', testData.Detail.Code)
            cy.wrap(tr[0].cells[2].innerText).should('eq', testData.Detail.Active.toString())
            modified = true
        }
    }).then(() => {
        cy.wrap(modified).should('eq', true)
    })
}

export function ModifyRouteSetup(tr) {
    cy.wrap(tr[0].cells[3]).then((cell) => {
        var urlSplit = cell[0].childNodes[0].hash.split('/')
        var id = urlSplit[urlSplit.length - 2]

        cy.route('GET', promotionRoute + '/*').as('GetPromotion')
        cy.route('GET', promotionRoute + '/*/summary').as('GetSummary')
        cy.route('GET', promotionRoute + '/*/bookingdate').as('GetBookingDate')
        cy.route('GET', promotionRoute + '/*/arrivaldate').as('GetArrivalDate')
        cy.route('GET', promotionRoute + '/*/exitdate').as('GetExitDate')
        cy.route('GET', promotionRoute + '/*/bookingtime').as('GetBookingTime')
        cy.route('GET', promotionRoute + '/*/varyingdiscount').as('GetVaryingDiscount')
        cy.route('GET', promotionRoute + '/*/affiliate').as('GetAffliate')
        cy.route('GET', promotionRoute + '/*/affiliate/available').as('GetAffliateAvailability')
        cy.route('GET', promotionRoute + '/*/channel').as('GetChannel')
        cy.route('GET', promotionRoute + '/*/addon?').as('GetAddOn')
        cy.route('GET', promotionRoute + '/*/product?').as('GetProduct')

        cy.route('GET', promotionTypeRoute).as('GetPromotionType')
        cy.route('GET', productReferenceRoute).as('GetRefProduct')
        cy.route('GET', productCategoryReferenceRoute).as('GetRefProductCategory')
        cy.route('GET', addOnRoute).as('GetAddOn')
        cy.route('GET', addOnGroupReferenceRoute).as('GetRefAddOnGroup')
        cy.route('GET', productRuleReferenceRoute).as('GetRefProductRule')

        cy.route('PUT', promotionRoute + '/*').as('PutData')
        cy.route('PUT', promotionRoute + '/*/arrival/*').as('PutArrival')
        cy.route('PUT', promotionRoute + '/*/exit/*').as('PutExit')
        cy.route('PUT', promotionRoute + '/*/booking/*').as('PutBooking')
        cy.route('PUT', promotionRoute + '/*/bookingTimes/*').as('PutBookingTime')
        cy.route('PUT', promotionRoute + '/*/availability').as('PutPromotionAvailability')
    })
}

export function ModifyRouteWait() {
    cy.wait('@GetPromotion')
    cy.wait('@GetPromotionType')
    cy.wait('@GetSummary')
    cy.wait('@GetBookingDate')
    cy.wait('@GetArrivalDate')
    cy.wait('@GetExitDate')
    cy.wait('@GetBookingTime')
    cy.wait('@GetVaryingDiscount')
    cy.wait('@GetAffliate')
    cy.wait('@GetAffliateAvailability')
    cy.wait('@GetChannel')
    cy.wait('@GetAddOn')
    cy.wait('@GetProduct')

    cy.wait('@GetRefChannel')
    cy.wait('@GetRefAirport')
    cy.wait('@GetRefProduct')
    cy.wait('@GetRefProductCategory')
    cy.wait('@GetRefAirport')
    cy.wait('@GetAddOn')
    cy.wait('@GetRefAddOnGroup')
    cy.wait('@GetRefProductRule')
}