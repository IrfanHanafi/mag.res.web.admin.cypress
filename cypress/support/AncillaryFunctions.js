import * as customFunctions from './functions.js'
import * as Data from '../fixtures/AdminSetup.json'

const ancillaryRoute = 'inventory/api/AddOnCosted'
const ancillaryGroupRoute = 'inventory/api/AddOnCosted/group'
const durationStayTypeRoute = 'inventory/api/AddOnCosted/durationOfStayType'
const pricingBasisRoute = 'inventory/api/AddOnCosted/pricingBasis'
const availabilityTypeRoute = 'inventory/api/AddOnCosted/availabilityType'
const intervalTypeRoute = 'inventory/api/AddOnCosted/intervalType'
const addOnCostedTypeRoute = 'inventory/api/AddOnCosted/addoncostedtype'
const addOnRoute = 'inventory/api/addOn'
const emailTemplateRoute = 'inventory/api/email/template'
const vatCodeRoute = 'inventory/api/product/vatcode'
const airportRoute = 'inventory/api/airport'
const pageContentReferenceRoute = 'inventory/api/reference/PageContentFormFields'
const bardcodeReferenceRoute = 'inventory/api/reference/barcode'
const capacityPoolRoute = 'inventory/api/capacitypool'
const tariffRoute = 'inventory/api/tariff'
const addOnBookingFieldRoute = 'inventory/api/addOnBookingField'
const ancillaryFieldTypeReferenceRoute = 'inventory/api/reference/AddOnCostedFieldTypes'

const Tab = Data.Enum.AncillaryTab

export function VisitTab(search, target) {
    var modified = false

    if (search.Detail.Active) {
        cy.get('[data-cy=add-on-select-active]').select('Active')
    }
    else {
        cy.get('[data-cy=add-on-select-active]').select('Inactive')
    }

    cy.get('tbody > tr').each((tr) => {

        if (tr[0].cells[0].innerText == search.Detail.Name) {
            RouteSetup()

            cy.wrap(tr[0].cells[7]).contains('Change').click()

            RouteWait()

            customFunctions.GetTab(target)

            modified = true
        }
    }).then(() => {
        cy.wrap(modified).should('eq', true)
        cy.log('Found Item')
    })
}

export function RouteSetup() {
    cy.route('GET', ancillaryGroupRoute).as('GetAncillaryGroup')
    cy.route('GET', emailTemplateRoute).as('GetEmailTemplate')
    cy.route('GET', vatCodeRoute).as('GetVatCode')
    cy.route('GET', airportRoute).as('GetAirport')
    cy.route('GET', pageContentReferenceRoute).as('GetPageContent')
    cy.route('GET', intervalTypeRoute).as('GetIntervalType')
    cy.route('GET', addOnCostedTypeRoute).as('GetCostedTypeRoute')
    cy.route('GET', pricingBasisRoute).as('GetPricingBasis')
    cy.route('GET', availabilityTypeRoute).as('GetAvailability')
    cy.route('GET', durationStayTypeRoute).as('GetDurationStayType')
    cy.route('GET', capacityPoolRoute).as('GetCapacityPool')
    cy.route('GET', tariffRoute).as('GetTariff')
    cy.route('GET', bardcodeReferenceRoute).as('GetBarcodeRef')
    cy.route('GET', ancillaryFieldTypeReferenceRoute).as('GetFieldTypeRef')

    cy.route('GET', ancillaryRoute + '/*').as('GetAncillary')
    cy.route('GET', ancillaryRoute + '/*/type/*').as('GetAncillaryType')
    cy.route('GET', addOnRoute + '/*/bookingField').as('GetBookingField')
    cy.route('GET', ancillaryRoute + '/*/OfferLine').as('GetOfferline')
    cy.route('GET', ancillaryRoute + '/*/amendmentrule').as('GetAmendmentRule')
    cy.route('GET', ancillaryRoute + '/*/cancellationrule').as('GetCancellationRule')
    cy.route('GET', ancillaryRoute + '/*/customField').as('GetCustomField')
    cy.route('GET', ancillaryRoute + '/*/OfferLine').as('GetOfferline')
    cy.route('GET', ancillaryRoute + '/*/ticketrange').as('GetTicketRange')
    cy.route('GET', ancillaryRoute + '/*/OfferLine/*').as('GetAddOnOfferline')
    cy.route('GET', airportRoute + '/*/terminal').as('GetTerminal')
    cy.route('GET', airportRoute + '/*/productrule').as('GetProductRule')
}

export function RouteWait() {

    cy.wait('@GetAncillary')
    cy.wait('@GetAncillaryType')
    cy.wait('@GetOfferline')
    cy.wait('@GetAmendmentRule')
    cy.wait('@GetCancellationRule')
    cy.wait('@GetBookingField')
    cy.wait('@GetTicketRange')
    cy.wait('@GetBarcodeRef')
    cy.wait('@GetFieldTypeRef')
    cy.wait('@GetTerminal')
    cy.wait('@GetProductRule')
}

export function AddRules(testData, target) {

    var ruleType = ""
    if (target == Tab.AMENDMENTRULES) {
        ruleType = 'amendment'
    }
    else if (target == Tab.CANCELLATIONRULES) {
        ruleType = 'cancellation'
    }

    cy.log('Add ' + customFunctions.Capitalize(ruleType) + ' Rules')

    cy.get('[data-cy=add-ons-' + ruleType + '-button-create]').click()

    FillRules(testData, target)
    cy.wait('@PostRule')
}

export function ModifyRules(testData, target) {
    var modified = false

    var ruleType = ""
    if (target == Tab.AMENDMENTRULES) {
        ruleType = 'amendment'
    }
    else if (target == Tab.CANCELLATIONRULES) {
        ruleType = 'cancellation'
    }

    cy.log('Modify ' + customFunctions.Capitalize(ruleType) + ' Rules')

    cy.get('[data-cy=add-ons-' + ruleType + '-table-container] > .table-component__table-wrapper > .table-component__table > .table-component__table__body > tr').then((row) => {

        cy.wrap(row[0].cells[4].children[0].children[0]).click()
        FillRules(testData, target)
        cy.wait('@PutRule').then((response) => {
            cy.wrap(response.status).should('eq', 200)
            modified = true
        })
    }).then(() => {
        cy.wrap(modified).should('eq', true)
    })

    cy.get('.container > div > .btn').click()
}

export function FillRules(testData, target) {

    cy.wait(500)

    var FromDate = customFunctions.GetDate(testData.DateFrom)
    var ToDate = customFunctions.GetDate(testData.DateTo)

    var ruleType = ""
    if (target == Tab.AMENDMENTRULES) {
        ruleType = 'amendment'
        customFunctions.SetCheckbox('.input__checkbox-input', testData.AutoUpdate)
    }
    else if (target == Tab.CANCELLATIONRULES) {
        ruleType = 'cancellation'
        cy.get('[data-cy=add-ons-cancellation-input-cancellation-grace-days]').clear().type(testData.GraceDays)
    }

    cy.get('[data-cy=add-ons-' + ruleType + '-input-hours]').clear().type(testData.Hours)

    customFunctions.SetDate('#a-startDate', FromDate)
    customFunctions.SetDate('#a-endDate', ToDate)

    cy.get('[data-cy=add-ons-' + ruleType + '-button-save]').click()
}

export function DeleteRules(target) {

    var modified = false

    var ruleType = ""
    if (target == Tab.AMENDMENTRULES) {
        ruleType = 'amendment'
    }
    else if (target == Tab.CANCELLATIONRULES) {
        ruleType = 'cancellation'
    }

    cy.log('Delete ' + customFunctions.Capitalize(ruleType) + ' Rules')

    cy.get('[data-cy=add-ons-' + ruleType + '-table-container] > .table-component__table-wrapper > .table-component__table > .table-component__table__body > tr').then((row) => {

        cy.wrap(row[0].cells[4].children[0].children[2]).click()
        cy.wait(500)
        cy.get(`#addOn${customFunctions.Capitalize(ruleType)}ConfirmDeleteModal___BV_modal_footer_ > .btn-danger`).click()

        cy.wait('@DeleteRule').then((response) => {
            cy.wrap(response.status).should('eq', 200)
            modified = true
        })
    }).then(() => {
        cy.wrap(modified).should('eq', true)
    })
}

export function CheckRules(testData, target) {
    var modified = false

    var FromDate = customFunctions.GetDate(testData.DateFrom).format('DD MMM YYYY')
    var ToDate = customFunctions.GetDate(testData.DateTo).format('DD MMM YYYY')

    var ruleType = ""
    if (target == Tab.AMENDMENTRULES) {
        ruleType = 'amendment'
    }
    else if (target == Tab.CANCELLATIONRULES) {
        ruleType = 'cancellation'
    }

    cy.log('Modify ' + customFunctions.Capitalize(ruleType) + ' Rules')

    cy.get('[data-cy=add-ons-' + ruleType + '-table-container] > .table-component__table-wrapper > .table-component__table > .table-component__table__body > tr').then((row) => {
        debugger
        cy.wrap(row[0].cells[0].innerText).should('eq', testData.Hours.toString())
        cy.wrap(row[0].cells[1].innerText).should('eq', FromDate)
        cy.wrap(row[0].cells[2].innerText).should('eq', ToDate)

        if (target == Tab.AMENDMENTRULES) {
            cy.wrap(row[0].cells[3].innerText).should('eq', testData.AutoUpdate.toString())
        }
        else if (target == Tab.CANCELLATIONRULES) {
            cy.wrap(row[0].cells[3].innerText).should('eq', testData.GraceDays.toString())
        }

        modified = true
    }).then(() => {
        cy.wrap(modified).should('eq', true)
    })
}