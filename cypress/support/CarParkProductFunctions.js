import * as customFunctions from './functions.js'
import * as Data from '../fixtures/AdminSetup.json'

const productRoute = '**/api/product'
const productTariffRoute = '**/api/product/tariff'
const tariffRoute = '**/api/tariff'
const moreInfoTypeRoute = '**/api/product/moreinfotypes'
const carParkRoute = '**/api/carpark'
const airportRoute = '**/api/airport'
const deliveryInstrunctionRoute = '**/api/product/deliveryinstrunctions'
const emailTemplateRoute = '**/api/email/template'
const reportingProductRoute = '**/api/reportingProduct'
const vatCodeRoute = '**/api/product/vatcode'
const pricingBasisRoute = '**/api/product/pricingbasis'
const leadTimeRoute = '**/api/leadtime'
const termsAndConditionsRoute = '**/api/product/termsandconditions'
const capacityPoolRoute = '**/api/capacitypool'
const occupancyPricingRoute = '**/api/occupancypricing'
const movementPricingRoute = '**/api/movementpricing'
const formFieldsRefRoute = '**/api/reference/PageContentFormFields'
const customFormFieldsRefRoute = '**/api/reference/PageContentCustomFormFields'
const customWhiteLabelRefRoute = '**/api/reference/PageContentCustomWhiteLabels'

const Tab = Data.Enum.CarParkProductTab
export function VisitTab(search, target) {

    var modified = false

    RouteSetup()

    cy.get('.table-component__table__body > tr').each((tr) => {
        if (tr[0].cells[3].innerText == search.Detail.ProductName) {
            cy.wait(1000)
            cy.wrap(tr[0].cells[5]).contains('Change').click()

            RouteWait()

            customFunctions.GetTab(target)
            modified = true
        }
    }).then(() => {
        cy.wrap(modified).should('eq', true)
    })
}

export function RouteSetup() {
    cy.route('GET', emailTemplateRoute).as('GetEmailTemplate')
    cy.route('GET', reportingProductRoute).as('GetReportingProduct')
    cy.route('GET', leadTimeRoute).as('GetLeadTime')
    cy.route('GET', capacityPoolRoute).as('GetCapacityPool')
    cy.route('GET', tariffRoute).as('GetTariff')
    cy.route('GET', movementPricingRoute).as('GetMovementPricing')
    cy.route('GET', occupancyPricingRoute).as('GetOccupancyPricing')

    cy.route('GET', productRoute + '/*/cancellationRule').as('GetCancellationRule')
    cy.route('GET', productRoute + '/*/amendmentRule').as('GetAmendmentRule')
    cy.route('GET', productRoute + '/*').as('GetCarParkProduct')
    cy.route('GET', productRoute + '/*/availability').as('GetAvailability')
    cy.route('GET', termsAndConditionsRoute + '/*').as('GetTAC')
    cy.route('GET', airportRoute + '/*/product').as('GetAirportProduct')
    cy.route('GET', airportRoute + '/*/productrule').as('GetProductRule')
    cy.route('GET', airportRoute + '/*/productBundles').as('GetProductBundles')
    cy.route('GET', tariffRoute + '/*/ladder').as('GetLadder')
    cy.route('GET', productRoute + '/*/upgradeOption').as('GetUpgrade')
    cy.route('GET', productRoute + '/*/movementmanagement').as('GetMovement')
    cy.route('GET', productRoute + '/*/blackout').as('GetBlackOut')
    cy.route('GET', productRoute + '/*/movement').as('GetMovementAdjustment')
    cy.route('GET', productRoute + '/*/occupancy').as('GetOccupancyAdjustment')
    cy.route('GET', productRoute + '/*/channel').as('GetChannel')
    cy.route('GET', productRoute + '/*/BookingField').as('GetBookingField')
    cy.route('GET', productRoute + '/*/customfield').as('GetCustomField')
    cy.route('GET', formFieldsRefRoute).as('GetFormFieldsRef')
    cy.route('GET', customFormFieldsRefRoute).as('GetCustomFieldsRef')
    cy.route('GET', customWhiteLabelRefRoute).as('GetCustomWhiteLabelRef')
    cy.route('GET', productRoute + '/pagearea').as('GetPageArea')
    cy.route('GET', productRoute + '/*/content').as('GetContent')
}

export function RouteWait() {
    cy.wait('@GetEmailTemplate')
    cy.wait('@GetReportingProduct')
    cy.wait('@GetLeadTime')
    cy.wait('@GetCarParkProduct')
    cy.wait('@GetProductRule')
    cy.wait('@GetTariff')
    cy.wait('@GetLadder')
    cy.wait('@GetTAC')
}

export function AddAdjustment(testData, target) {
    var ruleType = ""
    if (target == Tab.MOVEMENTADJUSTMENTS) {
        cy.route('POST', productRoute + '/*/movement/*').as('PostAdjustment')
        ruleType = "move"
    }
    else if (target == Tab.OCCUPANCYADJUSTMENTS) {
        cy.route('POST', productRoute + '/*/occupancy').as('PostAdjustment')
        ruleType = "occyp"
        cy.get('[data-cy=car-park-p-occyp-adj-select-capacity-pool-type]').select(testData.PoolType)
    }

    cy.get('[data-cy=car-park-p-' + ruleType + '-adj-select-rule]').select(testData.Rule)

    cy.get('[data-cy=car-park-p-' + ruleType + '-adj-create-btn]').click()

    cy.wait('@PostAdjustment').then((response) => {
        cy.wrap(response.status).should('eq', 200)
    })
}

export function CheckAdjustment(testData, target) {

    var modified = false

    var fromDate = customFunctions.GetDate(testData.ArrivalDateFrom).format('DD MMM YYYY')
    var toDate = customFunctions.GetDate(testData.ArrivalDateTo).format('DD MMM YYYY')

    var ruleType = ""
    var n = 0
    if (target == Tab.MOVEMENTADJUSTMENTS) {
        ruleType = "move"
        n = 0
    }
    else if (target == Tab.OCCUPANCYADJUSTMENTS) {
        ruleType = "occyp"
        n = 1

        var PoolTypeStr = (testData.PoolType == "Product Capacity Pool") ? "P" : "C"
    }

    cy.get('[data-cy=car-park-p-' + ruleType + '-adj-table-container] > .table-component__table-wrapper > .table-component__table > .table-component__table__body > tr').each((tr) => {
        if (tr[0].cells[0].innerText == testData.Rule) {

            cy.wrap(tr[0].cells[0].innerText).should('eq', testData.Rule)
            cy.wrap(tr[0].cells[1 + n].innerText).should('eq', fromDate)
            cy.wrap(tr[0].cells[2 + n].innerText).should('eq', toDate)

            if (target == Tab.OCCUPANCYADJUSTMENTS) {
                cy.wrap(tr[0].cells[1].innerText).should('eq', PoolTypeStr)
            }

            modified = true
        }
    }).then(() => {
        cy.wrap(modified).should('eq', true)
    })
}

export function DeleteAdjustment(testData, target) {
    var modified = false

    var ruleType = ""
    var n = 0
    if (target == Tab.MOVEMENTADJUSTMENTS) {
        cy.route('DELETE', productRoute + '/movement/*').as('DeleteAdjustment')
        ruleType = "move"
        n = 3
    }
    else if (target == Tab.OCCUPANCYADJUSTMENTS) {
        cy.route('DELETE', productRoute + '/occupancy/*').as('DeleteAdjustment')
        ruleType = "occyp"
        n = 4
    }

    cy.get('[data-cy=car-park-p-' + ruleType + '-adj-table-container] > .table-component__table-wrapper > .table-component__table > .table-component__table__body > tr').each((tr) => {
        if (tr[0].cells[0].innerText == testData.Rule) {
            cy.wrap(tr[0].cells[n]).contains('Delete').click()
            cy.get('.modal-footer > .btn').click()

            cy.wait('@DeleteAdjustment').then((response) => {
                cy.wrap(response.status).should('eq', 200)
                modified = true
            })
        }
    }).then(() => {
        cy.wrap(modified).should('eq', true)
    })
}

export function AddRule(testData, target) {

    var ruleType = ""
    var ruleURL = ""
    if (target == Tab.AMENDMENTRULES) {
        ruleType = 'a-rules'
        ruleURL = "amendmentRule"
    }
    else if (target == Tab.CANCELLATIONRULES) {
        ruleType = 'c-rules'
        ruleURL = "cancellationRule"
    }

    cy.route('POST', productRoute + '/*/' + ruleURL + '').as('SendRule')
    cy.get('[data-cy=car-park-p-' + ruleType + '-create-btn]').click()

    FillRules(testData, target)
}

export function ModifyRule(testData, target) {

    var ruleType = ""
    var ruleURL = ""
    if (target == Tab.AMENDMENTRULES) {
        var n = 7
        ruleType = 'amendment-rules'
        ruleURL = "amendmentRule"
    }
    else if (target == Tab.CANCELLATIONRULES) {
        var n = 8
        ruleType = 'cancellation-rules'
        ruleURL = "cancellationRule"
    }

    cy.route('GET', productRoute + '/*/' + ruleURL + '/*').as('GetProductRule')
    cy.route('PUT', productRoute + '/*/' + ruleURL + '/*').as('SendRule')

    cy.get('[data-cy=car-park-product-' + ruleType + '-table-container] > .table-component__table-wrapper > .table-component__table > .table-component__table__body > tr').then((tr) => {
        cy.wrap(tr[0].cells[n]).contains('Edit').click()

        cy.wait('@GetProductRule')
        FillRules(testData, target)
        cy.get('.container > div > .btn').click()
    })

}

export function CheckRule(testData, target) {

    var ruleType = ""
    if (target == Tab.AMENDMENTRULES) {
        var n = 0
        ruleType = "amendment-rules"
    }
    else if (target == Tab.CANCELLATIONRULES) {
        var n = 1
        ruleType = "cancellation-rules"
    }

    var fromDate = customFunctions.GetDate(testData.DateFrom).format('DD MMM YYYY')
    var toDate = customFunctions.GetDate(testData.DateTo).format('DD MMM YYYY')

    var typeStr = (testData.Type == '%') ? "Percent" : "Amount"
    var fee = testData.Fee.toFixed(2);
    var charge = testData.Charge.toFixed(2);

    cy.get('[data-cy=car-park-product-' + ruleType + '-table-container] > .table-component__table-wrapper > .table-component__table > .table-component__table__body > tr').then((tr) => {
        cy.wrap(tr[0].cells[0].innerText).should('eq', testData.HoursFrom.toString())
        cy.wrap(tr[0].cells[1].innerText).should('eq', testData.HoursTo.toString())
        cy.wrap(tr[0].cells[2].innerText).should('eq', fromDate)
        cy.wrap(tr[0].cells[3].innerText).should('eq', toDate)
        cy.wrap(tr[0].cells[4 + n].innerText).should('eq', fee.toString())
        cy.wrap(tr[0].cells[5 + n].innerText).should('eq', typeStr)
        cy.wrap(tr[0].cells[6 + n].innerText).should('eq', charge.toString())

        if (target == Tab.CANCELLATIONRULES) {
            cy.wrap(tr[0].cells[4].innerText).should('eq', testData.GraceDays.toString())
        }
    })
}

export function DeleteRule(target) {

    var ruleURL = ""
    var ruleType = ""
    if (target == Tab.AMENDMENTRULES) {
        var n = 7
        ruleURL = "amendmentRule"
        ruleType = "amendment-rules"
    }
    else if (target == Tab.CANCELLATIONRULES) {
        var n = 8
        ruleURL = "cancellationRule"
        ruleType = "cancellation-rules"
    }

    cy.route('DELETE', productRoute + '/*/' + ruleURL + '/*').as('DeleteRule')
    cy.get('[data-cy=car-park-product-' + ruleType + '-table-container] > .table-component__table-wrapper > .table-component__table > .table-component__table__body > tr').then((tr) => {
        cy.wrap(tr[0].cells[n]).contains('Delete').click()
        cy.get('.btn-danger').click()

        cy.wait('@DeleteRule').then((response) => {
            cy.wrap(response.status).should('eq', 200)
        })
    })
}

function FillRules(testData, target) {

    var FromDate = customFunctions.GetDate(testData.DateFrom)
    var ToDate = customFunctions.GetDate(testData.DateTo)

    var ruleType = ""
    var dateTarget = ""
    if (target == Tab.AMENDMENTRULES) {
        ruleType = 'amendment-rules'
        dateTarget = 'a'
    }
    else if (target == Tab.CANCELLATIONRULES) {
        ruleType = 'cancellation-rules'
        dateTarget = 'c'
        cy.get('[data-cy=car-park-product-cancellation-rules-input-grace-days] > .row > .field > :nth-child(1) > .input > .form-control').clear().type(testData.GraceDays)
    }

    cy.get('[data-cy=car-park-product-' + ruleType + '-input-hours-from] > .row > .field > :nth-child(1) > .input > .form-control').clear().type(testData.HoursFrom)
    cy.get('[data-cy=car-park-product-' + ruleType + '-input-hours-to] > .row > .field > :nth-child(1) > .input > .form-control').clear().type(testData.HoursTo)

    customFunctions.SetDate('#' + dateTarget + '-startDate', FromDate)
    customFunctions.SetDate('#' + dateTarget + '-endDate', ToDate)

    cy.get('[data-cy=car-park-product-' + ruleType + '-input-fee]').clear().type(testData.Fee)
    cy.get('[data-cy=car-park-product-' + ruleType + '-select-type] > .field > :nth-child(1) > .input > .form-control').select(testData.Type)
    cy.get('[data-cy=car-park-product-' + ruleType + '-input-charge]').clear().type(testData.Charge)

    cy.get('[data-cy=car-park-product-' + ruleType + '-button-save]').click()

    cy.wait('@SendRule').then((response) => {
        cy.wrap(response.status).should('eq', 200)
    })
}